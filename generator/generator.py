import os
import sys
module_path = os.path.abspath(os.path.join('..'))
if module_path not in sys.path:
    sys.path.append(module_path)
import dynamics.motor_analysis as analysis
import layout.motor_tools as tools
# import rotary_inchworm.dynamics.motor_analysis as analysis
# import rotary_inchworm.layout.motor_tools as tools
import gdspy
import numpy as np

class MotorDrawer:
    def __init__(self, lib:gdspy.GdsLibrary, motor: analysis.InchwormSpeed, **kwargs):
        """Draws motor from InchwormSpeed object
        kwargs override normal parmeters"""
        LAYOUT_UNITS = 1e6
        tools.MIN_FEATURE = motor.MF*LAYOUT_UNITS
        params = {
            'num_fingers': motor.num_of_fingers,
            'g0': motor.g1,
            'gb': motor.g2,
            'finger_width': motor.width,
            'finger_length': motor.L_overlap,
            # 'space': 5e-6,
            'bar_ext': motor.shuttle_extension, # these are implied by default, can be changed
            # 'n_teeth': 2,
            'tooth_width': 1.25*motor.MF,
            'tooth_separation': 0.75 *motor.MF,
            'pawl_width': None,
            'pawl_length': None,
            'arm_length': motor.L_arm,
            'arm_width': motor.arm_width,
            'arm_angle': 180-motor.arm_angle,
            'gap_stop_gap': motor.g1 - motor.gap_final,
            'anchor_width': motor.L_anchor,
            'etch_hole_size': motor.etch_hole_size,
            'spring_length': motor.support_springs[1],
            'spring_width': motor.support_springs[0], 
            'bar_width': motor.L_frame*2,
            'space': motor.finger_space/2
        }
        not_length = ['num_fingers', 'arm_angle', 'n_teeth']
        converted_params = {}
        for (key,value) in params.items():
            if value is not None:
                if key not in not_length:
                    value = value *LAYOUT_UNITS
                converted_params[key] = value
        converted_params = {**converted_params, **kwargs}
        gca = tools.GCA(None, lib, **converted_params)
        gca.draw()
        self.motor_object = motor
        self.drawn_gca = gca
    def compare_mass(self):
        """compares the as drawn mass of the rotor with provided by motor object"""
        areas = self.drawn_gca.get_movable_area()
        drawn_mass =  self.motor_object.thickness *(self.motor_object.rho_silicon * areas[(self.drawn_gca.layer, self.drawn_gca.datatype)]*1e-12\
            + (self.motor_object.rho_medium - self.motor_object.rho_silicon)* areas[(self.drawn_gca.not_layer,self.drawn_gca.datatype)]*1e-12)
        return {'drawn_mass': drawn_mass, 'input_mass': self.motor_object.comp_shuttle_finger_mass()}
    def compare_total_area(self):
        bb = self.drawn_gca.get_bounding_box()
        drawn_area = (bb[1,0]-bb[0,0])*(bb[1,1]-bb[0,1])
        calculated_area = self.motor_object.comp_my_area()/self.motor_object.num_act
        return {'drawn_area': drawn_area, 'input_area': calculated_area}

# def draw_motor(lib:gdspy.GdsLibrary, motor: analysis.InchwormSpeed, **kwargs):
#     """Draws motor from InchwormSpeed object
#     kwargs override normal parmeters"""
#     LAYOUT_UNITS = 1e6
#     tools.MIN_FEATURE = motor.MF*LAYOUT_UNITS
#     params = {
#         'num_fingers': motor.num_of_fingers,
#         'g0': motor.g1,
#         'gb': motor.g2,
#         'finger_width': motor.width,
#         'finger_length': motor.finger_length,
#         # 'space': 5e-6,
#         # 'bar_ext': 50e-6, # these are implied by default, can be changed
#         # 'n_teeth': 2,
#         'tooth_width': 1.25*motor.MF,
#         'tooth_separation': 0.75 *motor.MF,
#         'pawl_width': None,
#         'pawl_length': None,
#         'arm_length': motor.L_arm,
#         'arm_width': motor.arm_width,
#         'arm_angle': 180-motor.arm_angle,
#         'gap_stop_gap': motor.g1 - motor.gap_final,
#         'anchor_width': motor.L_anchor,
#         'etch_hole_size': motor.etch_hole_size
#     }
#     not_length = ['num_fingers', 'arm_angle', 'n_teeth']
#     converted_params = {}
#     for (key,value) in params.items():
#         if value is not None:
#             if key not in not_length:
#                 value = value *LAYOUT_UNITS
#             converted_params[key] = value
#     converted_params = {**converted_params, **kwargs}
#     gca = tools.GCA(None, lib, **converted_params)
#     gca.draw()
#     return gca
