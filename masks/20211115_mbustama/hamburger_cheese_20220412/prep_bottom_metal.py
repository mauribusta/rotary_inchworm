# %%
import os
import sys

from cv2 import trace




module_path = os.path.abspath(os.path.join('../../..'))
if module_path not in sys.path:
    sys.path.append(module_path)

# %%
import dynamics.motor_analysis as analysis
import layout.motor_tools as tools
import layout.masktools.masktools as masktools
import layout.myGdspy as myGdspy
import gdspy
import numpy as np
import generator as generator


# %%
from IPython.display import SVG ##to display layout directly

# %%
fname = 'hamb_cheese_20220426_botmet_101'
lib = gdspy.GdsLibrary(infile  = fname + '.GDS')

METAL = 6
TOP_METAL = 14
GUIDE = 120

old_top = lib.top_level()[0]
top = lib.new_cell('TOP_CU')

# %%
# REFLECT 
top.add(myGdspy.y_reflection(old_top, lib))


# %%
ks = masktools.Ksaligner(name= 'KSAlign', lib = lib)
# %% 
run = 'HAMB_CHEESE'
layer_n = 'BOT_METAL'
date = '2022/04/26'
ks.add_label(run, layer_n, date, 101)
# %%
ks.add_wafer(layer = GUIDE, datatype =0, frame_width=1e3)
ks.add_frame(layer =GUIDE, datatype =0, frame_width=1e3)
ks.zero_marker(10, 101, 0)
# %% 
top.add(gdspy.CellReference(ks))

# %%
lib.write_gds(fname + '_chromeup.gds')
# %%

