# %%
import os
import sys


module_path = os.path.abspath(os.path.join('../..'))
if module_path not in sys.path:
    sys.path.append(module_path)

# %%
import dynamics.motor_analysis as analysis
import layout.motor_tools as tools
import gdspy
import numpy as np
import generator as generator

# %%
from IPython.display import SVG ##to display layout directly

# %%
lib = gdspy.GdsLibrary()

# %%
shuttle = tools.RotShuttle(None, lib, (0,0), 0, 200, etch_size=8, etch_hole_params={'num_of_points': 12})
shuttle.draw_shuttle()
shuttle.draw_teeth(tooth_extension=0.5)
# shuttle.spiralspring(100, 2, 100)
# shuttle.spiral.opposite_spring()
# shuttle.spiral.opposite_spring(np.pi/2)
# shuttle.spiral.opposite_spring(-np.pi/2)

# %%
length = 80
rotor = tools.GCA(None, lib, 50, anchor_width=100)
rotor.draw()
# metal = gdspy.offset(rotor.flexure_ref, -10, layer = tools.METAL, datatype= 0, join_first= True)
# metal2 = gdspy.offset(rotor.stator, -10, layer = tools.METAL, datatype= 0, join_first= True)
# rotor.add((metal, metal2))
# interconnect = gdspy.FlexPath([(60+length,150), (400,150), (400, -125), (-(180+length),-125), (-(180+length), 100), (-(60+length), 100)], width = 70, layer = tools.SOI, datatype= 0 ).to_polygonset()
# metal3 = gdspy.offset(interconnect, -10, layer= tools.METAL, datatype=0)
# rotor.add((interconnect, metal3))
topanchor_bb = rotor.flexure_ref[1].get_bounding_box()



full_motor = tools.SealedMotor(None, lib, rotor, shuttle, 4)
full_motor.draw_gcas()


# %%
full_motor.connect_gca_sides(width = 150)


# %%
full_motor.connect_opposite_pairs(width = 300, extra_space=100)

# %%
full_motor.add_dummy_silicon(10)
full_motor.flagellar_seal(hydrophobic_fingers = True, metal_gap =5)

# %%
[ref.origin for ref in full_motor.gca_refs]

# %%
# metal = full_motor.metalize_GCAs()

# %%
result = full_motor.connect_grounds(width =3 )

# %%
full_motor.traces_to_pads()
# %%
def remove_small_dummy(cell:gdspy.Cell, threshold = 10, soi_layer = tools.SOI, soi_hole_layer = tools.SOI_HOLE, re_soi_layer =tools.RE_SOI, not_dummy_layer = tools.NOT_DUMMY, dummy_layer = tools.DUMMY, datatype = 0):
    bb = cell.get_bounding_box()
    info_layer = 120
    dummy_bb = gdspy.Rectangle(bb[0,:], bb[1,:], layer = dummy_layer, datatype =datatype)
    dummy_bb = gdspy.offset(dummy_bb, threshold*2, join_first=True, layer = dummy_layer, datatype=datatype)
    polsets = cell.get_polygonsets()
    layers = {'layer': soi_layer, 'not_layer': soi_hole_layer, 're_layer':re_soi_layer, 'not_dummy_layer': not_dummy_layer}
    pols = {k: [] for (k,v) in layers.items()}
    for pol in polsets:
        for key in layers:
            if layers[key] in pol.layers:
                pols[key].append(pol)
    
    ## do math
    silicon  = gdspy.boolean(pols['layer'], pols['not_layer'], 'not', max_points=2000)
    silicon = gdspy.boolean(silicon, pols['re_layer'], 'or', layer = info_layer, datatype=datatype, max_points=2000)
    dummy = gdspy.boolean(dummy_bb, pols['not_dummy_layer'], 'not', max_points=2000)
    result = gdspy.boolean(silicon, dummy, 'or', max_points=2000)
    result = gdspy.offset(result, -threshold, join= 'miter',join_first=True, layer = info_layer, max_points=2000, precision= 0.0001)
    
    result = gdspy.offset(result, threshold, join='miter',join_first=True, layer = info_layer, max_points=2000, precision= 0.0001)
    result = gdspy.boolean(dummy_bb, result, 'not', layer = info_layer, datatype=datatype, max_points=2000) 
    # result = gdspy.boolean(dummy_bb, result, 'not', layer = info_layer, datatype=datatype, max_points=2000) 
    # result = gdspy.offset(result, -threshold, join='miter',join_first=True, layer = info_layer, max_points=2000, precision= 0.0001)
    not_dummy = gdspy.boolean(result, pols['not_dummy_layer'], 'or', layer = not_dummy_layer, datatype=0, max_points=4000)
    cell.add(dummy_bb)
    cell.add(not_dummy)
# %%
# remove_small_dummy(full_motor.gca)
# %%
full_motor.write_svg('temp.svg', scaling = 0.5)
SVG('temp.svg')

# %%
spring_gauge = tools.SpringGauge(None, lib, shuttle_length =350)
spring_gauge.draw_shuttle()
spring_gauge.draw_parallel_springs()
spring_gauge.draw_bottom_shuttle()

# %%
spring_gauge.bottom_right_spring.points

# %%
spring_gauge.write_svg('1temp.svg', scaling = 0.5)
SVG('1temp.svg')


# %%
vernier = tools.Vernier(None, lib, N_left=10, N_right = 9, pitch_left =8, pitch_right = 9 )
# %% 
vernier.write_svg('temp.svg', scaling =1 )
SVG('temp.svg')

# %%
vernier.combs

# %%

lib.write_gds('layout.gds')


# %%
os.startfile('layout.gds')
# %%

# %%
