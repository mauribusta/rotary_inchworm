# %%
import os
import sys

from cv2 import trace




module_path = os.path.abspath(os.path.join('../..'))
if module_path not in sys.path:
    sys.path.append(module_path)

# %%
import dynamics.motor_analysis as analysis
import layout.motor_tools as tools
import layout.hydrophobic_tools as hydro
import layout.masktools.masktools as mask 
import gdspy
import numpy as np
import generator as generator

# %%
from IPython.display import SVG ##to display layout directly

# %%
lib = gdspy.GdsLibrary()

METAL = 6
TOP_METAL = 14
GUIDE = 120
TOP_VIA = tools.TOP_VIA
ALIGNMENT = 18

dies = []
# %% [markdown]
# Make circles of different radii to test bonding
# %% 

def  place_in_die(cell: gdspy.Cell, lib= lib, die_length = 1e4, die_width = 1e4, layer = GUIDE):
    bb = cell.get_bounding_box()
    center = bb.mean(axis =0)
    out = lib.new_cell(cell.name + '_die')
    rect = gdspy.Rectangle((-die_length/2, -die_width/2), (die_length/2,die_width/2), layer = layer)
    out.add(rect)
    out.add(gdspy.CellReference(cell, -center))

    corner_cell = lib.new_cell(f'corner_cell_' + cell.name)
    corner = gdspy.FlexPath(((-1000,0),(0,0),(0, -1000)), 100, layer = METAL)
    corner2= gdspy.boolean(corner, None, 'or', layer = TOP_METAL)
    corner_cell.add((corner,corner2))
    pt = np.array([die_length/2-50,die_width/2 -50])
    out.add(gdspy.CellReference(corner_cell, pt) )
    out.add(gdspy.CellReference(corner_cell, (pt[0], -pt[1]), -90) )
    out.add(gdspy.CellReference(corner_cell, (-pt[0], -pt[1]), -180) )
    out.add(gdspy.CellReference(corner_cell, (-pt[0], pt[1]), 90) )
    dies.append(out)



    return out
# %% 
import itertools
def array_for_die(xy_dims,  function, args, pitch = [1e3,1e3]):
    values = list(itertools.product(*args))
    cols = int(xy_dims[0]/pitch[0])
    for i, value in enumerate(values):
        loc = (pitch[0] *(i%cols), -(i//cols)* pitch[1])
        function(loc, *value)
def array_for_die_np(xy_dims,  function, args, pitch = [1e3,1e3]):
    values = zip(*args)
    cols = int(xy_dims[0]/pitch[0])
    for i, value in enumerate(values):
        loc = (pitch[0] *(i%cols), -(i//cols)* pitch[1])
        function(loc, *value)

def array_sep_dies( function, args):
    values = list(itertools.product(*args))
    for i, value in enumerate(values):
        cell = lib.new_cell(function.__name__ + str(i))
        function(cell, (0,0), *value)
        place_in_die(cell)
def array_lin_dies( function, args):
    values = zip(*args)
    out = []
    for i, value in enumerate(values):
        cell = lib.new_cell(function.__name__ + str(i))
        function(cell, (0,0), *value)
        out.append(place_in_die(cell))
    return out

def three_side_gap(cell:gdspy.Cell, width=1.2e3, gap = 2e3, y_gap = 2.5e3, x_gap = -100):
    """adds a seal around most of the die """
    bb = cell.get_bounding_box()
    top_left = np.array((bb[:,0].min()-gap, bb[:,1].max() + gap))
    top_right = np.array([bb[:,0].max() + gap, bb[:,1].max()+gap])
    bottom_left = np.array([bb[:,0].min() -x_gap, bb[:,1].min()+y_gap])
    bottom_right = np.array([bb[:,0].max()+x_gap,bb[:,1].min()+y_gap])
    trace = gdspy.FlexPath((bottom_left, (top_left[0], bottom_left[1]), top_left, top_right, (top_right[0], bottom_right[1]), bottom_right), 
    width, layer = METAL)
    trace_top = gdspy.offset(trace, -100, layer = TOP_METAL)
    cell.add((trace, trace_top))
# %%
# shuttle = tools.RotShuttle(None, lib, (0,0), 0, 200, etch_size=8, etch_hole_params={'num_of_points': 12})
# shuttle.draw_shuttle()
# shuttle.draw_teeth(tooth_extension=0.5)



### different types of shuttles 
r_in_r_out = [(0, 200), (50, 200), (100, 500), (200,500)]
spring_lengths = [50, 100, 200, 500, 1500]
spring_number = [1]
# %% 
def d_shuttle(cell:gdspy.Cell, loc, rs, spring_length, spring_number):
    shuttle = tools.RotShuttle(cell.name +str(loc), lib, (0,0), rs[0], rs[1], etch_hole_params={'num_of_points': 12})
    shuttle.draw_shuttle()
    shuttle.draw_teeth()
    if rs[0]>0:
        if not (spring_length<100 and spring_number ==4):
            
            shuttle.spiralspring(spring_length, 3, rs[0])
            if spring_number==2:
                shuttle.spiral.opposite_spring()
            

            if spring_number == 4:
                shuttle.spiral.opposite_spring()
                shuttle.spiral.opposite_spring(np.pi/2)
                shuttle.spiral.opposite_spring(-np.pi/2)
    hole = gdspy.Round((0,0), rs[1]-10, number_of_points=12, layer = TOP_VIA, datatype=0)
    shuttle.add(hole)
    circle = gdspy.Round((0,0), rs[1]+10, layer = tools.NOT_DUMMY, datatype=0)
    shuttle.add(circle)
    text = gdspy.Text(f'i {rs[0]}, o {rs[1]}, l {spring_length}', 30, [0, rs[1]+40], layer = METAL, datatype=0)
    shuttle.add(text)
    cell.add(gdspy.CellReference(shuttle, loc))
# %%



# %% 
def d_gca(cell: gdspy.Cell, loc, num_fingers, finger_length, spring_length, metalize ):
    rotor = tools.GCA(cell.name + str(loc), lib, num_fingers=num_fingers, finger_length=finger_length, spring_length=spring_length, anchor_width=150)
    rotor.draw()
   
    flex = rotor.flexure_ref[0].ref_cell.anchor
    flex.translate(0, -rotor.anchor_width/2 + 10)
    text = gdspy.Text(f'N {num_fingers}  L {finger_length} S {spring_length}', 30, [rotor.get_bounding_box()[0,0], rotor.get_bounding_box()[1,1] + 40], layer = METAL, datatype=0)
    rotor.add(text)
    not_dummy = tools.compute_anchors(rotor, -10, layer = tools.SOI, output_layer= tools.NOT_DUMMY, output_datatype=rotor.datatype, do_math = False)

    if metalize:
        rotor.metalize(6, top_metal_layer=None)
    # rect = gdspy.Rectangle(rotor.get_bounding_box()[0,:], rotor.get_bounding_box()[1,:])
    # rect = gdspy.offset(rect, 10, layer = tools.NOT_DUMMY)
    # rotor.add(rect)
    cell.add(gdspy.CellReference(rotor, loc))
# %%

num_fingers = [50, 100, 200]
finger_length = [50, 75, 100]
spring_length = [240, 440, 340]
metalize = [True]
# %%

gca_cells = lib.new_cell('GCA_TESTS_1')
# %%
array_for_die([0.9e4, 0.9e4], lambda *args: d_gca(gca_cells, *args), (num_fingers, finger_length, spring_length, metalize), pitch =[1e3,4e3])
place_in_die(gca_cells)
# %%
metalize = [False]
gca_cells = lib.new_cell('GCA_TESTS_2m')
# %%
array_for_die([0.9e4, 0.9e4], lambda *args: d_gca(gca_cells, *args), (num_fingers, finger_length, spring_length, metalize), pitch =[1e3,4e3])
place_in_die(gca_cells)


# %% 

def d_bridge(cell:gdspy.Cell, loc, trace_width, neck_width, alignment_tol):
    pad_size =1e3
    metal_gap = 30
    length = 6e3
    trace_width_start = 500
    sub_cell = lib.new_cell(cell.name + str(loc))
    pad_cell = lib.new_cell(cell.name + 'pad' + str(loc))
    rect = gdspy.Rectangle((-pad_size/2,-pad_size/2), (pad_size/2,pad_size/2), layer = tools.SOI, datatype=0)
    rect_m = gdspy.offset(rect, -metal_gap, layer = METAL, datatype= 0)
    rect_hole = gdspy.offset(rect_m, -metal_gap, layer = tools.TOP_VIA, datatype=0)
    pad_cell.add((rect,rect_m, rect_hole))
    soi_trace = gdspy.PolyPath([[0,0],[0,length/2-trace_width_start],[0,length/2]], [trace_width_start, trace_width_start,trace_width], layer = tools.SOI, datatype=0)
    pad_cell.add(soi_trace)
    pad_array = gdspy.CellArray(pad_cell, 4, 1, (pad_size + 200, 1))
    sub_cell.add(pad_array)

    top_trace_pts =[[0,length/2], [0, length], [(pad_size + 200)*2, length],  [(pad_size + 200)*2,length/2]]
    
    bottom_trace_pts =[[pad_size + 200,length/2], [pad_size + 200, length+ trace_width + 500], [(pad_size + 200)*3, length + trace_width + 500],  [(pad_size + 200)*3,length/2]]
    top_trace = gdspy.FlexPath(top_trace_pts, trace_width)
    bottom_trace = gdspy.FlexPath(bottom_trace_pts, trace_width)
    # sub_cell.add((top_trace, bottom_trace))
    tools.make_necking_bridge(sub_cell, top_trace=top_trace, bottom_trace=bottom_trace, neck_width=neck_width, alignment_tol=alignment_tol)
    cell.add(gdspy.CellReference(sub_cell, loc))
    tools.compute_anchors(cell, -10, layer = tools.SOI, output_layer= tools.NOT_DUMMY, output_datatype=0, do_math=False)
    text = gdspy.Text(f'tw {trace_width}  nw {neck_width}  at  {alignment_tol}', 100, [cell.get_bounding_box()[0,0], cell.get_bounding_box()[1,1] + 200], layer = METAL)
    cell.add(text)
    three_side_gap(cell,1e3, 0.5e3, x_gap =0)


def d_contact(cell:gdspy.Cell, loc, trace_width):
    pad_size =1e3
    metal_gap = 30
    length = 6e3
    trace_width_start = 500
    sub_cell = lib.new_cell(cell.name + str(loc))
    pad_cell = lib.new_cell(cell.name + 'pad' + str(loc))
    rect = gdspy.Rectangle((-pad_size/2,-pad_size/2), (pad_size/2,pad_size/2), layer = tools.SOI, datatype=0)
    rect_m = gdspy.offset(rect, -metal_gap, layer = METAL, datatype= 0)
    rect_hole = gdspy.offset(rect_m, -metal_gap, layer = tools.TOP_VIA, datatype=0)
    pad_cell.add((rect,rect_m, rect_hole))
    soi_trace = gdspy.PolyPath([[0,0],[0,length/2-trace_width_start],[0,length/2]], [trace_width_start, trace_width_start,trace_width], layer = tools.SOI, datatype=0)
    pad_cell.add(soi_trace)
    pad_array = gdspy.CellArray(pad_cell, 4, 1, (pad_size + 200, 1))
    sub_cell.add(pad_array)

    top_trace_pts =[[0,length/2], [0, length], [(pad_size + 200)*2, length],  [(pad_size + 200)*2,length/2]]
    
    bottom_trace_pts =[[pad_size + 200,length/2], [pad_size + 200, length+ trace_width + 500], [(pad_size + 200)*3, length + trace_width + 500],  [(pad_size + 200)*3,length/2]]
    top_trace = gdspy.FlexPath(top_trace_pts, trace_width, layer = TOP_METAL)
    bottom_trace = gdspy.FlexPath(bottom_trace_pts, trace_width, layer = METAL)
    soi_b = gdspy.offset(bottom_trace, metal_gap, layer = tools.SOI)

    hop_up = gdspy.FlexPath([[0,length/2], [0, length/2 + trace_width]],trace_width, layer= METAL)
    hop_up_soi = gdspy.offset(hop_up, metal_gap, layer = tools.SOI )

    hop_down = gdspy.copy(hop_up,2*(pad_size + 200), 0 )
    hop_down_soi = gdspy.copy(hop_up_soi, 2*(pad_size + 200), 0) 

    sub_cell.add((top_trace, bottom_trace, soi_b, hop_up, hop_up_soi, hop_down, hop_down_soi))
    # sub_cell.add((top_trace, bottom_trace))
    # for trace in (top_trace, bottom_trace):
    #     soi = gdspy.offset(trace, metal_gap, layer = tools.SOI)
    #     # top_metal = gdspy.boolean(trace, None, 'or', layer = TOP_METAL)
    #     sub_cell.add((trace, soi))
      
    # tools.make_necking_bridge(sub_cell, top_trace=top_trace, bottom_trace=bottom_trace, neck_width=neck_width, alignment_tol=alignment_tol)
    cell.add(gdspy.CellReference(sub_cell, loc))
    tools.compute_anchors(cell, -10, layer = tools.SOI, output_layer= tools.NOT_DUMMY, output_datatype=0, do_math = False)
    text = gdspy.Text(f'tw {trace_width}', 100, [cell.get_bounding_box()[0,0], cell.get_bounding_box()[1,1] + 200], layer = METAL)
    cell.add(text)
    three_side_gap(cell, 1e3, 0.5e3, x_gap=0)

# %%
jumping_cell = lib.new_cell('Jumping')
# %%
trace_widths = [50, 100, 200, 300, 500]
array_lin_dies(d_contact, (trace_widths,))
# %%

trace_width = [300,300,300, 500,500,500, 500]
neck_width = [50,100,150, 100, 100, 100, 20]
alignment_tol = [100, 100, 100, 50, 100, 200, 200]
array_lin_dies(d_bridge, (trace_width, neck_width, alignment_tol))
# place_in_die(gca_cells)




# %%


def d_full_motor(cell: gdspy.Cell, loc, shuttle_ri, shuttle_ro, N_fingers, metalize, trace_width):
    shuttle = tools.RotShuttle(None, lib, (0,0), shuttle_ri , shuttle_ro, etch_size=8, etch_hole_params={'num_of_points': 12})
    shuttle.draw_shuttle()
    shuttle.draw_teeth(tooth_extension=0.5)
    if shuttle_ri>0:
        shuttle.spiralspring(100, 3, shuttle_ri)
    rotor = tools.GCA(None, lib, N_fingers, anchor_width=100)
    rotor.draw()

    full_motor = tools.SealedMotor(None, lib, rotor, shuttle, 4)
    full_motor.draw_gcas()
    full_motor.connect_gca_sides(width = 150)
    if metalize:
        rotor.metalize(6, top_metal_layer=None)
    full_motor.connect_opposite_pairs(width = 300, extra_space=100)
    full_motor.add_dummy_silicon(10)
    full_motor.flagellar_seal(hydrophobic_fingers = False, metal_gap =5, num_points=12)
    [ref.origin for ref in full_motor.gca_refs]
    full_motor.connect_grounds(width =3 )
    full_motor.traces_to_pads(trace_width = trace_width ,extra_length=2e3)
    bb = full_motor.get_bounding_box()
    gap = 2e3
    top_left = np.array((bb[:,0].min()-gap, bb[:,1].max() + gap))
    top_right = np.array([bb[:,0].max() + gap, bb[:,1].max()+gap])
    bottom_left = np.array([bb[:,0].min() +100, bb[:,1].min()+gap])
    bottom_right = np.array([bb[:,0].max()-100,bb[:,1].min()+gap])
    trace = gdspy.FlexPath((bottom_left, (top_left[0], bottom_left[1]), top_left, top_right, (top_right[0], bottom_right[1]), bottom_right), 
    1.3e3, layer = METAL)
    trace_top = gdspy.offset(trace, -100, layer = TOP_METAL)
    full_motor.add((trace, trace_top))
    shuttle_bb = shuttle.get_bounding_box()
    dummy_ex = gdspy.Rectangle(shuttle_bb[0,:], shuttle_bb[1,:])
    dummy_ex = gdspy.offset(dummy_ex, 40, layer =18)
    shuttle.add(dummy_ex)
    cell.add(gdspy.CellReference(full_motor, loc))

# %%

shuttle_ri = [0, 100, 0, 100, 0, 250, 100]
shuttle_ro = [200, 200, 500, 500, 750, 750, 500]
N_fingers = [50, 50, 50, 50, 50, 50, 100]
metalize = [True, False, False, False, False, False, False]
trace_width = [300, 500, 300, 300, 300, 300, 300]

# shuttle_ri = [100]
# shuttle_ro = [500]
# N_fingers = [100]
# metalize = [False]
# trace_width = [300]
args = (shuttle_ri, shuttle_ro, N_fingers, metalize, trace_width)
for arg in args:
    print(len(arg))
# %%
motors = array_lin_dies(d_full_motor, args)
# import cProfile
# cProfile.run("array_lin_dies(d_full_motor, args)")

# %%
shuttles_cell = lib.new_cell('Shuttles_1')

# pillar = tools.BondingPillar(None, lib, 1000, 1000)
# pillar_array  = gdspy.CellArray(pillar, 4, 3, [2e3, 3e3], [-4e3,-3e3])
# pillar_array = gdspy.CellReference(pillar)

# %%
array_for_die([1e4,1e4], lambda *args: d_shuttle(shuttles_cell, *args), (r_in_r_out, spring_lengths, spring_number), pitch = [2e3, 2e3])

out= place_in_die(shuttles_cell)
# out.add(pillar_array)

# %% 
# out.write_svg('temp.svg', scaling = 0.25)
# SVG('temp.svg')

# %%

shuttles_2 = lib.new_cell('shuttles_2')
# %% 
r_in_r_out = [(50, 200), (100, 500), (200,500)]
spring_lengths = [100, 200, 500]
spring_number = [2,4]

Rs =[*[(50,200)],*[(100, 500)]*6, *[(200, 500)]*4, *[(400, 500)]*6] 
Spring_lengths= [100, *[100,200,500]*2, *[200,1500]*2, *[500,1500, 2000]*2  ]
Spring_number = [2, *[2]*3, *[4]*3 , *[2]*2, *[4]*2, *[2]*3, *[4]*3]

array_for_die_np([1e4,1e4], lambda *args: d_shuttle(shuttles_2, *args), (Rs, Spring_lengths, Spring_number), pitch = [2e3, 2e3])
place_in_die(shuttles_2)

# %%

# SVG('temp.svg')

top_cell = lib.new_cell('thistop')
dies = dies[0:-1] + motors[1:6] + [dies[-1]]
new_dies = dies.copy()
new_dies[1:6] = dies[2:7]
new_dies[6] = dies[1]
dies = new_dies

for i,die in enumerate(dies):
    row = i // 7
    col = i %7
    pos = [1.1e4 *col, -1.1e4 *row]
    top_cell.add(gdspy.CellReference(die, pos))



# %%

# add text
r_text = gdspy.Text('R', 3e3, (75000,0), layer = METAL)
r_text_t = gdspy.offset(r_text, -10, layer = TOP_METAL)
r_text_hole = gdspy.Text('R', 3e3, (75000,-3.5e3), layer = TOP_VIA)
top_cell.add(r_text)
top_cell.add(r_text_t)
top_cell.add(r_text_hole)

r_text = gdspy.Text('R', 3e3, (-12000,0), layer = METAL)
r_text_t = gdspy.offset(r_text, -10, layer = TOP_METAL)
r_text_hole = gdspy.Text('R', 3e3, (-12000,-3.5e3), layer = TOP_VIA)
top_cell.add(r_text)
top_cell.add(r_text_t)
top_cell.add(r_text_hole)
## add litho test (next to R)

litho = mask.LithoStrucs(lib = lib)
litho.exposure_test(layer = TOP_VIA, feature = [10,20,30,40])
top_cell.add(gdspy.CellReference(litho, (-14000,0)))
top_cell.add(gdspy.CellReference(litho, (77000,0)))

## shift top cell to correct location
loc = [-33000, -10741.6]
overtop = lib.new_cell('overtop')
overtop.add(gdspy.CellReference(top_cell, loc))

# %% ksaligner backside marks
ks = mask.Ksaligner(lib = lib)
ks.backside_am_windows()
overtop.add(gdspy.CellReference(ks))



# %% make masks for better alignment

mark_cell = lib.new_cell('AM_alignment mask')
innersquare = gdspy.Rectangle([-60, -60], [60,60], layer = ALIGNMENT)
# outersquare = gdspy.offset(innersquare, 20, layer = ALIGNMENT) 
# square = gdspy.boolean(outersquare, innersquare, 'not', layer = ALIGNMENT)
triangle = gdspy.Polygon([[-20,10], [20,10], [0,-10]], layer = ALIGNMENT, datatype=0 )
triangle_c = lib.new_cell('tri')
triangle_c.add(triangle)
coors = [[0, 50], [-50,0], [0, -50], [50,0]]
angs = [0, 90, 180, 270]
triang_refs =[]
for i, coor in enumerate(coors):
    triang_refs.append(gdspy.CellReference(triangle_c, coor, rotation = angs[i]))


final = gdspy.boolean(innersquare, triang_refs, 'not', layer = ALIGNMENT)
mark_cell.add(final)
overtop.add(gdspy.CellReference(mark_cell, [-58.007192e3, -0.795]))

overtop.add(gdspy.CellReference(mark_cell, [58.007192e3, -0.795]))

# %%
# %% import dillons stuff
dillon_fname = 'hamburger_cheese_20220621/Hamburger_cheese_via_drawing_T.GDS'
lib.read_gds(infile = dillon_fname, rename_template= 'DAJ-{name}', layers = {101: METAL, 102: tools.SOI, 103: tools.TOP_METAL})
daj_cell = lib.cells['DAJ-auau']
overtop.add(gdspy.CellReference(daj_cell))
# %%

lib.write_gds('20220618.gds')
# %%
os.startfile('20220618.gds')
# %%
# %%
  