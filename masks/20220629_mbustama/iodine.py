# %%
import os
import sys

from cv2 import trace





module_path = os.path.abspath(os.path.join('../..'))
if module_path not in sys.path:
    sys.path.append(module_path)

# %%
import layout.motor_tools as tools
import layout.hydrophobic_tools as hydro
import layout.masktools.masktools as mask 
import layout.myGdspy as myGdspy
import gdspy
import numpy as np
import generator as generator
import itertools

# %%
from IPython.display import SVG ##to display layout directly

# %%
lib = gdspy.GdsLibrary()

SOI = tools.SOI
SOI_HOLE = tools.SOI_HOLE
METAL = tools.METAL
TOP_METAL = tools.TOP_METAL
TOP_VIA = tools.TOP_VIA
DUMMY = tools.DUMMY
NOT_DUMMY = tools.NOT_DUMMY
# %% create top cell
die_top = lib.new_cell('DIE')


# %% add frame of reference for space
gc6 = mask.Gcaws6(lib = lib)
gc6.add_frame(layer = 0, datatype=1)
gc6.add_am((0,-2.8e3), layer = METAL, datatype=0)
gc6.add_am((0, 2.8e3), layer = TOP_METAL, datatype=0)
# gc6.add_reticle_am(layer = METAL, datatype=0)

die_top.add(gdspy.CellReference(gc6))

# %% 
def array_for_die_np(xy_dims,  function, args, pitch = [1e3,1e3]):
    values = zip(*args)
    cols = int(xy_dims[0]/pitch[0])
    for i, value in enumerate(values):
        loc = (pitch[0] *(i%cols), -(i//cols)* pitch[1])
        function(loc, *value)
def array_for_die(xy_dims,  function, args, pitch = [1e3,1e3]):
    values = list(itertools.product(*args))
    cols = int(xy_dims[0]/pitch[0])
    for i, value in enumerate(values):
        loc = (pitch[0] *(i%cols), -(i//cols)* pitch[1])
        function(loc, *value)
def produce_cells_np(function, args):
    values = zip(*args)
    cells = []
    for i, value in enumerate(values):
        loc = [0,0]
        cell = lib.new_cell(function.__name__ + str(id(value))+str(i))
        function(cell, loc, *value)
        cells.append(cell)
    return cells
def produce_cells_p(function, args):
    values = list(itertools.product(*args))
    cells = []
    for i, value in enumerate(values):
        loc = [0,0]
        cell = lib.new_cell(function.__name__ + str(id(value))+str(i))
        function(cell, loc, *value)
        cells.append(cell)
    return cells
def add_isolation_seal(cell, gap, isolation_width, mark_size = 100):
    bb = cell.get_bounding_box()
    frame = gdspy.Rectangle(bb[0,:], bb[1,:])
    inner = gdspy.offset(frame, gap)
    outer = gdspy.offset(inner, isolation_width)
    frame = gdspy.boolean(outer, inner, 'not', layer = tools.NOT_DUMMY)
    out = [frame]
    bb = outer.get_bounding_box()
    corners = [bb[0,:], bb[1,:], np.array([bb[0,0], bb[1,1]]), np.array([bb[1,0], bb[0,1]])]
    for corner in corners:
        vertex1 = corner - np.array([mark_size, mark_size])
        vertex2 = corner + np.array([mark_size, mark_size])
        rect = gdspy.Rectangle(vertex1, vertex2)
        mark= gdspy.boolean(frame, rect, 'and', layer = tools.TOP_VIA)
        out.append(mark)
    return out

def place_cells(top: gdspy.Cell, cells: list, origin = (0,0), gap = 200):
    origin = np.array(origin)
    for cell in cells:
        bb = cell.get_bounding_box()
        bottom_left = bb[0,:]
        ref = gdspy.CellReference(cell, origin - bottom_left)
        top.add(ref)
        tbb = ref.get_bounding_box()
        origin = np.array([tbb[1,0], tbb[0,1]]) + np.array([gap,0])
# %% add motors to occupy lower half
def d_full_motor(cell: gdspy.Cell, loc, shuttle_ri, shuttle_ro, N_fingers, metalize, trace_width):
    shuttle = tools.RotShuttle(None, lib, (0,0), shuttle_ri , shuttle_ro, etch_size=8, etch_hole_params={'num_of_points': 12})
    shuttle.draw_shuttle()
    shuttle.draw_teeth(tooth_extension=0.5)
    if shuttle_ri>0:
        shuttle.spiralspring(100, 3, shuttle_ri)
    rotor = tools.GCA(None, lib, N_fingers, anchor_width=100)
    rotor.draw()

    full_motor = tools.SealedMotor(None, lib, rotor, shuttle, 4)
    full_motor.draw_gcas()
    full_motor.connect_gca_sides(width = 150)
    if metalize:
        rotor.metalize(6, top_metal_layer=None)
    full_motor.connect_opposite_pairs(width = 300, extra_space=100)
    full_motor.add_dummy_silicon(10)
    full_motor.flagellar_seal(hydrophobic_fingers = False, metal_gap =5, num_points=12, num_circ_points = 24)
    [ref.origin for ref in full_motor.gca_refs]
    full_motor.connect_grounds(width =3 )
    full_motor.traces_to_pads(trace_width = trace_width ,extra_length=2e3)
    bb = full_motor.get_bounding_box()
    gap = 300
    bottom_clearance = 2e3
    seal_width = 500
    box = gdspy.Rectangle(bb[0,:] + np.array([0, bottom_clearance]), bb[1,:])
    inner =  gdspy.offset(box, gap)
    outer = gdspy.offset(inner, seal_width)
    motor_growth = full_motor.get_polygons(by_spec=True)[(tools.NOT_DUMMY,0)]
    motor_growth = gdspy.offset(motor_growth, 100)
    trace = gdspy.boolean(outer, [inner, motor_growth], 'not', layer = METAL, datatype =0)
    full_motor.add(trace)
    frame = add_isolation_seal(full_motor, 100, 40)
    full_motor.add(frame)

    # top_left = np.array((bb[:,0].min()-gap, bb[:,1].max() + gap))
    # top_right = np.array([bb[:,0].max() + gap, bb[:,1].max()+gap])
    # bottom_left = np.array([bb[:,0].min() +100, bb[:,1].min()+gap])
    # bottom_right = np.array([bb[:,0].max()-100,bb[:,1].min()+gap])
    
    

    # trace = gdspy.FlexPath((bottom_left, (top_left[0], bottom_left[1]), top_left, top_right, (top_right[0], bottom_right[1]), bottom_right), 
    # 1.3e3, layer = METAL)
    # trace_top = gdspy.offset(trace, -100, layer = TOP_METAL)
    # full_motor.add((trace, trace_top))
    shuttle_bb = shuttle.get_bounding_box()
    dummy_ex = gdspy.Rectangle(shuttle_bb[0,:], shuttle_bb[1,:])
    dummy_ex = gdspy.offset(dummy_ex, 40, layer =18)
    shuttle.add(dummy_ex)
    cell.add(gdspy.CellReference(full_motor, loc))

def d_bridge(cell:gdspy.Cell, loc, trace_width, neck_width, alignment_tol):
    pad_size =500
    metal_gap = 30
    length = 3e3
    trace_width_start = 500
    gap = 100
    sub_cell = lib.new_cell(cell.name + str(loc))
    pad_cell = lib.new_cell(cell.name + 'pad' + str(loc))
    rect = gdspy.Rectangle((-pad_size/2,-pad_size/2), (pad_size/2,pad_size/2), layer = tools.SOI, datatype=0)
    rect_m = gdspy.offset(rect, -metal_gap, layer = METAL, datatype= 0)
    rect_hole = gdspy.offset(rect_m, -metal_gap, layer = tools.TOP_VIA, datatype=0)
    pad_cell.add((rect,rect_m, rect_hole))
    soi_trace = gdspy.PolyPath([[0,0],[0,length/2-trace_width_start],[0,length/2]], [trace_width_start, trace_width_start,trace_width], layer = tools.SOI, datatype=0)
    pad_cell.add(soi_trace)
    pad_array = gdspy.CellArray(pad_cell, 4, 1, (pad_size + gap, 1))
    sub_cell.add(pad_array)

    top_trace_pts =[[0,length/2], [0, length], [(pad_size + gap)*2, length],  [(pad_size + gap)*2,length/2]]
    
    bottom_trace_pts =[[pad_size + gap,length/2], [pad_size + gap, length+ trace_width + 500], [(pad_size + gap)*3, length + trace_width + 500],  [(pad_size + gap)*3,length/2]]
    top_trace = gdspy.FlexPath(top_trace_pts, trace_width)
    bottom_trace = gdspy.FlexPath(bottom_trace_pts, trace_width)
    # sub_cell.add((top_trace, bottom_trace))
    tools.make_necking_bridge(sub_cell, top_trace=top_trace, bottom_trace=bottom_trace, neck_width=neck_width, alignment_tol=alignment_tol)
    cell.add(gdspy.CellReference(sub_cell, loc))
    tools.compute_anchors(cell, -10, layer = tools.SOI, output_layer= tools.NOT_DUMMY, output_datatype=0, do_math=False)
    text = gdspy.Text(f'tw {trace_width}  nw {neck_width}  at  {alignment_tol}', 100, [cell.get_bounding_box()[0,0], cell.get_bounding_box()[1,1] + 200], layer = METAL)
    cell.add(text)
    frame = add_isolation_seal(cell, 100, 40)
    cell.add(frame)
def d_contact(cell:gdspy.Cell, loc, trace_width):
    pad_size =500
    metal_gap = 30
    length = 3e3
    trace_width_start = 500
    gap = 100
    sub_cell = lib.new_cell(cell.name + str(loc))
    pad_cell = lib.new_cell(cell.name + 'pad' + str(loc))
    rect = gdspy.Rectangle((-pad_size/2,-pad_size/2), (pad_size/2,pad_size/2), layer = tools.SOI, datatype=0)
    rect_m = gdspy.offset(rect, -metal_gap, layer = METAL, datatype= 0)
    rect_hole = gdspy.offset(rect_m, -metal_gap, layer = tools.TOP_VIA, datatype=0)
    pad_cell.add((rect,rect_m, rect_hole))
    soi_trace = gdspy.PolyPath([[0,0],[0,length/2-trace_width_start],[0,length/2]], [trace_width_start, trace_width_start,trace_width], layer = tools.SOI, datatype=0)
    pad_cell.add(soi_trace)
    pad_array = gdspy.CellArray(pad_cell, 4, 1, (pad_size + gap, 1))
    sub_cell.add(pad_array)

    top_trace_pts =[[0,length/2], [0, length], [(pad_size + gap)*2, length],  [(pad_size + gap)*2,length/2]]
    
    bottom_trace_pts =[[pad_size + gap,length/2], [pad_size + gap, length+ trace_width + 500], [(pad_size + gap)*3, length + trace_width + 500],  [(pad_size + gap)*3,length/2]]
    top_trace = gdspy.FlexPath(top_trace_pts, trace_width, layer = TOP_METAL)
    bottom_trace = gdspy.FlexPath(bottom_trace_pts, trace_width, layer = METAL)
    soi_b = gdspy.offset(bottom_trace, metal_gap, layer = tools.SOI)

    hop_up = gdspy.FlexPath([[0,length/2], [0, length/2 + trace_width]],trace_width, layer= METAL)
    hop_up_soi = gdspy.offset(hop_up, metal_gap, layer = tools.SOI )

    hop_down = gdspy.copy(hop_up,2*(pad_size + gap), 0 )
    hop_down_soi = gdspy.copy(hop_up_soi, 2*(pad_size + gap), 0) 

    sub_cell.add((top_trace, bottom_trace, soi_b, hop_up, hop_up_soi, hop_down, hop_down_soi))
    # sub_cell.add((top_trace, bottom_trace))
    # for trace in (top_trace, bottom_trace):
    #     soi = gdspy.offset(trace, metal_gap, layer = tools.SOI)
    #     # top_metal = gdspy.boolean(trace, None, 'or', layer = TOP_METAL)
    #     sub_cell.add((trace, soi))
      
    # tools.make_necking_bridge(sub_cell, top_trace=top_trace, bottom_trace=bottom_trace, neck_width=neck_width, alignment_tol=alignment_tol)
    cell.add(gdspy.CellReference(sub_cell, loc))
    tools.compute_anchors(cell, -10, layer = tools.SOI, output_layer= tools.NOT_DUMMY, output_datatype=0, do_math = False)
    text = gdspy.Text(f'tw {trace_width}', 100, [cell.get_bounding_box()[0,0], cell.get_bounding_box()[1,1] + 200], layer = METAL)
    cell.add(text)
    # three_side_gap(cell, 1e3, 0.5e3, x_gap=0)
    frame = add_isolation_seal(cell, 100, 40)
    cell.add(frame)
def three_side_gap(cell:gdspy.Cell, width=1.2e3, gap = 2e3, y_gap = 2.5e3, x_gap = -100):
    """adds a seal around most of the die """
    bb = cell.get_bounding_box()
    top_left = np.array((bb[:,0].min()-gap, bb[:,1].max() + gap))
    top_right = np.array([bb[:,0].max() + gap, bb[:,1].max()+gap])
    bottom_left = np.array([bb[:,0].min() -x_gap, bb[:,1].min()+y_gap])
    bottom_right = np.array([bb[:,0].max()+x_gap,bb[:,1].min()+y_gap])
    trace = gdspy.FlexPath((bottom_left, (top_left[0], bottom_left[1]), top_left, top_right, (top_right[0], bottom_right[1]), bottom_right), 
    width, layer = METAL)
    trace_top = gdspy.offset(trace, -100, layer = TOP_METAL)
    cell.add((trace, trace_top))

def d_gca(cell: gdspy.Cell, loc, num_fingers, finger_length, spring_length, metalize ):
    rotor = tools.GCA(cell.name + str(loc), lib, num_fingers=num_fingers, finger_length=finger_length, spring_length=spring_length, anchor_width=150)
    rotor.draw()
   
    flex = rotor.flexure_ref[0].ref_cell.anchor
    flex.translate(0, -rotor.anchor_width/2 + 10)
    text = gdspy.Text(f'N {num_fingers}  L {finger_length} S {spring_length}', 30, [rotor.get_bounding_box()[0,0], rotor.get_bounding_box()[1,1] + 40], layer = METAL, datatype=0)
    rotor.add(text)
    not_dummy = tools.compute_anchors(rotor, -10, layer = tools.SOI, output_layer= tools.NOT_DUMMY, output_datatype=rotor.datatype, do_math = False)

    if metalize:
        rotor.metalize(6, top_metal_layer=None)
    # rect = gdspy.Rectangle(rotor.get_bounding_box()[0,:], rotor.get_bounding_box()[1,:])
    # rect = gdspy.offset(rect, 10, layer = tools.NOT_DUMMY)
    # rotor.add(rect)
    cell.add(gdspy.CellReference(rotor, loc))

def d_shuttle(cell:gdspy.Cell, loc, rs, spring_length, spring_number):
    shuttle = tools.RotShuttle(cell.name +str(loc), lib, (0,0), rs[0], rs[1], etch_hole_params={'num_of_points': 12})
    shuttle.draw_shuttle()
    shuttle.draw_teeth()
    if rs[0]>0:
        if not (spring_length<100 and spring_number ==4):
            
            shuttle.spiralspring(spring_length, 3, rs[0])
            if spring_number==2:
                shuttle.spiral.opposite_spring()
            

            if spring_number == 4:
                shuttle.spiral.opposite_spring()
                shuttle.spiral.opposite_spring(np.pi/2)
                shuttle.spiral.opposite_spring(-np.pi/2)
    hole = gdspy.Round((0,0), rs[1]-10, number_of_points=12, layer = TOP_VIA, datatype=0)
    shuttle.add(hole)
    circle = gdspy.Round((0,0), rs[1]+10, layer = tools.NOT_DUMMY, datatype=0)
    shuttle.add(circle)

    outside_seal = gdspy.Round((0,0), rs[1] + 20 + 100, rs[1] + 30, number_of_points=60, layer = METAL, datatype =0)
    t_outside_seal = gdspy.offset(outside_seal, -15, layer = TOP_METAL, datatype=0, join_first=True)
    shuttle.add((outside_seal, t_outside_seal))
    text = gdspy.Text(f'i {rs[0]}, o {rs[1]}, l {spring_length}', 30, [0, rs[1]+120], layer = METAL, datatype=0)
    shuttle.add(text)
    cell.add(gdspy.CellReference(shuttle, loc))
# %% 
# %% lower die
# %%
shuttle_ri = [0, 100, 100, 0]#, 250, 100]
shuttle_ro = [200, 200, 500, 750]#, 750, 500]
N_fingers = [50, 50, 50, 50, 50]#, 100]
metalize = [True, False, False, False]#, False, False]
trace_width = [300, 500, 300, 300]#, 300, 300]


# shuttle_ri = [100]
# shuttle_ro = [500]
# N_fingers = [100]
# metalize = [False]
# trace_width = [300]
args = (shuttle_ri, shuttle_ro, N_fingers, metalize, trace_width)
# %%
# array_for_die_np([1.9e4,1e4], d_full_motor, args, pitch = [5.3e3, 6e3])
cells = produce_cells_np(d_full_motor, args)
# %%
place_cells(die_top, cells[0:3], origin  = (-8e3, -9.9e3), gap = 200)
# %% 

trace_width = [300,300,300, 500,500,500]#, 500]
neck_width = [50,100,150, 100, 100, 100] #, 20]
alignment_tol = [100, 100, 100, 50, 100, 200]#, 200]
cells = produce_cells_np(d_bridge, (trace_width, neck_width, alignment_tol))

place_cells(die_top, cells, origin = (-8.3e3, -2.5e3), gap = 200)
# %%
trace_widths = [50, 100, 200, 300, 400]
cells = produce_cells_np(d_contact, (trace_widths,))
place_cells(die_top, cells, origin = (-8.3e3, 3e3), gap = 200)
# %% 
num_fingers = [50]
finger_length = [50, 75, 100]
spring_length = [240, 440, 340]
metalize = [True]
gca_cell = lib.new_cell('GCAs2')


array_for_die([3e3, 5e3], lambda *args: d_gca(gca_cell, *args), (num_fingers, finger_length, spring_length, metalize), pitch =[1e3,1.5e3])

bonder_strip = lib.new_cell('bonder_strip')
width = 200
metal = gdspy.Rectangle([-1400,0], [1400, width], layer = METAL)
top_metal = gdspy.offset(metal, -40, layer = TOP_METAL)
bonder_strip.add((metal, top_metal))
bb  = gca_cell.get_bounding_box()
gca_cell.add(gdspy.CellReference(bonder_strip, (0.5*(bb[0,0] + bb[1,0]), bb[0,1]-(width + 100))))
gca_cell.add(gdspy.CellReference(bonder_strip, (0.5*(bb[0,0] + bb[1,0]), bb[1,1]+100)))


frame = add_isolation_seal(gca_cell, 100, 40)
gca_cell.add(frame)
place_cells(die_top, [gca_cell], origin = (5700,3000), gap = 200)
 # %%
r_in_r_out = [(50, 200), (100, 500), (200,500)]
spring_lengths = [100, 200, 500]
spring_number = [2,4]

Rs =[*[(50,200)],*[(100, 200)]*6, *[(200, 500)]*4, *[(400, 500)]*5] 
Spring_lengths= [100, *[100,200,500]*2, *[200,1500]*2, *[500,1500, 2000]  ]
Spring_number = [2, *[2]*3, *[4]*3 , *[2]*2, *[4]*2, *[2]*3, *[4]*2]


shuttles = produce_cells_np(d_shuttle, (Rs, Spring_lengths, Spring_number))
shut_cell = lib.new_cell('shuttles')
place_cells(shut_cell, shuttles, (0,0), 50 )
frame = add_isolation_seal(shut_cell, 100,20)
shut_cell.add(frame)
place_cells(die_top, [shut_cell],  (-7500, 8200), 200)
# array_for_die_np([1e4,1e4], lambda *args: d_shuttle(shuttles_2, *args), (Rs, Spring_lengths, Spring_number), pitch = [2e3, 2e3])


# %% add litho test structures

layers = [SOI_HOLE, TOP_VIA, METAL, TOP_METAL]
sizes = [[2,4,10,20], [2,4,10,20,40], [4,8, 12, 20], [4,8,12,20]]
reverse= [False, True, False, True]
grid = [False, False, True, True]
for i in range(4):
    layer = layers[i]
    litho = mask.LithoStrucs('litho'+ str(layer), lib)
    litho.exposure_test(layer, (0,0), feature =sizes[i], datatype=0, text_size=40, grid = grid[i])
    bb = litho.get_bounding_box()
    center = np.mean(bb, axis =0)
    print(center)
    if reverse[i]:
        litho_ref = myGdspy.y_reflection(litho, lib, center)
        litho2 = lib.new_cell('litho2' + str(layer))
        litho2.add(litho_ref)
    else:
        litho2 = litho
    if layer == SOI_HOLE:
        bb = litho2.get_bounding_box()
        rect = gdspy.Rectangle(bb[0,:], bb[1,:])
        rect = gdspy.offset(rect, 10, layer = NOT_DUMMY)
        litho2.add(rect)
        rect = gdspy.offset(rect, 30, layer = SOI)
        litho2.add(rect)
    die_top.add(gdspy.CellReference(litho2, (9000, 1000 *i)))
# %% 
# %% add alignment marks for mla

mla = mask.Mla150(lib = lib)
pos = [(-8200, 8500), (8200,8500), (-9e3, -8e3), (9e3, -8e3)]
mla.add_am(origin=pos, layer = METAL, datatype=0)
mla.add_am(origin=pos, layer = TOP_METAL, datatype=0)

pos = [(-9e3, 7e3), (9200,7000), (-9e3, -7e3), (9e3, -7e3)]
mla.add_am(origin = pos, layer = TOP_VIA, datatype=0, am_type='tsa')
mla.add_am(origin = pos, layer = SOI_HOLE, datatype=0, am_type='tsa', frame = [SOI, NOT_DUMMY])

# %% add not dummy
nd = gdspy.Rectangle((-10e3, -10e3), (10e3, 10e3), layer = 0, datatype=1)
die_top.add(nd)

dummy_rect = gdspy.Rectangle((-12.5e3, -12.5e3), (12.5e3, 12.5e3), layer = DUMMY, datatype=0)
die_top.add(dummy_rect)
# %%
die_top.add(gdspy.CellReference(mla))
# %%

lib.write_gds('20220629_die.gds')
# %%
os.startfile('20220629_die.gds')
# %%
# cell = lib.new_cell('rrr')
# text = gdspy.Text('R', 10, (20,40))
# cell.add(text)
# cell2 = lib.new_cell('rr2')
# cell2.add(myGdspy.y_reflection(cell, lib, (20,40)))
# %%
