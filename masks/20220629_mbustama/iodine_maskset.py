"""This file is to be run after mask_gen drc file, since layer math is a lot faster with that method"""
# %%
import os
import sys

from cv2 import trace
from matplotlib.transforms import Bbox






module_path = os.path.abspath(os.path.join('../..'))
if module_path not in sys.path:
    sys.path.append(module_path)

# %%
import layout.motor_tools as tools
import layout.hydrophobic_tools as hydro
import layout.masktools.masktools as mask 
import layout.myGdspy as myGdspy
import gdspy
import numpy as np
import generator as generator
import itertools

# %%
from IPython.display import SVG ##to display layout directly

# %% define post drc layers
SOI_DRC = 101
METAL_DRC =102
TOP_METAL_DRC =106
TOP_VIA_DRC = 107


# %% load die

PATH = os.path.dirname(os.path.abspath(__file__))

rel_name = '20220629_postdrcmath.gds'

fname = os.path.join(PATH, rel_name)
die_lib = gdspy.GdsLibrary(infile=fname)
die_top = die_lib.cells['DIE']
# %% 

# %% 
def make_mask(cell: gdspy.Cell, layer, reticle_am =(1,1), mag =5, reflect= False):
    """layer makes a mask for gcaws"""
    cell = cell.flatten()
    polygons = cell.get_polygons(by_spec = True)
    polys = polygons[(layer,1)]
    frame = polygons[reticle_am]
    output_lib = gdspy.GdsLibrary(f'mask_{layer}')
    gdspy.current_library = output_lib
    base_cell = gdspy.Cell('layout', exclude_from_current = True)
    base_cell.add(gdspy.boolean(polys, None, 'or', layer = layer, datatype=0))
    if reflect:
        base_ref = myGdspy.y_reflection(base_cell, output_lib)
    else:
        base_ref = gdspy.CellReference(base_cell)
    
    res = gdspy.boolean(base_ref, frame, 'or', layer = layer, datatype =0)
    cell1x = gdspy.Cell('1x_mask', exclude_from_current=True)
    output_lib.add(cell1x)
    cell1x.add(res)
    output_cell = gdspy.Cell('MASK', exclude_from_current=True)
    output_lib.add(output_cell)
    output_cell.add(gdspy.CellReference(cell1x, magnification=5))
    # purge unused cells
    output_cell.flatten()
    return output_lib


# %%
out = make_mask(die_top, TOP_METAL_DRC, reflect=True)
# %%
out.write_gds(f'20220713top_metal_{TOP_METAL_DRC}.gds', ['MASK'])
os.startfile(f'20220713top_metal_{TOP_METAL_DRC}.gds')
# %%
soi = make_mask(die_top, SOI_DRC,reflect=False )
# %%
soi.write_gds(f'20220713soi_{SOI_DRC}.gds', cells = ['MASK'])
os.startfile(f'20220713soi_{SOI_DRC}.gds')
# %%
metal = make_mask(die_top, METAL_DRC)

# %%
metal.write_gds(f'20220713metal_{METAL_DRC}.gds', cells = ['MASK'])
os.startfile(f'20220713metal_{METAL_DRC}.gds')

# %%
top_via = make_mask(die_top, TOP_VIA_DRC, reflect=True)

# %%
top_via.write_gds(f'20220713top_via_{TOP_VIA_DRC}.gds', cells = ['MASK'])
os.startfile(f'20220713top_via_{TOP_VIA_DRC}.gds')
 # %%
