## make mask for gcaws6 with aml alignmentmarks
# %%
import os
import sys

from cv2 import trace





module_path = os.path.abspath(os.path.join('../..'))
if module_path not in sys.path:
    sys.path.append(module_path)

# %%
import layout.motor_tools as tools
import layout.hydrophobic_tools as hydro
import layout.masktools.masktools as mask 
import layout.myGdspy as myGdspy
import gdspy
import numpy as np
import generator as generator
import itertools

# %%
from IPython.display import SVG ##to display layout directly

# %%
lib = gdspy.GdsLibrary()

SOI = tools.SOI
SOI_HOLE = tools.SOI_HOLE
METAL = tools.METAL
TOP_METAL = tools.TOP_METAL
TOP_VIA = tools.TOP_VIA
DUMMY = tools.DUMMY
NOT_DUMMY = tools.NOT_DUMMY
# %%

die_cell = lib.new_cell('DIE')

# %%
gc_frame = mask.Gcaws6(lib = lib)
gc_frame.add_frame(0,1)
gc_frame.add_reticle_am(0,0)
gc_frame.add_am((-5e3, 3e3), 0,0)
gc_frame.add_am((-5e3, -3e3), 0,0)
gc_frame.add_am((5e3, 3e3), 0,0)
gc_frame.add_am((5e3, -3e3), 0,0)
die_cell.add(gdspy.CellReference(gc_frame))


# %% aml
aml = mask.AML_Bonder(None, lib, layers = [METAL, TOP_METAL])
aml.alignment_mark()
aml.add_landing_strips() 
pols = aml.landing_mark.get_polygons(by_spec=True)
# %% litho_structs 
ls = mask.LithoStrucs(lib = lib)
ls.exposure_test(0, feature= [2,4,10, 20], text_size=30)
die_cell.add(gdspy.CellReference(ls, (5e3, 5e3)))
# %%
met = lib.new_cell('metal_am')
met.add(gdspy.PolygonSet(pols[(METAL,0)], layer =0))
text = lib.new_cell('R')
text.add(gdspy.Text('R', 300, (0,0)))
met.add(gdspy.CellReference(text, (1e3, -1e3)))
die_cell.add(gdspy.CellReference(met, (-5e3,5e3)))

# %% top met
top_met = lib.new_cell('top_metal')
top_met.add(gdspy.PolygonSet(pols[(TOP_METAL,0)], layer =0, datatype=0))
ref = myGdspy.y_reflection(text, lib)
ref.origin = (1.2e3, -1e3)
top_met.add(ref)
die_cell.add(gdspy.CellReference(top_met, (-5e3,-5e3)))
# %%
lib.write_gds('20220629_aml.gds')
# %%
os.startfile('20220629_aml.gds')

# %% write top cell
top = lib.new_cell('TOP')
ref = gdspy.CellReference(die_cell, magnification=5)
big_pols = ref.get_polygons(by_spec=True)

top.add(gdspy.PolygonSet(big_pols[(0,0)], 100, 0))

# %%
lib.write_gds('20220713aml_100.gds', ['TOP'])
os.startfile('20220713aml_100.gds')
# %%
