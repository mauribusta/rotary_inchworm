## this file makes a gds for the mla that is lr flipeed and has a label
# %%
import os
import sys

from cv2 import trace






module_path = os.path.abspath(os.path.join('../..'))
if module_path not in sys.path:
    sys.path.append(module_path)


# %%
import layout.motor_tools as tools
import layout.hydrophobic_tools as hydro
import layout.masktools.masktools as mask 
import layout.myGdspy as myGdspy
import gdspy
import numpy as np
import generator as generator
import itertools

# %%
from IPython.display import SVG ##to display layout directly

# %%
rel_path = '20220713soi_101.gds'
PATH = os.path.dirname(os.path.abspath(__file__))
full_path = os.path.join(PATH, rel_path)
# %% 
lib = gdspy.GdsLibrary(infile = full_path)
# %%
top_cell = lib.top_level()[0]

# %% add label
run = 'IODINE'
layer_n = 'SOI'
date = '2022/07/20'
user = 'MBUSTAMA'
location = -63.5e3 + 10e3

text = gdspy.Text(f'{run}-{layer_n}  {date}  {user}', 3e3, layer = 101)



# %%
text_cell = lib.new_cell('label')
# %%
text_cell.add(text)
# %%
bb = text_cell.get_bounding_box()
# %%
top_cell.add(gdspy.CellReference(text_cell, (location,-np.mean(bb[:,0]) ), rotation=90))
# %%
reverse_cell= lib.new_cell('flip_lr')
# %%
reverse_cell.add(myGdspy.y_reflection(top_cell, lib))
# %%
lib.write_gds('20220713soi_101_lrflipped.gds')
os.startfile('20220713soi_101_lrflipped.gds')
# %%
