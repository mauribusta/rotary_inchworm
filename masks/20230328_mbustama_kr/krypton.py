# %%
import os
import sys

from cv2 import trace





module_path = os.path.abspath(os.path.join('../..'))
if module_path not in sys.path:
    sys.path.append(module_path)

# %%
import layout.motor_tools as tools
import layout.hydrophobic_tools as hydro
import layout.masktools.masktools as mask 
import layout.myGdspy as myGdspy
import gdspy
import numpy as np
import generator as generator
import itertools
# %%
lib = gdspy.GdsLibrary()

SOI = tools.SOI
SOI_HOLE = tools.SOI_HOLE
METAL = tools.METAL
TOP_METAL = tools.TOP_METAL
TOP_VIA = tools.TOP_VIA
DUMMY = tools.DUMMY
NOT_DUMMY = tools.NOT_DUMMY
# %% create top cell
die_top = lib.new_cell('DIE')


# %% add frame of reference for space
gc6 = mask.Gcaws6(lib = lib)
gc6.add_frame(layer = 0, datatype=1)
gc6.add_am((0,-2.8e3), layer = METAL, datatype=0)
gc6.add_am((0, 2.8e3), layer = TOP_METAL, datatype=0)
# gc6.add_reticle_am(layer = METAL, datatype=0)

die_top.add(gdspy.CellReference(gc6))


# %% 
lib.write_gds('20230328_die.gds')
# %%
os.startfile('20220328_die.gds')