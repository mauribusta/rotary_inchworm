# %%
import os
import sys

from cv2 import circle, trace


module_path = os.path.abspath(os.path.join('../..'))
if module_path not in sys.path:
    sys.path.append(module_path)

# %%
import dynamics.motor_analysis as analysis
import layout.motor_tools as tools
import gdspy
import numpy as np
import generator as generator
import layout.hydrophobic_tools.hydrophobic_tools as hydrophbic
# %%
from IPython.display import SVG ##to display layout directly

# %%
lib = gdspy.GdsLibrary()
dies = []
# %%
METAL = 6
TOP_METAL = 8

GUIDE = 120

# %% [markdown]
# Make circles of different radii to test bonding
# %% 

def  place_in_die(cell: gdspy.Cell, lib= lib, die_length = 1e4, die_width = 1e4, layer = GUIDE):
    bb = cell.get_bounding_box()
    center = bb.mean(axis =0)
    out = lib.new_cell(cell.name + '_die')
    rect = gdspy.Rectangle((-die_length/2, -die_width/2), (die_length/2,die_width/2), layer = layer)
    out.add(rect)
    out.add(gdspy.CellReference(cell, -center))

    corner_cell = lib.new_cell(f'corner_cell_' + cell.name)
    corner = gdspy.FlexPath(((-1000,0),(0,0),(0, -1000)), 100, layer = METAL)
    corner2= gdspy.boolean(corner, None, 'or', layer = TOP_METAL)
    corner_cell.add((corner,corner2))
    pt = np.array([die_length/2-50,die_width/2 -50])
    out.add(gdspy.CellReference(corner_cell, pt) )
    out.add(gdspy.CellReference(corner_cell, (pt[0], -pt[1]), -90) )
    out.add(gdspy.CellReference(corner_cell, (-pt[0], -pt[1]), -180) )
    out.add(gdspy.CellReference(corner_cell, (-pt[0], pt[1]), 90) )
    dies.append(out)



    return out

# %% 
import itertools
def array_for_die(xy_dims,  function, args, pitch = [1e3,1e3]):
    values = list(itertools.product(*args))
    cols = int(xy_dims[0]/pitch[0])
    for i, value in enumerate(values):
        loc = (pitch[0] *(i%cols), -(i//cols)* pitch[1])
        function(loc, *value)
# %%
def circle(loc, radius):
    circ = gdspy.Round(loc, radius, layer = METAL, number_of_points= 18)
    circ2 = gdspy.boolean(circ, None, 'or', layer = TOP_METAL)
    text_loc = (circ.get_bounding_box().min(axis=0)[0], circ.get_bounding_box().max(axis=0)[1])
    text_loc = np.array(text_loc) + np.array([0,300])
    text = gdspy.Text(f'r{radius}', 100, text_loc, layer = METAL)
    circle_cell.add((circ, circ2, text))
# %% 
# %%
radii = [20, 50, 100, 150, 200,250, 300, 500, 1000]
circle_cell = lib.new_cell('Circles')
separation = 2e3
separationy = 3e3
# %%
array_for_die([1e4,1e4], circle, (radii,), pitch=[2e3,3e3])
# %%
# die_length = 1e4
# cols = int(die_length/separation)

# for i, radius in enumerate(radii):
#     print((i%cols), i//cols)
#     loc = (separation *(i%cols), -(i//cols)* separationy)
#     print(loc)
#     circ = gdspy.Round(loc, radius, layer = METAL, number_of_points= 18)
#     circ2 = gdspy.boolean(circ, None, 'or', layer = TOP_METAL)
#     text_loc = (circ.get_bounding_box().min(axis=0)[0], circ.get_bounding_box().max(axis=0)[1])
#     text_loc = np.array(text_loc) + np.array([0,300])
#     text = gdspy.Text(f'r{radius}', 100, text_loc, layer = METAL)
#     circle_cell.add((circ, circ2, text))


# %%
circle_die = place_in_die(circle_cell, lib)


# %%
circle_die.write_svg('temp.svg', scaling = 0.2)
# SVG('temp.svg')


# %% 

ring_cell = lib.new_cell('Rings', overwrite_duplicate=True )

# %%
radii = [20, 50, 100, 150]
thick = [10, 20, 30, 50]
separationy = 3e3

# %%
def rings(cell:gdspy.Cell, loc, radius, thickness):
    if thickness > 0.8*radius:
        return None
    circ = gdspy.Round(loc, radius, inner_radius=radius - thickness,  layer = METAL, number_of_points= 36)
    circ2 = gdspy.boolean(circ, None, 'or', layer = TOP_METAL)
    text_loc = (circ.get_bounding_box().min(axis=0)[0], circ.get_bounding_box().max(axis=0)[1])
    text_loc = np.array(text_loc) + np.array([0,300])
    text = gdspy.Text(f'r{radius}', 100, text_loc + np.array([0, 200]), layer = METAL)
    text2 = gdspy.Text(f't{thickness}', 100, text_loc, layer = METAL)
    cell.add((circ, circ2, text, text2))

# %%
array_for_die([1e4, 1e4], lambda *args:  rings(ring_cell, *args), (radii, thick), [1.5e3, 1.5e3])

# %%
ring_die = place_in_die(ring_cell)
# %%
ring_die.write_svg('temp.svg', scaling = 0.2)
# SVG('temp.svg')

# %%
rings2= lib.new_cell('rings2')
radii = [250, 500, 1000]
thick = [50, 100, 200]
array_for_die([1e4, 1e4], lambda *args:  rings(rings2, *args), (radii, thick), [3e3, 3e3])

# %% 
rings2_die = place_in_die(rings2)
# %%
rings2_die.write_svg('temp.svg', scaling = 0.2)
# SVG('temp.svg')
# %%

def rectangle_rings(cell: gdspy.Cell,loc, length, width, trace_width):
    if (trace_width>length) and (trace_width>width):
        return None
    # circ = gdspy.Round(loc, radius, inner_radius=radius - thickness,  layer = METAL, number_of_points= 36)
    loc = np.array(loc)
    rect = gdspy.Rectangle(loc, loc + np.array([width, length]), layer = METAL)
    inset = gdspy.offset(rect, -trace_width)

    circ = gdspy.boolean(rect, inset, 'not', layer = METAL)
    circ2 = gdspy.boolean(circ, None, 'or', layer = TOP_METAL)
    text_loc = (circ.get_bounding_box().min(axis=0)[0], circ.get_bounding_box().max(axis=0)[1])
    text_loc = np.array(text_loc) + np.array([0,300])
    text = gdspy.Text(f'l{length}, w{width}', 100, text_loc + np.array([0, 200]), layer = METAL)
    text2 = gdspy.Text(f't{trace_width}', 100, text_loc, layer = METAL)
    cell.add((circ, circ2, text, text2))


# %%
rrings_cell = lib.new_cell('rrings')
# %%
lengths = [50, 100, 150, 200, 250, 500]
widths = [500, 750, 1000]
trace_widths = [20, 40, 60]

# %%
array_for_die([1e4, 1e4], lambda *args:  rectangle_rings(rrings_cell, *args), (lengths, widths, trace_widths), [1.2e3, 1.5e3])
# %%
rrings_die = place_in_die(rrings_cell)
rrings_die.write_svg('temp.svg', scaling = 0.2)
# SVG('temp.svg')


# %%
rrings_cell2 = lib.new_cell('rrings2')

# %%
lengths = [250, 500, 1000]
widths = [500, 750, 1000]
trace_widths = [50, 100, 150, 200]
array_for_die([1e4, 1e4], lambda *args:  rectangle_rings(rrings_cell2, *args), (lengths, widths, trace_widths), [1.2e3, 2e3])
# %%
rrings_die2 = place_in_die(rrings_cell2)
rrings_die2.write_svg('temp.svg', scaling = 0.2)
# %%
# SVG('temp.svg')

def square_ring(cell, loc, side_length, trace_width):
    return rectangle_rings(cell, loc, side_length, side_length, trace_width)
#  %% 

# %% 

side_lengths = [500, 1000, 1500, 5000, 5000]
widths = [[100, 200], [100, 200, 300], [125,250, 500], [200], [500] ]

for i, side_length in enumerate(side_lengths):
    cell = lib.new_cell(f'seal_ring_{side_length}_{i}')
    width =  widths [i]
    pitch = side_length*2
    array_for_die([1e4, 1e4], lambda *args: square_ring(cell, *args), ([side_length], width), [pitch, pitch])
    die = place_in_die(cell)
# %%

def ring_with_fingers(cell, loc, radius, width, finger_length, finger_width):
    if width > 0.8*radius:
        return None
    circ = gdspy.Round(loc, radius,  layer = METAL, number_of_points= 36)
    inner_circ = gdspy.offset(circ, -width)
    fings, shape = hydrophbic.add_fingers_to_shape(circ, lib, finger_length, finger_width, finger_width, layer = METAL)
    circ =gdspy.boolean(circ, inner_circ, 'not', layer = METAL)
    circ2 = gdspy.boolean(circ, fings, 'or', layer = TOP_METAL)
    text_loc = (circ.get_bounding_box().min(axis=0)[0], circ.get_bounding_box().max(axis=0)[1])
    text_loc = np.array(text_loc) + np.array([0,300])
    text = gdspy.Text(f'l{finger_length}, w{finger_width}', 100, text_loc, layer = METAL)
    cell.add((circ, circ2, text))
    cell.add(fings)
# %%
radius =[500]
widths = [100, 200]
finger_length = [10, 20, 50]
finger_width = [3, 6, 9, 12]
# %%

for i,width in enumerate(widths):
    cell = lib.new_cell(f'fingers_ring_{side_length}_{i}')
    array_for_die([1e4,1e4], lambda *args: ring_with_fingers(cell, *args), (radius, [width], finger_length, finger_width), [2e3, 2e3] )
    place_in_die(cell)


additional_cell = lib.new_cell('full_device')
box = gdspy.Rectangle((0,0), (10,10), layer = 120)
additional_cell.add(box)
place_in_die(additional_cell)

# %%
master_cell = lib.new_cell('TOP')
for i,die in enumerate(dies):
    ref = gdspy.CellReference(die, np.array([i*1.1e4, 0]))
    master_cell.add(ref)


# %%
lib.write_gds('temp.gds')
# %%
