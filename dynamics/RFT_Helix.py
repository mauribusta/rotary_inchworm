import numpy as np
# import matplotlib
# matplotlib.rcParams['text.usetex'] = True
import matplotlib.pyplot as plt

def compute_Cstar(lambda_R, a_R):
    """ computes tangential and normal non-dimensionalized coefficients, by Gray and Hancock"""
    Ctstar = 2*np.pi/(np.log(2*lambda_R/a_R)- 0.5)
    Cnstar = 4*np.pi/(np.log(2*lambda_R/a_R) +0.5)
    # self.a_R = a_R
    # self.Ctstar = Ctstar
    # self.Cnstar = Cnstar
    return Ctstar, Cnstar
def compute_FTDstar(L_R, lambda_R, a_R):
    Ctstar, Cnstar = compute_Cstar(lambda_R, a_R)
    theta = np.arctan(2*np.pi/lambda_R)
    Fstar = (Cnstar-Ctstar)*np.sin(theta) *L_R
    Tstar = (Cnstar*np.cos(theta)**2 + Ctstar*np.sin(theta)**2)* L_R/np.cos(theta)
    Dstar = (Cnstar*np.sin(theta)**2 + Ctstar*np.cos(theta)**2)* L_R/np.cos(theta)
    return Fstar, Tstar, Dstar
def compute_F_Tstar(lambda_R, a_R):
    L_R = 1
    Fstar, Tstar, _ = compute_F_Tstar(1, lambda_R, a_R)
    F_Tstar = Fstar/Tstar
    return F_Tstar
def dimensionalizeFTD (Fstar,Tstar,Dstar, R, mu, U, Omega):
    F = Fstar * mu * R**2 * Omega
    T = Tstar * mu * R**3 *Omega
    D = U*mu *R*Dstar
    return F,T,D
    

def plot_FTD(fig,F, T, D, x, xaxis = 'lambda_R', ax = None):
    if ax is None:
        ax = fig.subplots(3,1)
    ax[0].plot(x, F)
    ax[0].set_ylabel(r'$\frac{F}{\mu R^2 \Omega}$', fontsize=14, rotation =0 )
    ax[0].set_ylim([0,30])
    ax[1].plot(x, T)
    ax[1].set_ylim([0, 230])
    ax[1].set_ylabel(r'$\frac{T}{\mu R^3 \Omega}$',fontsize=14, rotation =0)
    ax[2].plot(x, D)
    ax[2].set_ylim([0,100])
    ax[2].set_ylabel(r'$\frac{D}{\mu U R}$', fontsize=14, rotation =0)
    if xaxis =='lambda_R':
        ax[2].set_xlabel(r'$\frac{\lambda}{R}$')
    elif xaxis == 'L_R':
        ax[2].set_xlabel(r'$\frac{L}{R}$')
    elif xaxis == 'a_R':
        ax[2].set_xlabel(r'$a/R$')
    elif xaxis =='L_lambda':
        ax[2].set_xlabel(r'$L/\lambda$')
    return fig, ax


