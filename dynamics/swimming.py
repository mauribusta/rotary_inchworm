import numpy as np
# import RFT_Helix
from dynamics import RFT_Helix

from matplotlib import ticker

def thrust( input_torque, R,lambda_R, a_R):
    F_Tstar = RFT_Helix.compute_F_Tstar(lambda_R, a_R)
    thrust = input_torque * F_Tstar /R
    return thrust

def angular_vel(input_torque, R, lambda_R, a_R, L_R, mu):
    _, Tstar, _  = RFT_Helix.compute_FTDstar(L_R, lambda_R, a_R)
    Omega = input_torque/(Tstar*mu*R**3)
    return Omega

def swimming_dynamics_nofriction(input_torque, R, lambda_R, a_R, L_R, d_sub, mu):
    """ takes in geometry and torque, outputs swimming speeds and forces. Does not consider any drag on the shuttle
    inputs: input_torque, R, lambda_R, a_R, L_R, d_sub, mu 
    outputs: Omega(angular velocity), Thrust, U, substrate_drag, flagellar_drag""" 

    Fstar, Tstar, Dstar  = RFT_Helix.compute_FTDstar(L_R, lambda_R, a_R)
    Omega = input_torque/(Tstar*mu*R**3)
    Thrust = Fstar/Tstar * input_torque/R
    U = Thrust/(mu * (3*np.pi*d_sub + Dstar *R))
    substrate_drag = mu*U*3*np.pi*d_sub
    flagellar_drag = mu*Dstar*R*U


    return input_torque,Omega, Thrust, U, substrate_drag, flagellar_drag

def plot_swimming_dyanmics(dynamics,x, fig, ax =None, xaxis = 'Torque'):
    input_torque, Omega, Thrust, U, substrate_drag, flagellar_drag = dynamics 

    if ax is None:
        ax = fig.subplots(5,1)
    ax[0].plot(x, U)
    ax[0].set_ylabel(r'Velocity (m/s)', fontsize=14)
    # ax2 = ax[0].twinx()
    # mn, mx = ax[0].get_ylim()
    # ax2.set_ylim(mn/d_sub, mx/d_sub)
    # ax2.set_ylabel('(body lengths)/s')
    # ax[0].set_ylim([0,30])
    ax[1].plot(x, Thrust)
    # ax[1].set_ylim([0, 230])
    ax[1].set_ylabel(r'Thrust (N)',fontsize=14)
    ax[2].plot(x, substrate_drag, '-')
    color = ax[2].lines[-1].get_color()
    ax[2].plot(x, flagellar_drag, '-.', color = color)
    ax[2].set_ylabel(r'Drag (N)', fontsize=14 )
    ax[2].legend(['substrate', 'Flagella'])
    ax[3].plot(x, Omega*60/(2*np.pi))
    ax[3].set_ylabel('Rotation Rate (rpm)',fontsize=14)
    ax[4].plot(x, input_torque)
    ax[4].set_ylabel('Torque (Nm)',fontsize=14 )
    if xaxis == 'Torque':
        ax[-1].set_xlabel('Input torque (Nm)',fontsize=14)
    if xaxis == 'Velocity':
        ax[-1].set_xlabel('Swimming velocity (m/s)',fontsize=14)
    if xaxis == 'rpm':
        for thisax in ax:
            thisax.set_xlabel('Angular velocity (rpm)',fontsize=14)
   
    for axis in ax:
        formatter = ticker.ScalarFormatter(useMathText=True)
        formatter.set_scientific(True)      
        formatter.set_powerlimits((-1,2)) 
        axis.yaxis.set_major_formatter(formatter)

    # ax[2].set_ylim([0,100])
    # ax[2].set_ylabel(r'$\frac{D}{\mu U R}$', fontsize=14, rotation =0)
    # if xaxis =='lambda_R':
    #     ax[2].set_xlabel(r'$\frac{\lambda}{R}$')
    # elif xaxis == 'L_R':
    #     ax[2].set_xlabel(r'$\frac{L}{R}$')
    # elif xaxis == 'a_R':
    #     ax[2].set_xlabel(r'$a/R$')
    # elif xaxis =='L_lambda':
    #     ax[2].set_xlabel(r'$L/\lambda$')
    return fig, ax





def taylor_couette_b1(mu, R, h,g):
    b1 = 2 * np.pi *mu * R **2 * h * (2*R**2 + 2*R*g* + g**2)/(2*R*g + g**2)
    return b1 
def Taylor_number(rho_f, mu, Omega, R1, g):
    nu = mu/rho_f
    Ta = Omega**2 * R1 *g**3/nu**2
    return Ta

def couette_disk_b2(mu, R, t):
    b2 = np.pi * mu * R**4/(2*t)
    return b2
def swimming_dynamics_ss(input_torque,flagella, shuttle,substrate, fluid ):
    """ takes in geometry and torque, outputs swimming speeds and forces. Does not consider any drag on the shuttle
    inputs: input_torque,
    flagella = dictionary with all parameters R, lambda_R, a_R, L_R 
    shuttle = dicitionary with parameters R, side_gap, bottom_gap, height, rho
    substrate = dictionary with parameters L, W, H, rho
    fluid: rho, mu
    outputs: Omega(angular velocity), Thrust, U, substrate_drag, flagellar_drag""" 
    b1 = taylor_couette_b1(fluid['mu'], shuttle['R'], shuttle['height'], shuttle['side_gap'])
    b2 = couette_disk_b2(fluid['mu'], shuttle['R'], shuttle['bottom_gap'])
    Fstar, Tstar, Dstar  = RFT_Helix.compute_FTDstar(flagella['L_R'], flagella['lambda_R'], flagella['a_R'])
    
    Omega = input_torque/(Tstar*flagella['R']**3 *fluid['mu'] + b1 + b2)
    Thrust = Fstar*fluid['mu']*flagella['R']**2 * Omega
    substrate_drag_coef = substrate_stokes_drag(fluid['mu'], substrate['L'],substrate['W'], substrate['H'])
    linear_drag_coefficient = substrate_drag_coef + Dstar * fluid['mu']*flagella['R']
    U = Thrust/linear_drag_coefficient
    substrate_drag = substrate_drag_coef*U
    flagellar_drag = Dstar * fluid['mu']*flagella['R']*U
    return input_torque,Omega, Thrust, U, substrate_drag, flagellar_drag

def swimming_dynamics_ss_omega(Omega,flagella, shuttle,substrate, fluid):
    """ takes in geometry and torque, outputs swimming speeds and forces. Does not consider any drag on the shuttle
    inputs: omega,
    flagella = dictionary with all parameters R, lambda_R, a_R, L_R 
    shuttle = dicitionary with parameters R, side_gap, bottom_gap, height, rho
    substrate = dictionary with parameters L, W, H, rho
    fluid: rho, mu
    outputs: Omega(angular velocity), Thrust, U, substrate_drag, flagellar_drag""" 
    b1 = taylor_couette_b1(fluid['mu'], shuttle['R'], shuttle['height'], shuttle['side_gap'])
    b2 = couette_disk_b2(fluid['mu'], shuttle['R'], shuttle['bottom_gap'])
    Fstar, Tstar, Dstar  = RFT_Helix.compute_FTDstar(flagella['L_R'], flagella['lambda_R'], flagella['a_R'])
    b3 = Tstar*flagella['R']**3 *fluid['mu']
    torque = Omega*(b3 + b1 + b2)
    print(b3/(b1+b2+b3)*100, b1/(b1+b2+b3)*100, b2/(b1+b2+b3)*100)
    Thrust = Fstar*fluid['mu']*flagella['R']**2 * Omega
    substrate_drag_coef = substrate_stokes_drag(fluid['mu'], substrate['L'],substrate['W'], substrate['H'])
    linear_drag_coefficient = substrate_drag_coef + Dstar * fluid['mu']*flagella['R']
    U = Thrust/linear_drag_coefficient
    substrate_drag = substrate_drag_coef*U
    flagellar_drag = Dstar * fluid['mu']*flagella['R']*U
    return torque ,Omega, Thrust, U, substrate_drag, flagellar_drag
def substrate_stokes_drag(mu, L,W,H):
    """computes stokes drag on arbitrary substrate, L,W are considered perpendicular to flow, H is parallel
    Based on 1987 leith paper""" 
    d_norm = np.sqrt(4*L*W/np.pi)
    surface_area = 2*L*W * 2*(L+W)*H 
    d_s = np.sqrt(surface_area/np.pi)
    drag_coefficient = 3*np.pi*mu*(d_norm/3 + 2*d_s/3)
    return drag_coefficient

