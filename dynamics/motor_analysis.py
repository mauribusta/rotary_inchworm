import matplotlib
import numpy as np
import scipy.optimize as opt
import scipy.integrate as inte
import logging

from numba import jit



class Inchworm:
    """Object to represent and inchworm motor with given parameters 
    Expressions based on Penskiy 2012
    Class variables
    ---------------
    epsilon_0: permitivity of free space
    rho_silicon: density of silicon
    E_silicon: youngs modulus of silicon
    Parameters
    ----------
    MF: minimum feature size
    arm_angle: (alpha in Pensiky) angle arm actuation angle
    width: figer width
    y: displacement at which to compute force. 2*MF if none is provided
    L_overlap: overlap length for electrode fingers
    gap_final: final gap, set to 1um by standard
    num_fingers = number of pairs of electorde 
    max_voltage : maximum voltage applied, 110 set for air
    buckling_safety: safety factor to avoid buckling of flexible arm, n in Penskiy
    beam_length_factor: K_b in penskiy, based on fixed pivot condition
    epsilon: relative permitivity of medium (1 for air)
    DRIE_aspect : aspect ratio allowed by DRIE
    rho_medium: density of medium
    k_back : back side gap factor, 1.5 
    Delta_x: MF by default: step size of inchworm
    num_act: 2 by default=
    arm_width: (b) in penskiy if none set to min feature size
    f_res: resonance frequency (10 kHz by default)

    """
    epsilon_0 = 8.85418782e-12 # F/m
    rho_silicon = 2320 #kg/m3
    E_silicon = 170e9 #Pa
    max_thickness = 500e-6
    def __init__(
        self, MF, arm_angle, width, y = None, L_overlap = None,  gap_final = None, num_of_fingers=40, max_voltage = 110,  buckling_safety = 2, beam_length_factor = 0.7, 
        epsilon = 1,rho_medium = 0, DRIE_aspect = 20, k_back = 1.5, Delta_x = None, num_act =2, arm_width = None, f_res = 10e3,
        L_sup = None, L_frame = None, L_anchor = None, thickness = None
        ):
        

        self.MF = MF
        self.arm_angle = arm_angle
        self.width = width
        if y is None:
            self.y = 2*self.MF
        else:
            self.y = y
        if gap_final is None:
            gap_final = self.MF
        self.gap_final = gap_final
        self.num_of_fingers = num_of_fingers
        self.max_voltage = max_voltage
        self.buckling_safety = buckling_safety
        self.beam_length_factor= beam_length_factor
        self.DRIE_aspect = DRIE_aspect
        self.rho_medium = rho_medium
        self.permitivity = epsilon*self.epsilon_0 
       
        self.k_back = k_back
        self.thickness = thickness
        if Delta_x is None:
            self.Delta_x = self.MF
        else: 
            self.Delta_x = Delta_x
        self.num_act = num_act
        if arm_width is None:
            self.arm_width = self.MF
        else:
            self.arm_width = arm_width
        self.f_res = f_res
        self.Delta_y = self.Delta_x/np.tan(np.radians(self.arm_angle))
        self.L_overlap = L_overlap
        self.L_sup = L_sup
        self.L_frame = L_frame
        self.L_anchor = L_anchor
        self.compute()
    def compute(self):
        ##perform computations
        self.comp_g1()
        self.comp_g2()
        if self.thickness is None:
            self.comp_thickness()
        self.comp_finger_L_max()
        if self.L_overlap is None:
            self.L_overlap = self.L_max
            logging.debug('L_overlap set to maximum')
        self.comp_f_load_per_area()
        self.comp_efficiency()
        # print(self.MF, self.arm_angle, self.width, self.L_overlap,self.L_max, self.f_load_per_area)

    def comp_g1(self):
        """Equation 7 in penskiy"""
        self.g1= 2*self.MF + self.Delta_y + self.gap_final
        return self.g1
    def comp_g2(self):
        """ back gap"""
        self.g2 = self.g1 * self.k_back
        return self.g2
    def comp_thickness(self):
        """compute thickness"""
        self.thickness = self.DRIE_aspect*self.MF # from eq (9)
        return self.thickness

    def comp_finger_L_max(self):
        """Equation 10 in penskiy"""
        L_max =  (0.28*self.E_silicon * self.width**3 *self.gap_final**3/(self.permitivity * self.max_voltage**2 *(1+ 0.42*self.gap_final/ self.thickness)) )**(1/4)
        self.L_max = L_max
        return L_max
    def comp_k_spr_max(self):
        """eq (11) in Pensiky"""
        k_spr_max = 27/8 * self.permitivity*self.thickness* self.L_overlap* self.num_of_fingers/(self.g1**3) * self.max_voltage**2
        self.k_spr_max = k_spr_max
        return k_spr_max
    def comp_k_spr(self):
        """eq (12) in spensiky"""
        L_overlap = self.L_overlap
        f_res = self.f_res
        k_spr = (2*np.pi*f_res)**2 * self.rho_silicon*self.thickness* L_overlap*self.width*self.num_of_fingers
        self.k_spr = k_spr
        self.comp_k_spr_max()
        if k_spr>self.k_spr_max:
            logging.warning('Spring constant too high for pull in. Resonance is changed')
            self.k_spr = self.k_spr_max
        return self.k_spr
    def comp_electrode_area(self):
        """from equation 6"""
        L_overlap = self.L_overlap
        area = self.num_act* self.num_of_fingers *(2*self.width + self.g1 + self.g2)* L_overlap
        self.electrode_area = area
        return area
    def force_el_st(self, y, L_overlap, voltage=None):
        """ electrostatic force"""
        if voltage is None:
            voltage = self.max_voltage
        force = 1/2* self.permitivity*self.num_of_fingers*voltage**2* L_overlap * self.thickness*(1/(self.g1 - y)**2 - 1/(self.g2+y)**2)
        return force
    def force_y(self):
        """eq 5 in Penskiy"""
        L_overlap = self.L_overlap
        y = self.y
        force_el_st = self.force_el_st(y, L_overlap)
        k_spr = self.comp_k_spr()
        force_y = force_el_st - k_spr* self.Delta_y
        if force_y<0:
            logging.warning(f'Negative Y-force for MF: {self.MF}, angle(deg){self.arm_angle}, (width) {self.width}, (l_overlap){self.L_overlap}')
        return force_y
    def comp_L_arm(self):
        """torsional spring to witholdbuckling"""
        L_overlap = self.L_overlap
        y = self.y
        force_y = self.force_y()
        if force_y<=0:
            L_arm = 1e-6
            # logging.warning(f'Negative Y-force for MF: {self.MF}, angle(deg){self.arm_angle}, (width) {self.width}, (l_overlap){self.L_overlap}')
        else:
            self.I_arm = 1/12* self.thickness*self.arm_width**3
            L_arm = np.sqrt(self.E_silicon* self.I_arm * np.pi**2 * np.sin(np.radians(self.arm_angle))/(self.buckling_safety*force_y*self.beam_length_factor**2))
        self.L_arm = L_arm
        return L_arm
    def comp_k_tors(self):
        L_overlap = self.L_overlap
        y = self.y
        L_arm = self.comp_L_arm()
        force_y = self.force_y()
        k_tors = self.buckling_safety* force_y* self.beam_length_factor**2 * L_arm/(np.pi**2 * np.sin(np.radians(self.arm_angle)))
        k_tors = self.E_silicon*1/12*self.thickness*self.arm_width**3/self.L_arm
        self.k_tors = k_tors
        return k_tors
    def comp_force_load(self):
        force_y = self.force_y()
        if force_y<=0:
            self.force_load =0
            return 0
        k_tors= self.comp_k_tors()
        L_arm = self.L_arm
        force_load = force_y/np.tan(np.radians(self.arm_angle)) #- k_tors* self.Delta_x/(L_arm**2 * np.sin(np.radians(self.arm_angle))**2)
        self.force_load = force_load
        return force_load
    def comp_total_area(self, L_sup, L_frame, L_anchor = 50e-6):
        area = self.num_act*(self.num_of_fingers*(2*self.width+self.g1+self.g2) + self.L_arm*np.sin(np.radians(self.arm_angle))) *(self.L_overlap + L_sup+L_frame +L_anchor)
        return area

    def comp_f_load_per_area(self):
        f_load = self.comp_force_load()
        area = self.comp_electrode_area()
        self.f_load_per_area = f_load/area
        return f_load/area

    def comp_f_load_per_total_area(self):
        f_load = self.comp_force_load()
        tarea = self.comp_total_area(self.L_sup, self.L_frame, self.L_anchor)
        self.f_load_per_total_area = f_load/tarea
        return f_load/tarea
    def comp_efficiency(self):
        work = self.force_load*self.Delta_x
        input_energy = 0.5*self.permitivity*self.num_of_fingers*self.thickness*self.L_overlap*self.max_voltage**2/self.gap_final
        self.efficiency = work/input_energy
        self._maxefficiency = self.gap_final*self.Delta_y/((self.Delta_y+self.gap_final)**2)
    def __str__(self):
        try:
            self.comp_f_load_per_total_area()
        except:
            self.f_load_per_total_area = 0
        string = f'''Object representing motor with parameters:\n
        PROCESS: MF = {self.MF*1e6:.2f}um, thikness = {self.thickness*1e6:.0f} um \n
        GAPS: g1 = {self.g1*1e6:.2f}um, g2 = {self.g2*1e6:.2f}um, g_f = {self.gap_final*1e6:.1f},\n
        FINGERS: L_overlap = {self.L_overlap*1e6:.2f}um, L_max = {self.L_max*1e6:.2f}um,  finger_width = {self.width*1e6:.2f}um,  N (number of fingers) = {self.num_of_fingers}\n
        FLEXIBLE ARM: angle = {self.arm_angle:.1f} deg, L_arm = {self.L_arm*1e6:.1f}um, arm_width = {self.arm_width*1e6:.1f} um\n
        FLEXURE: k_spring = {self.k_spr:.1f}N/m, k_spring_max = {self.comp_k_spr_max():.1f}N/m 
        FORCE: at {self.max_voltage}V is {self.force_load*1e3:.2f} mN and per F/A = {self.f_load_per_area*1e-3:.1f} mN/mm2 and F/A_tot = {self.f_load_per_total_area*1e-3:.1f}\n
        EFFICIENCY: {self.efficiency*100:.2f}%, maximum is {self._maxefficiency*100:.2f}'''
        return string


class InchwormSpeed(Inchworm):
    """Includes features of Inchworm to compute dimensions and forces, but adds time solutions with ODEs from Ryan Shih's paper
    Additional Params
    -----------------
    finger length: if None provided is calculated as overlap length + space
    finger space: non-overlap finger length
    shuttle_extension : extra shuttle amount beyond movable fingers
    shuttle_width: central width of the shuttle
    etch_hole_size:  8 um by default'
    mu_medium: viscosity of medium (water by default) in Pa.s
    substrate_gap : BOX thickness, 2um by default"""
    def __init__(
        self, 
        MF, arm_angle, width, y = None, L_overlap = None,  gap_final = None, num_of_fingers=40, max_voltage = 110,  buckling_safety = 2, beam_length_factor = 0.7, 
        epsilon = 1,rho_medium = 0, DRIE_aspect = 20, k_back = 1.5, Delta_x = None, num_act =2, arm_width = None, f_res = None,
        L_sup = None, L_frame = 10e-6, L_anchor = 70e-6,thickness = None,
        finger_length = None, finger_space = 10e-6, shuttle_extension = 0, shuttle_width = 20e-6, etch_hole_size = 8e-6, mu_medium = 1e-3, substrate_gap = 2e-6, support_springs = (2e-6,241e-6)
    ):
        self.finger_space = finger_space
        if finger_length is None and L_overlap is not None:
            self.finger_length = L_overlap + finger_space
        else:
            self.finger_length = finger_length
        self.shuttle_extension = shuttle_extension
        self.shuttle_width = shuttle_width
        self.etch_hole_size = etch_hole_size
        self.mu_medium =mu_medium
        self.substrate_gap = substrate_gap
        self.support_springs = support_springs
        Inchworm.__init__(self,  MF, arm_angle, width, y , L_overlap ,  gap_final, num_of_fingers, max_voltage ,  buckling_safety , beam_length_factor , 
            epsilon, rho_medium , DRIE_aspect , k_back , Delta_x , num_act , arm_width , f_res ,
            L_sup, L_frame, L_anchor, thickness)
        if self.L_sup is None:
            self.L_sup = finger_space

        
    def comp_mass(self):
        if hasattr(self, 'shuttle_finger_mass'):
            m1 = self.shuttle_finger_mass
        else:
            m1 = self.comp_shuttle_finger_mass()
        m2 = self.comp_effective_fluid_mass()
        self.mass = m1+m2
        return self.mass
    def comp_shuttle_finger_mass(self):

        shuttle_length = self.num_of_fingers/2*(2*self.width +  self.g1 + self.g2) + 2*self.shuttle_extension ##assumes shuttle extension in same ballpark as 
        area_shuttle = shuttle_length*self.shuttle_width
        if self.etch_hole_size>0:
            N_etch_holes = np.ceil(shuttle_length/(2*self.etch_hole_size))
        else:
            N_etch_holes= 0
        area_etch_hole = N_etch_holes*self.etch_hole_size**2

        area_fingers = self.num_of_fingers*self.finger_length*self.width 
        if hasattr(self, 'L_arm'):
            angle_arm_area = self.arm_width*self.L_arm
        else:
            angle_arm_area = self.arm_width * 100e-6 ###estimate for shuttle inertia
        shuttle_finger_mass = self.rho_silicon*self.thickness*(area_fingers+ area_shuttle +angle_arm_area- area_etch_hole)  + self.rho_medium*self.thickness*area_etch_hole
        self.shuttle_finger_mass = shuttle_finger_mass
        return self.shuttle_finger_mass
    def comp_effective_fluid_mass(self):
        fluid_mass = self.num_of_fingers*self.rho_medium*self.L_overlap**2 *self.thickness**2/(2*(self.L_overlap + self.thickness))
        self.effective_fluid_mass = fluid_mass
        return self.effective_fluid_mass
    def comp_damping_coefficient(self):
        S1 = np.max((self.thickness, self.L_overlap))
        S2 = np.min((self.thickness, self.L_overlap))
        aspect_ratio = S2/S1 
        if aspect_ratio>1 or aspect_ratio<0:
            logging.WARNING('Aspect ratio for plate larger than 1 or negative ' + str(aspect_ratio))
        beta = 1 - 0.58*aspect_ratio #based onf Bao Fig 8
        damping_coeff = self.num_of_fingers*self.mu_medium*S1*S2**3*beta 
        damping_coeff = damping_coeff* (4*self.g1**3 * self.width + 2 * self.substrate_gap**3*self.thickness)/(self.g1**3*self.width + 2*self.substrate_gap**3*self.thickness)
        self.damping_pre_coefficient = damping_coeff
        return self.damping_pre_coefficient 
    def damping_coefficient(self, x):
        if not hasattr(self, 'damping_pre_coefficient'):
            self.comp_damping_coefficient()
        return self.damping_pre_coefficient*(1/(self.g1-x)**3 + 1/(self.g2 + x)**3)
    def comp_spring_constant_from_dimensions(self):
        self.spring_constant = 2*self.E_silicon*self.thickness*self.support_springs[0]**3/(self.support_springs[1]**3)
        return self.spring_constant
    def comp_k_spr(self):
        """overrides parent method"""
        logging.debug(self.f_res)
        if (self.f_res is None) and (not hasattr(self, 'k_spr')):
            self.k_spr = self.comp_spring_constant_from_dimensions()
            if not hasattr(self, 'mass'):
                self.comp_mass()
            self.f_res = 1/(2*np.pi)*np.sqrt(self.k_spr/self.mass)
        elif (self.f_res is not None) and hasattr(self, 'k_spr'):
            pass
        else:
            super().comp_k_spr()
            support_spring_length = ((2*self.E_silicon*self.thickness*self.support_springs[0]**3)/self.k_spr)**(1/3)
            self.support_springs = (self.support_springs[0], support_spring_length) 
        return self.k_spr
    def force_y(self):
        """eq 5 in Penskiy"""
        L_overlap = self.L_overlap
        y = self.y
        force_el_st = self.force_el_st(y, L_overlap)
        k_spr = self.comp_k_spr()
        force_y = force_el_st - k_spr* (y+self.Delta_y) ##includes full power lost to force
        if force_y<0:
            logging.warning(f'Negative Y-force for MF: {self.MF}, angle(deg){self.arm_angle}, (width) {self.width}, (l_overlap){self.L_overlap}')
        return force_y
        
    def pull_in_ODE(self, state,t):
        """defines ODE for state variable where state[0] is x and state[1] is dx/dt"""
        Force = self.force_el_st(state[0], self.L_overlap)
        b_T = self.damping_coefficient(state[0])
        if b_T<0:
            logging.debug(f'b_T= {b_T}, pre_damp = {self.damping_pre_coefficient} at x, dx/dt: {state}')
        k = self.k_spr
        m_eff = self.mass
        d2x_dt2 = (Force - b_T* state[1] - k*state[0])/m_eff
        # logging.debug(f'Force: {Force}, b_T: {b_T}, state: {state}, k: {k}')
        if state[0]>= self.g1- self.gap_final:
            return np.array([0, d2x_dt2])
        return np.array([state[1], d2x_dt2])
    def comp_pull_in(self, t_max = 20e-3, time_step = 1e-6, return_trajetory = False):
        """Solve ODE for pull in using data computed"""
        self.comp_damping_coefficient()
        self.comp_mass()
        self.comp_k_spr()
        
        init_cond = np.array([0,0])
        # sol = inte.solve_ivp(self.pull_in_ODE, (0, t_max), init_cond)
        # # vel = sol['y'][1,:]
        # times = sol['t']
        # x = sol['y'][0,:]
        times = np.arange(0, t_max, time_step)
        out = inte.odeint(self.pull_in_ODE, init_cond, times) ##deprecated method but sooooo much faster
        x = out[:,0]
        xdot = out[:,1]
        sol = {'t': times, 'y': np.transpose(out)}
        try:
            ind = np.min(np.argwhere(x>self.g1-self.gap_final))
            pull_in_time = times[ind]
        except: 
            pull_in_time = None
        if return_trajetory:
            return pull_in_time, sol
        else:
            return pull_in_time, None
    # @jit adding this tag can speed up things, may require stand alone functions
    def pull_out_ODE(self,state,t):
        """Defines ODE for pull out. Same as pull in but force is 0 and no gap stop"""
        Force = 0
        b_T = self.damping_coefficient(state[0])
        if b_T<0:
            # logging.warning(f'b_T= {b_T} at x, dx/dt: {state}')
            pass
        k = self.spring_constant
        m_eff = self.mass
        d2x_dt2 = (Force - b_T* state[1] - k*state[0])/m_eff
        # logging.debug(f'Force: {Force}, b_T: {b_T}, state: {state}, k: {k}')
        return np.array([state[1], d2x_dt2])
    def comp_pull_out(self, t_max = 20e-3, time_step = 1e-6, return_trajetory = False):
        """Solve ODE for pull in using data computed"""
        self.comp_damping_coefficient()
        self.comp_spring_constant_from_dimensions()
        self.comp_mass()
        init_cond = np.array([(self.g1-self.gap_final),0])
        # sol = inte.solve_ivp(self.pull_out_ODE, (0, t_max), init_cond, first_step = first_step)
        # times = sol['t']
        # x = sol['y'][0,:]
        # xdot = sol['y'][1,:]
        times = np.arange(0, t_max, time_step)
        out = inte.odeint(self.pull_out_ODE, init_cond, times)
        x = out[:,0]
        xdot = out[:,1]
        sol = {'t': times, 'y': np.transpose(out)}
        try:
            ind = np.min(np.argwhere(((x<0)&(abs(xdot)<1e-10))|((x<1e-8)&(abs(xdot)<1e-4))))
            pull_out_time = times[ind]
        except Exception as e: 
            logging.info('error finding pull out time' + str(e) )
            pull_out_time = None
        if return_trajetory:
            return pull_out_time, sol
        else:
            return pull_out_time, None
    def comp_period(self):
        self.pull_in,_ = self.comp_pull_in(t_max = 100e-3)
        self.pull_out,_ =self.comp_pull_out(t_max = 60e-3)
        try: 
            self.period = self.pull_in + self.pull_out
        except TypeError:
            self.period = None ##make it undesirable
        # self.period = self.pull_in*5
        logging.debug(f'Period is {self.period} with pull out {self.pull_out} for MF {self.MF}, angle {self.arm_angle}, width: {self.width}, spring_dims {self.support_springs}')
        return self.period
        
    def comp_power(self):
        work = self.force_load* self.Delta_x
        period = self.comp_period()
        if period is not None:
            self.power = work/period
        else:
            self.power =0 
        return self.power
    def comp_my_area(self):
        self.total_length = self.num_of_fingers/2*(2*self.width+self.g1+self.g2) + self.L_arm*np.sin(np.radians(self.arm_angle))+self.shuttle_extension + 2*self.L_anchor
        self.total_half_width = max(self.L_anchor + self.L_sup + self.finger_length + self.L_frame,  self.L_arm*np.cos(np.radians(self.arm_angle)))/2+ max((self.L_anchor + self.L_sup + self.finger_length + self.L_frame, self.L_anchor + self.support_springs[1] + self.L_frame))/2
        self.area = 2*self.num_act*self.total_length * self.total_half_width
        return self.area





class MotorOptimizer:
    def __init__(self,x0, x_units, bounds,*args, max_thickness = 500e-6, log_level = logging.INFO, motor_class = Inchworm):
        logger = logging.getLogger()
        logger.setLevel(log_level)

        self.x_units = np.array(x_units)
        self.x0 = np.array(x0)/x_units
        self.motor_class = motor_class
        self.kwargs = {}
        if len(args)>0:
            self.kwargs = args[0]
        self.constraints = []
        self.bounds = bounds
        self.max_thickness = max_thickness
        for index in range(len(x0)):
            lower, upper = bounds[index]
            if lower is not None:
                l = {'type': 'ineq', 'fun': lambda x, lb=lower, i=index: self.x_units[i]*x[i] - lb}
                self.constraints.append(l)
            if upper is not None:
                u = {'type': 'ineq', 'fun': lambda x, ub=upper, i=index: ub - self.x_units[i]*x[i]}
                self.constraints.append(u)
        self.constraints.append({'type': 'ineq', 'fun':self.max_t})
        self.constraints.append({'type': 'ineq', 'fun':self.max_finger_L})
        self.constraints.append({'type': 'ineq', 'fun':self.max_k_spr})
        self.constraints.append({'type': 'ineq', 'fun':self.width_mf})
    def get_motor(self,x):
        """returns motor object for x params"""
        return self.motor_class(x[0], x[1], x[2], None, x[3], **self.kwargs)
    def force_per_area(self,x):
        """objective function from penskiy. Might wanna optimize something else"""
        x= x*self.x_units
        motor = self.get_motor(x)
        logging.debug(f'x= {x}')
        logging.debug(f'Force per area {motor.f_load_per_area}')
        return -motor.f_load_per_area
    def force_per_total_area(self,x):
        """objective function from penskiy. Might wanna optimize something else"""
        x= x*self.x_units
        motor = self.get_motor(x)
        motor.comp_f_load_per_total_area()
        logging.debug(f'x= {x}')
        logging.debug(f'Force per total area {motor.f_load_per_total_area}')
    def efficiency(self,x):
        """Assumes DC operation, not AC square wave"""
        x= x*self.x_units
        motor = self.get_motor(x)
        logging.debug(f'x= {x}')
        logging.debug(f'Efficiency {motor.efficiency}')
        return -motor.efficiency
    def max_t(self,x):
        x = x* self.x_units
        motor = self.get_motor(x)
        return self.max_thickness - motor.thickness
    def max_finger_L(self,x):
        x = x* self.x_units
        motor = self.get_motor(x)
        return motor.L_max-(motor.L_overlap)
    def min_finger_L(self,x):
        x = x* self.x_units
        motor = self.get_motor(x)
        return motor.L_overlap-motor.L_max
    def max_k_spr(self,x):
        x = x* self.x_units
        motor = self.get_motor(x)
        motor.comp_k_spr_max()
        return motor.k_spr_max-motor.k_spr
    def width_mf(self,x):
        x = x* self.x_units
        return x[2] - x[0]
    def optimize(self, obj=None):
        if obj is None:
            obj = self.force_per_area
        self.result = opt.minimize(obj, self.x0, method = "COBYLA", constraints=self.constraints, options = {'rhobeg': 1, 'maxiter': 10000} )
        x = self.result['x']*self.x_units
        output = -self.result['fun']
        self.message = f'Maximum, optimized value {output}. Achieved at MF = {x[0]*1e6:.2f} um, angle = {x[1]:.1f} deg, width = {x[2]*1e6:.2f}um, and overlap lenght = {x[3]*1e6:.2f}um'
        logging.info(self.message)
        return self.result, self.result['x']*self.x_units, -self.result['fun'], self.get_motor(x)


class MotorSpeedOptimizer(MotorOptimizer):
    """Is able to optimize for speed, includes x[4]  for spring constant """
    def __init__(self, x0, x_units, bounds, *args, max_thickness=500e-6, log_level=logging.INFO, motor_class=InchwormSpeed):
        super().__init__(x0, x_units, bounds, *args, max_thickness=max_thickness, log_level=log_level, motor_class=motor_class)
    def get_motor(self, x):
        motor = self.motor_class(x[0], x[1], x[2], None,x[3], support_springs = (2e-6, x[4]), **self.kwargs)
        motor.comp_L_arm()
        return motor
    def average_power(self, x):
        """computs power for a single GCA delivered"""
        x= x*self.x_units
        motor = self.get_motor(x)
        return - motor.comp_power()
    def power_per_area(self,x):
        x= x*self.x_units
        motor = self.get_motor(x)
        motor.comp_force_load()
        power = motor.comp_power()
        area = motor.comp_my_area()
        logging.debug(f'Springs are {motor.support_springs}')
        return -power/area
    def period(self,x):
        x= x*self.x_units
        motor = self.get_motor(x)
        period = motor.comp_period()
        if period is not None:
            return period
        else: 
            return 10e9
    def pull_in(self,x):
        x= x*self.x_units
        motor = self.get_motor(x)
        t, _ = motor.comp_pull_in()
        if t is not None:
            return t
        else: 
            return 10e9
    def pull_out(self,x):
        x= x*self.x_units
        motor = self.get_motor(x)
        t, _ = motor.comp_pull_out()
        if t is not None:
            return t
        else: 
            return 10e9
    def optimize(self, obj=None):
        if obj is None:
            obj = self.power_per_area
        self.result = opt.minimize(obj, self.x0, method = "COBYLA", constraints=self.constraints, options = {'rhobeg': 1, 'maxiter': 10000} )
        x = self.result['x']*self.x_units
        output = -self.result['fun']
        self.message = f'Maximum, optimized value {output}.'+f'Achieved at MF = {x[0]*1e6:.2f} um, angle = {x[1]:.1f} deg, width = {x[2]*1e6:.2f}um, and overlap lenght = {x[3]*1e6:.2f}um, spring_length = {x[4]*1e6:.0f}'
        logging.info(self.message)
        return self.result, self.result['x']*self.x_units, -self.result['fun'], self.get_motor(x)

