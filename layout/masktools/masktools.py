import gdspy
import numpy as np
from layout.myGdspy import myText
import os
import itertools 
PATH = os.path.dirname(os.path.abspath(__file__))
INCH = 25400
class AML_Bonder(gdspy.Cell):
    """makes a cell with alignment marks for AML bonder, placed on appropriate place with respect to 0,0, the origin of the wafer"""


    def __init__(self, name ,  lib: gdspy.GdsLibrary, layers = [6,14]):
        if name is None:
            self.name = 'AML_AM' + str(id(self))
        else:
            self.name = name
        super().__init__(self.name)
        self.lib = lib
        lib.add(self)
        self.bottom = layers[0]
        self.top = layers[1]
        # self.alignment_mark()
        # left = gdspy.CellReference(self.am, (-58e3, 0))
        # right = gdspy.CellReference(self.am, (58e3, 0))
        # self.add((right, left))


    def alignment_mark(self):
        self.am = self.lib.new_cell(self.name + '_single_am' )
        rect1 = gdspy.Rectangle((-50,-10), (50, 10), layer = self.top, datatype= 0)
        rect2 = gdspy.Rectangle((-10,-50), (10, 50), layer = self.top, datatype=0)
        cross = gdspy.boolean(rect1, rect2, 'or', layer = self.top, datatype= 0)
        self.am.add(cross)
        not_cross= gdspy.offset(cross, 5., layer = self.top,datatype=0, join_first= False)
        square = gdspy.Rectangle((-50,-50), (50,50), layer = self.bottom, datatype=0)
        pattern_4x = gdspy.boolean(square, not_cross, 'not', layer = self.bottom, datatype=0)
        self.am.add(pattern_4x)
    def add_landing_strips(self):
        """adds landing strips to both layers"""
        self.landing_mark = self.lib.new_cell(self.name + 'am_ls')
        self.landing_mark.add(gdspy.CellReference(self.am))
        for layer in [self.top, self.bottom]:
            ls = LandingStrips(self.am, None, self.lib)
            ls.single_strip(layer = layer, datatype = 0)
            ls.draw()
            self.landing_mark.add(gdspy.CellReference(ls))
        return self.landing_mark
    def place_alignment_mark(self): 
        self.alignment_mark()
        left = gdspy.CellReference(self.am, (-58e3, 0))
        right = gdspy.CellReference(self.am, (58e3, 0))
        self.add((right, left))


class Gcaws6(gdspy.Cell):
    """ Makes a cell with GCAWS Frame and alignment mark centered at 0,0
    Attributes
    ----------


    Methods
    -------

    """
    mask_length = 25000
    frame_width = 2500
    ram_coors = np.array([[2000, -11700],[-200, 11300]]) #reticle alingment marks
    ram_dim = 400
    def __init__(self, name = 'gcaws6', lib: gdspy.GdsLibrary = None):
        """
        Parameters
        ----------
        layer : int
            GDSII layer value for mask
        datatype : int
            GDSII datatype value for mask
        name : str
            Cell name for cell (default is gcaws6)
        """
        self.lib = lib
        gdspy.Cell.__init__(self, name)
        self.lib.add(self)

    def add_frame(self,layer =0, datatype =1):
        rel_path = 'gcaws_temp.gds'
        fpath = os.path.join(PATH, rel_path)
        lib = gdspy.GdsLibrary(infile  = fpath)
        cell = lib.cells['TOP'].flatten(single_layer = layer, single_datatype = datatype)
        cell.name = f'gcaws6frame_{layer}_{datatype}'
        self.lib.add(cell)
        self.add(gdspy.CellReference(cell))
        # self.add(cell.polygons)
        # rect = gdspy.Rectangle([0,0], [Gcaws6.frame_width, Gcaws6.mask_length], layer= layer, datatype=datatype)
        # rect.translate(-Gcaws6.mask_length/2, -Gcaws6.mask_length/2)
        # self.add(rect)
        # self.add(gdspy.copy(rect, Gcaws6.mask_length - Gcaws6.frame_width))
        # ram1 = gdspy.Rectangle(Gcaws6.ram_coors[0], Gcaws6.ram_coors[0]+Gcaws6.ram_dim, layer=layer, datatype = datatype)
        # ram2 = gdspy.Rectangle(Gcaws6.ram_coors[1], Gcaws6.ram_coors[1]+Gcaws6.ram_dim, layer=layer, datatype = datatype)
        # rect1 = gdspy.copy(rect).rotate(np.pi/2)
        # rect2 = (gdspy.copy(rect1, 0, Gcaws6.mask_length-Gcaws6.frame_width))
        # rect1 = gdspy.boolean(rect1, ram1,'not')
        # self.add(rect1)
        # rect2 = gdspy.boolean(rect2, ram2,'not')
        # self.add(rect2)
    def add_am(self, coordinates, layer =0, datatype =1):
        """ Add alignment marks at coordinates
          Parameters
        ----------
        coordinates : array-like
            center of alignment marks
        layer : int
            GDSII layer value for mask
        datatype : int
            GDSII datatype value for mask        """
        cell = extract_gca_am(layer, datatype)
        cell.name = cell.name + f'{coordinates}'
        self.lib.add(cell)
        cell_ref = gdspy.CellReference(cell, coordinates)

        self.add(cell_ref)
        # self.flatten() # if you want separate cell try diff value
    def add_reticle_am(self, layer=0, datatype =1):
        """
        Adds alignment marks for the reticle in gcaws6 in the desired layer
        """
        rect = gdspy.Rectangle((-200,-200),(200,200), layer = layer, datatype=datatype)
        bot = gdspy.copy(rect, 2200, -11500)
        top = gdspy.copy(rect,0, 11500)
        self.add((bot, top))
class Mla150(gdspy.Cell):
    """ mask tools for MLA150 mask printing"""
    wafer_diam = 6 *INCH
    def __init__(self, name = 'mla150', lib:gdspy.GdsLibrary=None):
        """Starts cell to which usefull components for Mla150.
        Parameters
        ----------
        name : str
            cell name. Default is mla150
        """
        
        gdspy.Cell.__init__(self, name)
        self.lib = lib
        lib.add(self)
    def add_am(self, origin = (0,0), layer=0, datatype =1, am_type = 'standard', frame = None):
        """
        Adds an alignment mark for Mla150 in the required layer at the location specified
        type: 'standard', 'tsa', bsa'
        """
        if am_type == 'standard':
            rel_path = 'mla150_am.gds'
        if am_type =='tsa':
            rel_path = 'mla150AlignMarkTSA.gds'
        if am_type =='bsa':
            rel_path = 'mla150AlignMarkBSA.gds'
        fpath = os.path.join(PATH, rel_path)
        lib = gdspy.GdsLibrary(infile  = fpath)
        cell = lib.cells['TOP'].flatten(single_layer = layer, single_datatype = datatype)
        cell.name = f'mla150_am_{am_type}_{layer}_{datatype}'
        self.lib.add(cell)
        if frame is not None:
            bb = cell.get_bounding_box()
            rect = gdspy.Rectangle(bb[0,:], bb[1,:])
            for lay in frame:
                rect = gdspy.offset(rect, 20, layer = lay, datatype=datatype)
                cell.add(rect)
                

        origin = np.array(origin)
        if len(origin.shape) ==1:
            ref = gdspy.CellReference(cell, origin = origin)
            self.add(ref)
        if len(origin.shape) == 2:
            for ori in origin:
                ref = gdspy.CellReference(cell, origin = ori)
                self.add(ref)
       


class Ksaligner(gdspy.Cell):
    """ Tool to make a 7 inch mask for KS Aligner or Quintel"""
    mask_size = 7 * INCH  
    wafer_diam = 6 *INCH
    def __init__(self, name = 'ksaligner', lib:gdspy.GdsLibrary=None):
        """Starts cell to which usefull components for KSAligner.
        Parameters
        ----------
        name : str
            cell name. Default is KS Alignter
        """
        
        gdspy.Cell.__init__(self, name)
        self.lib = lib
        lib.add(self)
    def add_frame(self, layer =0, datatype =1, frame_width = None):
        """ draws a (potentially hollow) frame to show the size of a mask 
        Parameters
        ----------
        layer, datatype : int
            GDSII layer and datatype. Defaults are 0 and 1 respectively
        frame_width : float
            size in microns of width to be added to frame. If none a full rectangle is drawn
        """
        
        rect = gdspy.Rectangle([-Ksaligner.mask_size/2, -Ksaligner.mask_size/2],\
            [Ksaligner.mask_size/2, Ksaligner.mask_size/2],\
            layer = layer, datatype = datatype)
        if not( frame_width is None):
            length = Ksaligner.mask_size/2 - frame_width
            rect2 = gdspy.Rectangle([-length, -length],\
                [length, length],\
                layer = layer, datatype = datatype)
            rect = gdspy.boolean(rect, rect2, 'not', layer = layer, datatype=datatype)
        self.add(rect)
    def add_wafer(self, layer =0, datatype =1, frame_width = None, tolerance = 300):
        """ draws a (potentially hollow) frame  with wafer size to show the size of a mask 
        Parameters
        ----------
        layer, datatype : int
            GDSII layer and datatype. Defaults are 0 and 1 respectively
        frame_width : float
            size in microns of width to be added to frame. If none a full rectangle is drawn
        """
        if frame_width is None:
            inner_radius = 0
        else:
            inner_radius = Ksaligner.wafer_diam/2 - frame_width/2
        wafer = gdspy.Round([0,0], Ksaligner.wafer_diam/2+frame_width/2, inner_radius=inner_radius, tolerance = tolerance, layer = layer, datatype = datatype)
        self.add(wafer)
    def add_gcaws_am(self, coordinates, mag = 1, layer =0, datatype =1):
        """ Add alignment mark like that in GCAWS
        Parameters
        ----------
        coordinates : array_like
            center of cell
        mag : float

        """
        am = extract_gca_am(layer, datatype)
        self.lib.add(am)
        am_ref = gdspy.CellReference(am, coordinates, magnification = mag)
        self.add(am_ref)
    def zero_marker(self, dimension, layer =0, datatype =0):
        """ Add diamond marker to origin for reference """
        rect = gdspy.Rectangle([-dimension/2, -dimension/2], [dimension/2,dimension/2], layer = layer, datatype =datatype)
        cell = self.lib.new_cell('zero')
        cell.add(rect)
        self.add(gdspy.CellReference(cell, rotation = 45))
    def add_aligment_mark(self, length, width, loc = (0,0), gap = 20,  layer = [0,1,2], datatype = [0,0,0]):
        """Adds (inverted) alignment marks for desired number of layers """
        frame_rec = gdspy.Rectangle([-length/2 - gap, -length/2 -gap], [length/2 + gap, length/2 + gap])
        if isinstance(layer, int):
            layer = [layer]
            datatype = [datatype]
        cross = gdspy.Rectangle([-length/2, -width/2], [length/2, width/2], layer = layer[0], datatype = datatype[0])
        # cross_cell = gdspy.Cell('cross1_' + str(id(cross)))
        # cross_cell.add(cross)
        # full_cross = gdspy.Cell('cross_' + str(id(cross)))
        all_layers_cross = self.lib.new_cell('KAM' +str(loc) +str(id(cross)))
        # full_cross.add(gdspy.CellReference(cross_cell))
        # full_cross.add(gdspy.CellReference(cross_cell, rotation = 90))
        # inv_cross = gdspy.Cell('Icross_' + str(id(cross)))
        # full_cross = gdspy.boolean(frame_rec, gdspy.CellReference(full_cross), 'not', layer = layer[0], datatype = datatype[0])
        # inv_cross.add(full_cross)
        # all_layers_cross.add(gdspy.CellReference(inv_cross))
        if len(layer) >0:
            for i in range(0, len(layer)):
                pt0 = np.array([2*i*width, -length/2])
                pt1 = np.array([pt0[0], -2*i*width])
                pt2 = np.array([length/2, -2*i*width])
                l2 = gdspy.FlexPath([pt0, pt1, pt2], width)
                l2_cell = self.lib.new_cell('l' + str(i) +'_am'+ str(id(cross)))
                l2_cell.add(l2)
                L22_cell = self.lib.new_cell('l' + str(i) +'2_am'+ str(id(cross)))
                L22_cell.add(gdspy.CellReference(l2_cell))
                L22_cell.add(gdspy.CellReference(l2_cell, rotation =90))
                L22_cell.add(gdspy.CellReference(l2_cell, rotation =180))
                L22_cell.add(gdspy.CellReference(l2_cell, rotation =270))

                INV_L22_cell = self.lib.new_cell('Il' + str(i) +'2_am'+ str(id(cross)))
                INV_L22_cell.add(gdspy.boolean(frame_rec, gdspy.CellReference(L22_cell), 'not', layer = layer[i], datatype = datatype[i]))
                all_layers_cross.add(gdspy.CellReference(INV_L22_cell))

            # all_layers_cross.add(l2)
        
        del cross
        return gdspy.CellReference(all_layers_cross, loc)
        # self.add(gdspy.CellReference(all_layers_cross, loc))
    def add_label(self, run, layer_name, date, layer,  username = 'MBUSTAMA', size = 3e3, datatype = 0):
        """ adds a label outside the frame for KSaligner marks to be written on MLA"""
        strings = [f'{run} \n \{layer_name}', f' {username} \n{date}']
        loc =[ np.array([58e3, -68e3]), np.array([60e3, 67e3])]
        for i, string in enumerate(strings):
            l_cell = self.lib.new_cell('label'+str(i))
            text = gdspy.Text(string, size, (0,0), layer = layer, datatype=datatype)
            l_cell.add(text)
            bb = l_cell.get_bounding_box()
            offset = - np.array([bb[:,0].mean(), bb[:,1].mean()])
            self.add(gdspy.CellReference(l_cell, loc[i] + offset))
    def backside_am_windows(self, layer = 0, datatype =1):
        """ shows the location of alignment windows on backside"""
        rect1 = gdspy.Rectangle((-55e3, 8e3), (-14e3, -12e3), layer = layer, datatype=datatype)
        rect2 = gdspy.Rectangle((55e3, 8e3), (14e3, -12e3), layer = layer, datatype=datatype)
        self.add((rect1, rect2))
def test_structures(dimensions, number = [5,5], text=True, gap = 100, layer =0, datatype =1):
    dimensions1 = np.array([dimensions[0], dimensions[0]])
    square = gdspy.Rectangle([0,0], dimensions1, layer = layer, datatype = datatype)
    sq_cell =gdspy.Cell('sq' + str(id(square)))
    sq_cell.add(square)
    sq_array = gdspy.CellArray(sq_cell, number[1], number[0],2* dimensions1)
    rect = gdspy.Rectangle([0,0], dimensions, layer = layer, datatype = datatype)
    rect_cell = gdspy.Cell('rec' + str(id(square)))
    rect_cell.add(rect)
    rect_array = gdspy.CellArray(rect_cell, number[0], 1, 2*dimensions1, (0, -dimensions[1] - gap))
    text = gdspy.Text(str(dimensions[0]) + ' um', gap*0.8, (0,-gap*0.9), layer= layer, datatype = datatype)
    out = gdspy.Cell('TEST' + str(id(text)))
    out.add(text)
    out.add(sq_array)
    out.add(rect_array)
    return out





def extract_gca_am(layer =0, datatype =1 ):
    rel_path = 'gcaws_am.gds'

    fpath = os.path.join(PATH, rel_path)
    lib = gdspy.GdsLibrary(infile  = fpath)
    cell = lib.cells['TOP'].flatten(single_layer = layer, single_datatype = datatype)
    cell.name = 'AM_GCAWS_' + str(layer) + str(datatype)
    return cell



class LithoStrucs(gdspy.Cell):
    def __init__(self, name = 'litho', lib:gdspy.GdsLibrary=None):
        gdspy.Cell.__init__(self, name)
        self.lib = lib
        lib.add(self)
    def exposure_test(self, layer, loc = (0,0), feature = 2, datatype=0, num = 5, text_size =20, grid = True):
        """ draws offset arrays of lines with aspect ratio 5 
        if list is passed to feature it draws several sizes for them"""

        # num = 5
        this_loc = loc
        try:
            for feat in feature:
                self.exposure_test(layer, this_loc, feat, datatype= datatype, num=num, text_size=text_size, grid =grid)
                this_loc = np.array(this_loc) + np.array([(num+1)*2*feat, 0])
        except TypeError:
            asp = 5
            cell = self.lib.new_cell(self.name + f'{loc}_{feature}')
            self.add(gdspy.CellReference(cell, loc))
            shift = np.array([feature, 5*feature])
            for i in range(num):
                pt1 = np.array([0 + 2*feature*i, 0])
                pt2 = np.array([feature*(2*i+1), feature*asp])
                rect1 = gdspy.Rectangle(pt1, pt2, layer = layer, datatype = datatype)
                rect2 = gdspy.Rectangle(pt1 + shift, pt2+shift, layer = layer, datatype = datatype)
                cell.add((rect1,rect2))
            if grid == True:
                small_cell = self.lib.new_cell(self.name + f'{loc}_{feature}_a')
                square = gdspy.Rectangle((0,0), (feature,feature), layer = layer, datatype=datatype)
                small_cell.add(square)
                cell.add(gdspy.CellArray(small_cell, num, num, (feature*2, feature*2), (0, feature * (2+ 2*asp))))
                cell.add(gdspy.CellArray(small_cell, num, num, (feature*2, feature*2), (0 + feature, feature * (3+ 2*asp))))
            text = myText(f'{feature}', text_size, (0, -10 - text_size), layer = layer, datatype=datatype)
            cell.add(text)



class LandingStrips(gdspy.Cell):
    """creates landing strips to find things in masks"""
    def __init__(self, focus_cell: gdspy.Cell,  name = None, lib: gdspy.GdsLibrary = None, exclude_from_current = False):
        if name is None:
            self.name = 'LS' + str(id(self))
        else:
            self.name = name
        gdspy.Cell.__init__(self, self.name, exclude_from_current=exclude_from_current)
        self.lib = lib
        self.lib.add(self)
        self.focus_cell = focus_cell
    def single_strip(self, length = 1.5e3, width = 200, layer = 0, datatype =0):
        """draws a single strip, to be placed to the left"""
        self.strip = self.lib.new_cell(self.name + 'strip' + str(layer)+'_'+str(datatype))
        arrow_length = width *2
        arrow_width = width/2
        rect = gdspy.Rectangle([-length, -width/2], [0, width/2], layer = layer, datatype = datatype)
        self.strip.add(rect)
        arrow = self.lib.new_cell(self.name + 'arrow')
        arrow_rect = gdspy.Rectangle([-arrow_length, 0], [0, arrow_width], layer= layer, datatype =0)
        arrow.add(arrow_rect)
        locations_x = np.array([-4/5*length, -2/5 *length, -1/10*length])
        locations_y = np.array([-width/2, width/2])
        angles = np.array([45, -45])
        reflections = np.array([False, True])
        xx, yy = np.meshgrid(locations_x, locations_y)
        xx, aa = np.meshgrid(locations_x, angles)
        xx,rr = np.meshgrid(locations_x, reflections)
        def make_ref(x, y, refl, angle = 45):
            ref = gdspy.CellReference(arrow, (x, y), rotation = angle,x_reflection=refl )
            return ref
        v_make_array = np.vectorize(make_ref)
        refs  = v_make_array(xx, yy,rr, aa)
        self.strip.add(refs.flatten())
    def draw(self, gap = 100):
        bb = self.focus_cell.get_bounding_box()
        center = np.mean(bb, axis =0)
        rect = gdspy.Rectangle(bb[0,:], bb[1,:])
        rect = gdspy.offset(rect, gap)
        bb = rect.get_bounding_box()
        side_cents = [np.array([bb[0,0], center[1]]), np.array([center[0], bb[0,1]]), np.array([bb[1,0], center[1]]), np.array([center[0], bb[1,1]])]
        side_cents = np.array(side_cents)
        angles = np.arange(0, 271, 90)
        place_ref = np.vectorize(lambda x,y, ang: gdspy.CellReference(self.strip, (x,y), ang))
        refs = place_ref(side_cents[:,0], side_cents[:,1], angles)
        self.add(refs)
# mask = Gcaws6()
# mask.add_frame()
# mask.add_am([0,0])
# mask.add_am((5000,0))


# ksal = Ksaligner()
# ksal.add_frame(frame_width= 300)
# ksal.add_wafer(frame_width= 300)
# ksal.add_gcaws_am((0,0))
# ksal.add_gcaws_am((0, 300), 3)
# ksal.add_aligment_mark(300, 10, (0, 300))
# ksal.zero_marker(10)
# gdspy.LayoutViewer()

