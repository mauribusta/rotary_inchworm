import gdspy
import numpy as np
import pandas as pd
from layout.myGdspy import myText
import os
import itertools 
import masktools
PATH = os.path.dirname(os.path.abspath(__file__))
INCH = 25400


class Gcaws6_EM(gdspy.Cell):
    """ class for recreating virtual gcaws6 jobs and passes"""
    SHRINK = 0.20
    UNITS = 1e3 # values given to gca in mm
    DIAM = 150
    def __init__(self, name = 'GCAWS6_WAFER', lib: gdspy.GdsLibrary= None, exclude_from_current= False):
        if lib is None:
            lib = gdspy.current_library
        self.lib = lib
        self.name = name
        gdspy.Cell.__init__(self, name, exclude_from_current=exclude_from_current)
        self.lib.add(self)
        ## add a 6 inch wafer
        ks = masktools.Ksaligner('frame' + name , self.lib)
        ks.add_wafer(frame_width=100)
        self.add(gdspy.CellReference(ks))
        self.passes = {}
        
    def define_job(self,step_x: float, step_y: float, cols_or_width,rows_or_height,  mode_x = 'COUNT',mode_y = 'COUNT', translate_origin = (0,0), display = True):
        """ emulates SPEC command with metric units
        parameters
        ----------
        step_x: step size in x direction
        step_y: step size in y direction
        rows, columns: respectively assumign count, same variable used for span (not yet implemented)
        mode_x : 'COUNT', 'SPAN', 'ALL'
        mode_y : 'COUNT', 'SPAN', 'ALL'
        translate_origin = offsets array from center


        """
        # x direction
        if mode_x == 'COUNT':
            columns = cols_or_width
            x = np.arange(0, columns*step_x, step_x)
            width = x[-1]
        if mode_x == 'SPAN':
            width = cols_or_width
            x = np.arange(0, width, step_x)
            columns = len(x)
        if mode_x == 'ALL':
            x = np.arange(0, Gcaws6_EM.DIAM, step_x)
            columns = len(x)
            width = x[-1]
        # y direction
        if mode_y == 'COUNT':
            rows = rows_or_height
            y = np.arange(0, rows*step_y, step_y)
            height = y[-1]
        if mode_y == 'SPAN':
            height = rows_or_height
            y = np.arange(0, height, step_y)
            rows = len(y)
        if mode_x == 'ALL':
            y = np.arange(0, Gcaws6_EM.DIAM, step_y)
            rows = len(y)
            height = y[-1]
        
        #center array
        x = x - np.mean(x)
        y = y - np.mean(y)

        x = x+ translate_origin[0]
        y = y + translate_origin[1]
        ## make meshgrid
        xx, yy = np.meshgrid(x,y)
        def in_wafer(x,y):
            return (x**2 +  y **2 <= (Gcaws6_EM.DIAM/2)**2) & (y > -Gcaws6_EM.DIAM/2 + 12.5)

        in_wafer_v = np.vectorize(in_wafer)
        expose_arr = in_wafer_v(xx,yy)
        if display:
            self.display(expose_arr)
        
        self.xx = xx 
        self.yy = yy
        self.expose_arr = expose_arr
    
    def define_pass(self, pass_name, reticle_name, masking_aperture = [0,0,0,0], pass_shift = (0,0), array_or_plug = 'ARRAY', drop_or_plugs= [], comment = 'commment', display = True, plug_offsets = None):
        this_pass = {'name': pass_name, 'reticle': reticle_name, 'apperture': masking_aperture, 'pass_shift': pass_shift, 'comment': comment}
        drops_or_plugs = np.array(drop_or_plugs) - 1 #change to 0 indexing
        if array_or_plug.upper()== 'ARRAY':
            drops = tuple(np.array(drops_or_plugs.T.tolist()))
            expose_arr = np.full(self.expose_arr.shape, True)
            expose_arr[drops] = False
            expose_arr = self.expose_arr& expose_arr
        elif array_or_plug.upper() == 'PLUG':
            plugs = tuple(np.array(drops_or_plugs.T.tolist()))
            expose_arr = np.full(self.expose_arr.shape, False)
            expose_arr[plugs] = True
            expose_arr = self.expose_arr& expose_arr
            if plug_offsets is not None:
                if len(plug_offsets) != drops_or_plugs.shape[0]:
                    raise Exception('plug offsets not found')
                else:
                    xx = np.copy(self.xx)
                    yy = np.copy(self.yy)
                    for plug,offset in zip(drops_or_plugs, plug_offsets):
                        print(plug, offset)
                        xx[tuple(plug)] += offset[0]
                        yy[tuple(plug)] += offset[1]
                    this_pass['xx'] = xx
                    this_pass['yy'] = yy
        else:
            raise Exception('Only ARRAY or PLUG as possible modes')
        this_pass['expose_arr'] = expose_arr
        
        if display:
            self.display(expose_arr)
        self.passes[pass_name] = this_pass

    def display(self, array: np.ndarray):
        ast = np.vectorize(lambda x: '*' if x else ' ' )
        arr = pd.DataFrame(ast(array))
        arr.index += 1
        arr.columns += 1
        print(arr)
    
    def execute_pass(self, pass_name, display = True):
        """Places reticles at locations gcaws6 would"""
        this_pass = self.passes[pass_name]
        if display:
            print(this_pass['comment'])
            self.display(this_pass['expose_arr'])
        if 'xx' in this_pass.keys():
            xx = this_pass['xx']
        else:
            xx = self.xx
        if 'yy' in this_pass.keys():
            yy = this_pass['yy']
        else:
            yy = self.yy
        xx = xx + this_pass['pass_shift'][0]
        yy = yy + this_pass['pass_shift'][1]
        xx = xx * 1e3
        yy = yy *1e3
        ## load cell
        self.lib.read_gds(infile = this_pass['reticle'], rename_template=f'{pass_name}-' + "{name}")
        cells = list(filter(lambda cell: pass_name in cell.name, self.lib.top_level()))
        if len(cells) != 1:
            raise Exception('several top level cells')
        cell = cells[0]
        ref = gdspy.CellReference(cell, (0,0))
        xr, xl, yf, yb = this_pass['apperture'] 
        shutters_rect = gdspy.Rectangle((-10e3*5 + xl*1e3, -10e3*5 + yb*1e3), (10e3*5 - xr*1e3, 10e3*5 - yf*1e3))
        print(shutters_rect)
        die = self.lib.new_cell(f'PASS_{pass_name}')
        filtered = gdspy.boolean(shutters_rect, ref, 'and')
        die.add(filtered)
        make_refs = np.vectorize(lambda x,y, expose: self.make_ref(die, x, y, expose))
        ref_list = list(make_refs(xx,yy, this_pass['expose_arr']).flatten())
        ref_list = list(filter(None, ref_list))
        self.add(ref_list)
    def make_ref(self, cell, x, y, bool,  mag=0.2):
        if bool:
            return gdspy.CellReference(cell, (x,-y), magnification=mag)