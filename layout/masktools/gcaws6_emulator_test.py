# %%

import os
import sys

from cv2 import trace




module_path = os.path.abspath(os.path.join('../..'))
if module_path not in sys.path:
    sys.path.append(module_path)

import gdspy
import gcaws6_emulator

# %%
lib = gdspy.GdsLibrary()

PATH = os.path.dirname(os.path.abspath(__file__))
# %%
# gc = gcaws6_emulator.Gcaws6_EM(lib=lib)
# # %%
# gc.define_job(10, 10, None, None, mode_x = 'ALL', mode_y='ALL')
# # %%
# p = gc.define_pass(0, 'temp.gds', [0,0,0,0], array_or_plug='ARRAY', drop_or_plugs=[(1,2), (5,7)])
# # %%

# p2 = gc.define_pass(1, 'temp.gds', [0, 50, 0, 50], (5,5), array_or_plug='PLUG', drop_or_plugs=[(2,3),(5,7),(6,8)],plug_offsets=[(0,0), (-2,0), (2,-0)])

# %%
gcaws = gcaws6_emulator.Gcaws6_EM('gc6_2pass', lib = lib)
# %%
gcaws.define_job(22, 22, 7, 7, 'COUNT', 'COUNT')
# %%
gcaws.define_pass('base', os.path.join(PATH,'base.gds'), drop_or_plugs=[(3,1), (4,1), (5,1), (3,7), (4,7), (5,7)], comment = 'This pass contains device info')
# %%
gcaws.define_pass('aml', os.path.join(PATH,'aml.gds'), [50,0,0,50], array_or_plug='PLUG', drop_or_plugs=[(4,1),(4,7)], plug_offsets=[(13,5), (-3,5)], comment='This pass is for aml bonder')
# %%
gcaws.execute_pass('base', True)
# %%
gcaws.execute_pass('aml', True)
# %%
lib.write_gds('temp.gds')
os.startfile('temp.gds')
# %%
