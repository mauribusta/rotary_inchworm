import gdspy
import numpy as np
import pickle
import os
""" This module collects random tools to aid drawing across the board"""

def y_reflection(cell, lib, origin = (0,0) ):
    """creates a cell reference that is reflected along the y axis, preserving location of origin """
    if isinstance(cell, gdspy.CellReference):
        cell = lib.new_cell('y'+ cell.ref_cell.name).add(cell)
    origin = np.array(origin)
    cell0 = lib.new_cell(cell.name + 'o')
    cell0.add(gdspy.CellReference(cell, -origin))
    cell1 = lib.new_cell(cell.name + 'yr')
    ref = gdspy.CellReference(cell0,origin=(0,0), rotation= 90)
    cell1.add(ref)
    ref1 = gdspy.CellReference(cell1, origin = origin, rotation=-90, x_reflection=True)
    return ref1

def get_cell_pts(fnc, cell, level =0, depth = float('inf')):
    """
    copied from earlier pylayut 
    Returns locations of all points supplied by fnc 
    Parameters
    ----------
    fnc : function handle to get points for a given cell. fnc must return desired point if prompted, else return []
    cell : gdspy.Cell or gdspy.CellReference
    level : int
        level of recursion
    depth : int
        stop at deepest level of recursion

    """
    endpts = []
    if level > depth:
        return endpts

    if isinstance(cell, gdspy.Cell):
        origin = np.array([0,0])
        tm = np.eye(2)
        xy = np.array([[0,0]])
    elif isinstance(cell, gdspy.CellReference):
        origin = np.array(cell.origin)
        if cell.rotation is None:
            angle = 0
        else:
            angle = np.radians(cell.rotation)
        if cell.magnification is None:
            mag = 1
        else: 
            mag = cell.magnification
        rm = np.array([[np.cos(angle), -np.sin(angle)], [np.sin(angle), np.cos(angle)]])
        tm = mag*rm
        if cell.x_reflection:
            tm = tm.dot(np.array([[1,0], [0,-1]]))
        cell = cell.ref_cell
        xy = np.array([[0,0]])
    elif isinstance(cell, gdspy.CellArray):
        origin = cell.origin
        x,y = np.meshgrid(range(cell.columns), range(cell.rows))
        x = x* cell.spacing[0]
        y = y* cell.spacing[1]
        xy = np.array((x.flatten(),y.flatten())).transpose()
        if cell.rotation is None:
            angle = 0
        else:
            angle = np.radians(cell.rotation)
        if cell.magnification is None:
            mag = 1
        else: 
            mag = cell.magnification
        rm = np.array([[np.cos(angle), -np.sin(angle)], [np.sin(angle), np.cos(angle)]])
        tm = mag*rm
        if cell.x_reflection:
            tm = tm.dot(np.array([[1,0], [0,-1]]))
        cell = cell.ref_cell
    upr_endpoints = fnc(cell)
    for aoffset in xy:
        endpts.extend([tm.dot(endpt + aoffset) + origin for endpt in upr_endpoints])
    for reference in cell.references:
        lwr_endpts = get_cell_pts(fnc,reference, level + 1, depth)
        for aoffset in xy:
            endpts.extend([tm.dot(endpt+aoffset) + origin for endpt in lwr_endpts])
 
    return endpts
class myText(gdspy.Text):
    """make text with font from matlab code (stays tethered, no donuts)
    Code for font extracted from from gdsii for the rest of us"""

    def __init__(
        self, text, size, position=(0, 0), horizontal=True, angle=0, layer=0, datatype=0
        ):
        old_font = gdspy.Text._font
        mypath = os.path.abspath(os.path.dirname(__file__))
        path = os.path.join(mypath,'font_conv/new_font' )
        with open(path, 'rb') as file_obj:
            gdspy.Text._font = pickle.load(file_obj)
        position = np.array(position) + np.array([0,0.2336])*size
        gdspy.Text.__init__(self, text, size, position, horizontal, angle, layer, datatype)
        gdspy.Text._font = old_font