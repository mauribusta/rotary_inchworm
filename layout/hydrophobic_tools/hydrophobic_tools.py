import numpy as np
import os
import sys
module_path = os.path.abspath(os.path.join('..'))
if module_path not in sys.path:
    sys.path.append(module_path)
# import layout.motor_tools as motor
import gdspy

TRENCH =0
NOT_TRENCH = 1
RE_TRENCH = 2
SOI = 3
SOI_HOLE = 4 
RE_SOI = 5
METAL = 6
METAL_HOLE = 7
RE_METAL = 8
DUMMY = 9
NOT_DUMMY = 10
DRC_EXCLUDE_TRENCH = 11
DRC_EXCLUDE_SOI = 12
DRC_EXCLUDE_METAL = 13
TOP_METAL = 14
TOP_METAL_HOLE =15
RE_TOP_METAL = 16
TOP_VIA = 17


def add_fingers(pts, lib: gdspy.GdsLibrary, length=10, width=3, spacing=3, flip = False, layer = SOI, datatype =0):
    """draws fingers in lines connecting pts 
    Parameters
    ----------
    pts: points of lines on which to draw fingers
    length: finger length
    width = finger width
    spacing = finger spacing"""
    fingers = []
    finger = gdspy.Rectangle((-width/2,0), (width/2, length), layer = layer, datatype = datatype)
    fing_cell = lib.new_cell(f'finger{id(finger)}')
    fing_cell.add(finger)
    pts = np.array(pts)

    segments = pts[1:,:] - pts[0:-1, :]
    segment_lenghts = np.linalg.norm(segments, axis =1 )
    length = np.concatenate((np.array([0]), np.cumsum(segment_lenghts)), axis= None)
    # length = np.cumsum(segment_lenghts)
    N = int(np.floor((length[-1])/(width+spacing)))
    distance = (length[-1])/N
    for i in range(N):
        index = np.max((length< distance *(i+0.5)).nonzero())
        d = distance *(i+0.5) - length[index]
        if d<0:
            print(f'd={d}')
        vec = pts[index+1,:] - pts[index,:]
        nvec = vec/np.linalg.norm(vec)
        angle = np.arctan2(vec[1], vec[0])
        pos = pts[index,:] + nvec *d 
        ref = gdspy.CellReference(fing_cell, pos, np.rad2deg(angle)+180)
        fingers.append(ref)
    return fingers

def add_fingers_to_shape(shape: gdspy.PolygonSet, lib: gdspy.GdsLibrary, length=10, width=3, spacing=3, flip = False, layer = SOI, datatype =0):
    distance = 10*(width+spacing)/(2*np.pi)
    shape = gdspy.offset(shape,-distance, join = 'round', tolerance = 200, join_first=True, max_points=30000)
    shape = gdspy.offset(shape,distance, join = 'round', tolerance = 200, join_first=True, max_points=30000)
    distance = 1
    shape = gdspy.offset(shape,-distance, join = 'round', tolerance = 200, join_first=True, max_points=30000)
    polygons = shape.polygons
    fingers = []
    for poly in polygons:
        poly= np.append(poly, [poly[0,:]], axis = 0)
        fingers.extend(add_fingers(poly, lib, length+distance, width, spacing, flip, layer, datatype))
    return fingers, shape

def add_seal_ring(cell: gdspy.Cell, gap = 10, seal_width = 50, opening  = 200,  bond_layers = (TOP_METAL, METAL), datatype =0): 
        """Add seal ring around device on bond_layers, with opening at the bottom"""
        bb = cell.get_bounding_box()
        inner = gdspy.Rectangle(bb[0,:], bb[1,:], layer = bond_layers[0], datatype= datatype)
        inner = gdspy.offset(inner, gap, layer = bond_layers[0], datatype= datatype)
        outer = gdspy.offset(inner, seal_width, layer = bond_layers[0], datatype = datatype)
        ring = gdspy.boolean(outer, inner, 'not', layer = bond_layers[0], datatype = datatype)
        x_pos = outer.get_bounding_box()[0,0] + (outer.get_bounding_box()[1,0]-outer.get_bounding_box()[0,0] - opening)/2
        y_pos = outer.get_bounding_box()[0,1]
        not_ring = gdspy.Rectangle((x_pos, y_pos), (x_pos + opening,y_pos + seal_width))
        ring = gdspy.boolean(ring, not_ring, 'not', layer=bond_layers[0], datatype = datatype)
        ring2 = gdspy.boolean(ring, None, 'or', layer = bond_layers[1], datatype= datatype)
        cell.add((ring, ring2))
        return ring, ring2

