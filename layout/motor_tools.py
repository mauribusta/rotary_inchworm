from gdspy.library import CellReference, get_binary_cells
from gdspy.operation import boolean
from gdspy.path import FlexPath
import numpy as np
import gdspy
from scipy.optimize import fsolve
from scipy.optimize.nonlin import _array_like
import os
import sys



module_path = os.path.abspath(os.path.join('../..'))
if module_path not in sys.path:
    sys.path.append(module_path)
try:
    import layout.hydrophobic_tools.hydrophobic_tools as hydro
    import layout.myGdspy
    from layout.myGdspy import get_cell_pts, y_reflection
except ModuleNotFoundError:
    import hydrophobic_tools.hydrophobic_tools as hydro
    import myGdspy
    from myGdspy import get_cell_pts, y_reflection
##layer names following D. COntreraras convention
TRENCH =0
NOT_TRENCH = 1
RE_TRENCH = 2
SOI = 3
SOI_HOLE = 4 
RE_SOI = 5
METAL = 6
METAL_HOLE = 7
RE_METAL = 8
DUMMY = 9
NOT_DUMMY = 10
DRC_EXCLUDE_TRENCH = 11
DRC_EXCLUDE_SOI = 12
DRC_EXCLUDE_METAL = 13
TOP_METAL = 14
TOP_METAL_HOLE =15
RE_TOP_METAL = 16
TOP_VIA = 17

UNDERCUT = 6
MIN_FEATURE = 2
# class GCA(gdspy.Cell):
#     """Creates a gap closing actuator array from desired parameters. Based on make_GCA_array by J. Greenspun and D. Contreras"""
class SpiralSpring(gdspy.Cell):
    def __init__(
        self, name, lib: gdspy.GdsLibrary, 
        width, center : tuple, start_pt: tuple, end_pt: tuple,tether = 10, num_turns =1, layer = SOI, datatype =0, pt_separation =MIN_FEATURE, counterclockwise = True
        ):        
        if name is None:
            self.name = 'SpiralSpring_' + str(id(self))
        else: 
            self.name = name
        super().__init__(self.name)
        self.lib = lib
        lib.add(self)
        self.width = width
        self.center = np.array(center)
        self.start_pt = np.array(start_pt)
        self.end_pt = np.array(end_pt)
        self.tether = tether
        self.num_turns = num_turns
        self.layer = layer
        self.datatype = datatype
        self.pt_separation = pt_separation
        self.counterclockwise = counterclockwise
    def compute_points(self):
        direction_0 = self.start_pt - self.center
        self.R_in = np.linalg.norm(direction_0)
        direction_0 = direction_0/np.linalg.norm(direction_0)
        direction_end = self.end_pt - self.center
        self.R_out = np.linalg.norm(direction_end)
        direction_end = direction_end/ np.linalg.norm(direction_end)
        R_diff = self.R_out - self.R_in - 2*self.tether
        pt1 = self.start_pt + self.tether* direction_0
        # print(pt1, direction_0)
        ptm1 = self.end_pt - self.tether * direction_end
        R_out = self.R_out
        angle0 = np.arctan2(direction_0[1],direction_0[0])
        angle_end = np.arctan2(direction_end[1],direction_end[0])
        angle_range = 2*np.pi * self.num_turns + angle_end-angle0
        d_angle = self.pt_separation/R_out
        angles = np.arange(angle0, angle0 + angle_range, d_angle)
        self.angles = angles
        if not self.counterclockwise:
            angles = np.flip(angles)
        
        pts =  self.center.reshape(2,1) + (self.R_in +self.tether+ R_diff*np.linspace(0, 1, angles.shape[0])[np.newaxis,:])*np.array([np.cos(angles), np.sin(angles)])
        pts = np.concatenate((self.start_pt.reshape(2,1), pts, ptm1.reshape(2,1), self.end_pt.reshape(2,1)),axis =1) # np.append(np.append(self.start_pt.reshape(2,1), pts, axis =1), self.end_pt.reshape(2,1), axis =1)
        pts = pts.transpose()
        self.pts = pts
    def draw(self):
        self.compute_points()
        pts = self.pts
        self.path = gdspy.FlexPath(pts, self.width, layer = self.layer, datatype = self.datatype, gdsii_path=True, max_points=2000, precision = MIN_FEATURE/4)
        ## add extra path
        pts2 = pts[-2:, :]
        vec = pts2[1,:] - pts2[0,:]
        pts2[1,:] = pts2[0,:] + vec + MIN_FEATURE/2 * vec /np.linalg.norm(vec)
        pts2[0,:] = pts2[0,:] -  MIN_FEATURE/2 * vec /np.linalg.norm(vec)
        self.path2 =gdspy.FlexPath(pts2, self.width, layer = self.layer, datatype = self.datatype, gdsii_path=True, max_points=2000, precision = MIN_FEATURE/4)
        # self.path = gdspy.offset(self.path, MIN_FEATURE/8, join_first = True, max_points=40000)
        # self.path = gdspy.offset(self.path, -MIN_FEATURE/8, join_first=True, layer = self.layer, datatype = self.datatype, max_points=40000)
        self.add(self.path)
        self.add(self.path2)
    def add_anchor(self, extra =0):
        self.anchor = gdspy.Round(self.center, self.R_in+extra, layer = self.layer, datatype = self.datatype)
        self.add(self.anchor)
    def compute_length(self):
        self.compute_points()
        computed_length = np.sum(np.linalg.norm(self.pts[1:,:]- self.pts[0:-1, :], axis =1))
        self.computed_length = computed_length
        return computed_length
    def opposite_spring(self, angle = np.pi):
        """draws opposite spring for stability"""
        path = gdspy.copy(self.path)
        rotated_path = path.rotate(angle)
        path2 = gdspy.copy(self.path2)
        rotated_path2 = path2.rotate(angle)
        self.add((rotated_path, rotated_path2))


        ##compute spiral points 
class SpiralSpringL(SpiralSpring):
    def __init__(self, name, lib: gdspy.GdsLibrary, width, center: tuple, length, end_pt: tuple, R_in, tether = 10,  layer = SOI, datatype =0, pt_separation =MIN_FEATURE, counterclockwise = True):
        #compute start point from length
        self.length = length
        R_out = np.linalg.norm(np.array(end_pt)-np.array(center))
        
        len_spiral = length - 2 * tether ##assumes provided length is total
        num_turns = np.floor(length/(np.pi * 2* R_in) )
        theta = 2*length/(R_in + R_out)
        dR_dtheta = (R_out - R_in - 2*tether)/(theta)
        # def arc_len(theta):
        #     return ((R_in + dR_dtheta*theta)**2 + dR_dtheta**2)**1/2
        # def zero_fun(theta):
        #     integral, err = quad(arc_len, 0, theta)
        #     return len_spiral - integral
        # vfunc = np.vectorize(zero_fun)
        # theta_guess = theta
        # theta, *_ = fsolve(vfunc, theta_guess)
        dR_dtheta_new = (R_out - R_in - 2*tether)/theta
        # print(dR_dtheta, dR_dtheta_new)
        vec_end = np.array(end_pt)-np.array(center)
        theta_end = np.arctan2(vec_end[1],vec_end[0])
        theta_start = theta_end - theta
        start_pt = R_in* np.array([np.cos(theta_start), np.sin(theta_start)])
        

        super().__init__(name, lib, width, center, start_pt, end_pt, tether=tether, num_turns=num_turns, layer=layer, datatype=datatype, pt_separation=pt_separation, counterclockwise=counterclockwise)
class RotShuttle(gdspy.Cell):
    """Defines a circular, release shuttle with different spring options"""
    def __init__(self, name, lib: gdspy.GdsLibrary,origin:_array_like,  R_in, R_out, layer = SOI, not_layer = SOI_HOLE, datatype =0, etch_size = 8, undercut = UNDERCUT, etch_hole_params:dict={} ):
        if name is None:
            self.name = 'Shuttle_' + str(id(self))
        else: 
            self.name = name
        super().__init__(self.name)
        lib.add(self)
        self.lib = lib
        self.origin = np.array(origin)
        self.R_in = R_in
        self.R_out= R_out
        self.layer = layer
        self.datatype = datatype
        self.not_layer = not_layer
        self.etch_hole_size = etch_size
        self.undercut = undercut
        self.etch_hole_params = etch_hole_params
    def draw_shuttle(self):
        self.shuttle = gdspy.Round(self.origin, self.R_out, self.R_in, layer = self.layer, datatype= self.datatype, max_points=2000)
        self.add(self.shuttle)
        if self.R_in>0:
            not_dummy = gdspy.Round(self.origin, self.R_in, layer = NOT_DUMMY, datatype =0)
            self.add(not_dummy)
        etch_holes_p = {'shape': 'circle', 'geometry': 'rings', 'R_in': self.R_in, 'skip_filter': False}
        etch_holes_p = {**etch_holes_p, **self.etch_hole_params}
        holes = add_etch_holes(self.shuttle, self.etch_hole_size, self.undercut, self.not_layer, self.lib, self.datatype, **etch_holes_p)
        self.add(holes)
    def draw_teeth(self, tooth_width = 2.5, tooth_separation =1.5, tooth_extension = 0.5):
        self.tooth_width = tooth_width
        self.tooth_separation = tooth_separation
        self.tooth_extension =tooth_extension
       
        tooth_cell = Tooth(None, self.lib, self.tooth_width, self.tooth_extension, 0.03, layer = self.layer, datatype = self.datatype)
        ## modified to make sure teeth align
        n_teeth = 2*np.pi*self.R_out/(tooth_width+tooth_separation)
        n_teeth_4 = int(n_teeth//4) 
        angle = np.pi/(2*n_teeth_4)
        self.teeth = []
        for ind in range(4*n_teeth_4):
            p = self.origin + self.R_out*np.array([np.cos(ind*angle), np.sin(ind*angle)])
            # tooth = gdspy.Round(p, self.tooth_width/2,initial_angle=ind*angle-np.pi/2, final_angle=ind*angle+ np.pi/2, number_of_points= 12, layer = self.layer, datatype=self.datatype)
            tooth_ref = gdspy.CellReference(tooth_cell, p, rotation=np.degrees(ind*angle-np.pi/2))
            self.teeth.append(tooth_ref)
        self.add(self.teeth)
    def spiralspring(self, spring_length, spring_width = MIN_FEATURE, anchor_width = 100):
        self.spiral = SpiralSpringL(None, self.lib, spring_width, self.origin, spring_length, self.origin + np.array([self.R_in+.1, 0]),anchor_width/2, layer = self.layer, datatype=self.datatype)
        self.spiral.draw()
        self.spiral.add_anchor(extra = 1)
        self.add(gdspy.CellReference(self.spiral))
    def draw_rothub(self, n_teeth, max_dispalcement = MIN_FEATURE, tooth_width = 2.5, tooth_extension =0 ):
        self.hub = RotHub(
            self.name + 'rothub', self.lib, self.R_in*2, max_displacement=max_dispalcement, tooth_width = tooth_width, tooth_extension = tooth_extension,
            layer = self.layer, datatype= self.datatype )
        self.add(gdspy.CellReference(self.hub,self.origin))
class Tooth(gdspy.Cell):
    """Draws tooth with given dimensions"""
    def __init__(self, name, lib: gdspy.GdsLibrary, tooth_width=2.5, tooth_extension=0.5, tol = 0.03, layer =SOI, datatype = 0):
        if name is None:
            self.name = 'Tooth_' + str(id(self))
        else:
            self.name = name
        gdspy.Cell.__init__(self, self.name)
        self.lib = lib
        self.lib.add(self)
        self.layer = layer 
        self.datatype = datatype
        self.tooth_width = tooth_width
        tooth_extension = tooth_extension + 2*tol
        tooth = gdspy.Round((0,tooth_extension-tol), self.tooth_width/2, final_angle=np.pi, number_of_points= 12, layer = self.layer, datatype=self.datatype)
        self.add(tooth)
        rect = gdspy.Rectangle((-tooth_width/2,-tol), (tooth_width/2, tooth_extension-tol),layer = self.layer, datatype=self.datatype)
        self.add(rect)

class RotHub(gdspy.Cell):
    """ Draws an anchor with teeth that allow for at max max_displacement"""
    def __init__(
        self, name, lib: gdspy.GdsLibrary, 
        inner_width, max_displacement = MIN_FEATURE, n_teeth = 12, tooth_width = 2.5, tooth_extension = 0.5, layer = SOI, datatype =0
        ): 
        if name is None:
            self.name = 'RotHub_' + str(id(self))
        else:
            self.name = name
        gdspy.Cell.__init__(self, self.name)
        self.lib = lib
        lib.add(self)
        self.inner_width = inner_width
        self.max_displacement = max_displacement
        self.n_teeth = n_teeth
        self.layer = layer
        self.datatype = datatype
        self.tooth_width = tooth_width
        self.tooth_extension = tooth_extension
        self.radius = self.inner_width/2 - self.tooth_width/2 - self.tooth_extension - self.max_displacement
        self.draw_anchor()
        self.draw_teeth()
    def draw_anchor(self):
        
        anchor = gdspy.Round((0,0), self.radius, layer = self.layer, datatype = self.datatype)
        self.add(anchor)
    def draw_teeth(self):
        tooth = Tooth(None, self.lib, self.tooth_width, self.tooth_extension, layer = self.layer, datatype = self.datatype)
        self.teeth = []
        angle = 2*np.pi/self.n_teeth
        
        for ind in range(self.n_teeth):
            p = self.radius*np.array([np.cos(ind*angle), np.sin(ind*angle)])
            tooth_ref = gdspy.CellReference(tooth, p, rotation=np.degrees(ind*angle-np.pi/2))
            self.teeth.append(tooth_ref)
        self.add(self.teeth)
class SingleFlexure(gdspy.Cell):
    """Draws a flexure of given length, width, and anchor size
    Parameters
    ----------
    spring_length: L in spring
    spring_width: W in spring 
    anchor_width: size of anchor"""
    def __init__(self, name, lib, spring_length, spring_width, anchor_width, layer = SOI, not_layer = SOI_HOLE, datatype =0):
        if name is None:
            self.name = 'SingleFlexure_' + str(id(self))
        else: 
            self.name = name

        gdspy.Cell.__init__(self, self.name)
        self.lib = lib
        lib.add(self)
        self.spring_width = spring_width
        self.spring_length = spring_length 
        self.anchor_width = anchor_width
        self.layer = layer
        self.datatype = datatype
        self.draw()
    def draw(self):
        spring = gdspy.Rectangle((0,-self.spring_width/2), (self.spring_length, self.spring_width), layer = self.layer, datatype= self.datatype)
        self.add(spring)
        anchor_or = np.array([self.spring_length, -self.anchor_width/2])
        anchor = gdspy.Rectangle(anchor_or, anchor_or + np.array([self.anchor_width, self.anchor_width]), layer = self.layer, datatype= self.datatype)
        self.anchor = anchor
        self.add(anchor)

class Pawl(gdspy.Cell):
    """creates a pawl cell with origin at lower left 
    Parameters
    ----------
    n_teeth: 2
    tooth_width : 2.5 base width for semicirlces
    tooth_separation : 1.5
    pawl_width: length of pawl, if none a value is computed
    pawl_length: lenght of pawl, made to fit all teeth and separation precisely
    contact_point: gives the geometric mean of contact point of all 
    """
    def __init__(self, name, lib, n_teeth =2, tooth_width = 2.5, tooth_separation =1.5, pawl_width = None, pawl_length = None, layer = SOI, datatype = 0):
        if name is None:
            self.name = 'Pawl_' + str(id(self))
        else: 
            self.name = name
        gdspy.Cell.__init__(self, self.name)
        self.lib = lib
        self.lib.add(self)
        self.n_teeth = n_teeth
        self.tooth_width = tooth_width
        self.tooth_separation = tooth_separation
        if pawl_width is None:
            self.pawl_width = self.n_teeth * self.tooth_width + self.tooth_separation
        else:
            self.pawl_width = pawl_width
        if pawl_length is None:
            self.pawl_length = self.n_teeth * self.tooth_width + (self.n_teeth-1)* self.tooth_separation +0.5
        else:
            self.pawl_length = pawl_length
        self.layer = layer
        self.datatype = datatype
        self.draw_pawl()
        self.draw_teeth()
    def draw_pawl(self):
        self.pawl = gdspy.Rectangle((0,0), (self.pawl_length, self.pawl_width), layer = self.layer, datatype=self.datatype)
        self.add(self.pawl)
    def draw_teeth(self):
        if self.n_teeth == 0:
            self.contact_point = np.array((self.pawl_length/2, self.pawl_width))
        tooth = gdspy.Round((self.tooth_width/2,0), self.tooth_width/2, final_angle=np.pi, number_of_points= 12, layer = self.layer, datatype=self.datatype)
        p0 = np.array((0, self.pawl_width))
        for i in range(self.n_teeth):
            p = p0 + np.array((i*(self.tooth_width + self.tooth_separation),0))
            self.add(gdspy.copy(tooth, p[0], p[1]))
        if self.n_teeth>0:
            self.contact_point = p0 + (self.n_teeth-1)/2 *np.array(((self.tooth_width + self.tooth_separation),0)) + np.array((self.tooth_width/2, self.tooth_width/2))
    
class GCA(gdspy.Cell):
    """Movable (rotor) GCA 
    Parameters
    ----------
    num_fingers: number of fingers
    g0: smallest gap
    gb: larger gap on gcas
    finger_width : width of movable fingers
    finger_length: length of capacitor fingers
    space: default spacing for structure
    bar_ext : default is 50. Extension for central bar
    n_teeth: number of teeth for pawl
    tooth_width : 2.5 base width for semicirlces
    tooth_separation : 1.5
    pawl_width: length of pawl, if none a value is computed
    pawl_length: lenght of pawl, made to fit all teeth and separation precisely
    contact_point: gives the geometric mean of contact point of all 
    Arm params:
    arm_angle: angle for arm, in degrees
    arm_length: length of arm
    arm_width: width of angled arm
    gap_stop: dimensions of gap stop T, short first
    gap_stop_bump : bumps for gaps top dimensions
    gap_stop_gap: minimum gap at which shuttle stops
    anchor_width: minimum size of anchor width
    etch_hole_size: size for etch holes
    undercut: default set by global variable above, amount we can cut through
    layer: layer to draw SOI
    not_layer: layer for etch holes
    datatype: default is 0
    lib: gdspy library
    back_stop_anchor: anchor size for back stop
    ---------
    Variables
    """
    def __init__(
        self, name, lib, num_fingers, g0=4.8, gb = None,finger_width = 5, finger_length = 75, 
        space = 5, spring_width =3, spring_length = 240.8, bar_width = 25, bar_ext =50,
        gap_stop = (10,110), gap_stop_bump = (2,5), gap_stop_gap = 3.8, anchor_width = 70, etch_hole_size = 8, 
        n_teeth =2, tooth_width = 2.5, tooth_separation =1.5, pawl_width = None, pawl_length = None,
        arm_angle = 180-67.4, arm_length =122, arm_width = 3,  
        back_stop_anchor = 50, back_stop_bump = None, back_stop_gap = MIN_FEATURE,
        undercut = UNDERCUT, min_feature = MIN_FEATURE,
        layer = SOI, not_layer = SOI_HOLE, datatype =0, triangle_fingers = False, min_finger_width = MIN_FEATURE):
        if name is None:
            self.name = 'GCA_' + str(id(self))
        else: 
            self.name = name
        gdspy.Cell.__init__(self, self.name)
        self.lib = lib
        self.lib.add(self)

        self.num_fingers = num_fingers
        self.g0 = g0
        if gb is None:
            self.gb = g0*7.75/4.8
        else:
            self.gb= gb 
        self.finger_width = finger_width
        self.finger_length = finger_length
        self.space = space
        self.spring_width = spring_width
        self.spring_length = spring_length 
        self.bar_width = bar_width
        self.bar_ext = bar_ext
        self.gap_stop = gap_stop
        self.gap_stop_bump = gap_stop_bump
        self.gap_stop_gap = gap_stop_gap
        gap_stop_space = self.gap_stop[0] + self.gap_stop_bump[0] + self.gap_stop_gap
        self.anchor_width = anchor_width
        self.bar_length =  self.num_fingers/2 *(2*self.finger_width + self.g0 + self.gb) + self.bar_ext+  gap_stop_space + self.anchor_width
        self.layer = layer
        self.not_layer = not_layer
        self.datatype= datatype
        self.etch_hole_size = etch_hole_size
        self.min_feature =min_feature
        self.undercut = undercut
        self.triangle_fingers = triangle_fingers ## if true fingers are not rectangular
        self.back_stop_anchor = back_stop_anchor
        if back_stop_bump is None:
            self.back_stop_bump = self.gap_stop_bump
        self.back_stop_gap = back_stop_gap
        ##pawl params
        self.n_teeth = n_teeth
        self.tooth_width = tooth_width
        self.tooth_separation = tooth_separation
        self.pawl_width = pawl_width
        self.pawl_length = pawl_length
        ## angle arms params
        self.arm_angle = arm_angle
        self.arm_width = arm_width
        self.arm_length = arm_length
        self.min_finger_width = min_finger_width
        self.rotor = self.lib.new_cell('GCA_ROT_'+ str(id(self)))
        self.stator = self.lib.new_cell('GCA_STA_'+ str(id(self)))
    def draw(self):
        #rotor cell
        self.draw_rotor_shuttle()
        self.draw_back_stop()
        self.draw_flexures()
        self.draw_capacitor_fingers()
        self.draw_stator_anchors()
        self.draw_gap_stop()
        self.draw_angle_arm()
        self.add(gdspy.CellReference(self.rotor))
        self.add(gdspy.CellReference(self.stator))
    def draw_rotor_shuttle(self):
        """Draws a shuttle for the rotor"""
        rotor = self.lib.new_cell('rotor_shuttle' + str(id(self)))
        rectangle = gdspy.Rectangle((0,0), (self.bar_width, self.bar_length), layer= self.layer, datatype=self.datatype)
        etch_holes = add_etch_holes(rectangle, self.etch_hole_size,self.undercut, not_layer=self.not_layer, lib = self.lib, datatype=self.datatype, min_feature=self.min_feature)
        rotor.add(rectangle)
        rotor.add(etch_holes)
        origin_offset= (-self.bar_width/2, 0)
        self.rotor_shuttle = gdspy.CellReference(rotor, origin_offset)
        self.rotor.add(self.rotor_shuttle)


    def draw_back_stop(self):
        """draws back stop, origin of the cell is center of end of shuttle"""
        back_stop = self.lib.new_cell('back_stop' + str(id(self)))
        bump_dims = np.flip(np.array(self.back_stop_bump))
        offset = [-bump_dims[0]/2, -self.back_stop_gap - bump_dims[1]]
        bump = gdspy.Rectangle(offset, offset+bump_dims, layer = self.layer, datatype=self.datatype)
        back_stop.add(bump)
        anchor_dims = np.array([self.back_stop_anchor, -self.back_stop_anchor])
        up_left = [-anchor_dims[0]/2, -self.back_stop_gap - bump_dims[1]]
        anchor = gdspy.Rectangle(up_left, up_left + anchor_dims, layer = self.layer, datatype = self.datatype)
        back_stop.add(anchor)
        shuttle_bb = self.rotor_shuttle.get_bounding_box() 
        origin = ( np.mean(shuttle_bb[:,0]),shuttle_bb[0,1])
        self.back_stop = gdspy.CellReference(back_stop,origin )
        self.add(self.back_stop)
        # return back_stop
    def draw_flexures(self, left = False):
        flexure = SingleFlexure(None, self.lib, self.spring_length, self.spring_width, self.anchor_width, self.layer, self.not_layer, self.datatype)
        shuttle_bb = self.rotor_shuttle.get_bounding_box() 
        p1 = np.array([shuttle_bb[1,0], shuttle_bb[0,1] + self.space + self.spring_width/2])
        p2 = np.array([shuttle_bb[1,0], shuttle_bb[1,1] -self.space - self.spring_width/2]) 
        if not left:
            rotation = 0
        else:
            rotation = 180
            p1 = (-p1[0], p1[1])
            p2 = (-p2[0], p2[1])
        self.flexure_ref = [gdspy.CellReference(flexure,p1, rotation=rotation), gdspy.CellReference(flexure,p2,rotation=rotation)]
        self.rotor.add(self.flexure_ref)
    def draw_capacitor_fingers(self, extra_space =0 ):
        """GCA capacitor fingers, both for stator and rotor """
        single_finger = self.lib.new_cell('finger_'+ str(id(self)))
        dimensions = np.array([self.finger_width, self.finger_length+2*self.space])
        if not self.triangle_fingers:
            rect = gdspy.Rectangle((0,0), dimensions, layer = self.layer, datatype = self.datatype)
            single_finger.add(rect)
        else:
            alpha_min = self.min_finger_width/self.finger_width
            x_guess = self.space  
        
            func = lambda x: self.gb*(1-np.sqrt(alpha_min**2*self.finger_width**2 + x**2)/x) + self.finger_width*(2-alpha_min - alpha_min*(self.finger_length+x)/x) 
            x_sol = fsolve(func,x_guess)
            x_sol = x_sol[0]
            factor = alpha_min*(2*self.space + self.finger_length + x_sol)/(x_sol)
            quad = gdspy.Polygon(((0,0), (self.finger_width*factor,0), ( self.finger_width*alpha_min, dimensions[1]), (0, dimensions[1])), layer = self.layer, datatype=self.datatype)
            single_finger.add(quad)

        shuttle_bb = self.rotor_shuttle.get_bounding_box() 
        origin = np.array(( np.mean(shuttle_bb[:,0]),shuttle_bb[0,1]))
        #from top right

        p1 = [np.max(shuttle_bb[:,0]),shuttle_bb[1,1]-3*self.space -self.spring_width-self.anchor_width-self.gap_stop_gap-self.gap_stop_bump[0]-self.gap_stop[0]-self.gb - self.finger_width -self.g0 - extra_space]
        p1 = np.array(p1)
        p2 = [np.min(shuttle_bb[:,0]), p1[1]]
        p2 = np.array(p2)
        p3 = [p1[0]+4*self.space + self.finger_length, p1[1] + self.g0 + self.finger_width]
        p3 = np.array(p3)
        p4 = [p2[0]-4*self.space - self.finger_length, p3[1]]
        p4 = np.array(p4)
        pts = [p1,p2, p3, p4]
        rot = [-90, -90,-90, -90]
        x_reflection = [False, True, True, False]
        self.rot_fingers  = []

        for i in range(2):
            fingers = gdspy.CellArray(single_finger, int(self.num_fingers/2),1, np.array([self.gb + self.g0 + 2*self.finger_width,0]),
            pts[i], rotation = rot[i], x_reflection=x_reflection[i])
            self.rot_fingers.append(fingers)
        self.rotor.add(self.rot_fingers)
        self.stat_fingers = []
        y_single_finger = self.lib.new_cell('ref'+single_finger.name)
        y_single_finger.add(y_reflection(single_finger, self.lib, (self.finger_width/2,0)))
        for i in range(2,4):
            fingers = gdspy.CellArray(y_single_finger, int(self.num_fingers/2),1, np.array([self.gb + self.g0 + 2*self.finger_width,0]),
            pts[i], rotation = rot[i], x_reflection=x_reflection[i])
            self.stat_fingers.append(fingers)
        self.stator.add(self.stat_fingers)
    def draw_stator_anchors(self):
        """"Draws anchors for stator fingers"""
        self.stator_anchors = [None, None]
        for i in range(2):
            fing_bb = self.stat_fingers[0].get_bounding_box()
            length = fing_bb[1,1]-fing_bb[0,1] + 5 * self.space
            dimensions = np.array(((-1)**i*self.anchor_width,- length))
            pt1= np.array(((-1)**i*fing_bb[1,0], fing_bb[1,1] + 2*self.space))
            anchor1 = gdspy.Rectangle(pt1, pt1+dimensions, layer = self.layer, datatype=self.datatype)
            self.stator_anchors[i] = anchor1
            self.stator.add(anchor1)
    def draw_gap_stop(self):
        """Adds gap stop on top of shuttle, with anchors"""
        shuttle_bb = self.rotor_shuttle.get_bounding_box()
        p1 = shuttle_bb[1] + np.array([self.space, -2*self.space-self.spring_width])
        dims = np.array((self.anchor_width, -self.anchor_width))
        anchor = gdspy.Rectangle(p1, p1+dims, layer = self.layer, datatype=self.datatype)
        self.add(anchor)
        self.add(gdspy.copy(anchor, -self.anchor_width - 2*self.space - self.bar_width))
        gstop_bar = self.lib.new_cell('gstop_bar' + str(id(self)))
        bar = gdspy.Rectangle((-self.gap_stop[1]/2, -self.gap_stop[0]- self.gap_stop_bump[0]),( self.gap_stop[1]/2, 0), layer = self.layer, datatype=self.datatype)
        gstop_bar.add(bar)
        bump = gdspy.Rectangle((0,0), (self.gap_stop_bump[1], self.gap_stop_bump[0]), layer = self.layer, datatype=self.datatype)
        bump_cell = self.lib.new_cell('bump' + str(id(self)))
        bump_cell.add(bump)
        spacing = ((self.gap_stop[1] - self.bar_width)/2 - 2*self.space - self.gap_stop_bump[1])/2
        gstop_bumps = gdspy.CellArray(bump_cell, 3, 1, (spacing,1),(self.bar_width/2 + self.space,0))
        gstop_bumps = [gstop_bumps, gdspy.CellArray(bump_cell, 3, 1, (spacing, 0),(-self.gap_stop[1]/2+self.space, 0))]
        gstop_bar.add(gstop_bumps)
        gstop_bar_ref = gdspy.CellReference(gstop_bar,(0, np.min(anchor.get_bounding_box()[:,1]) - self.gap_stop_gap-self.gap_stop_bump[0]) )
        self.rotor.add(gstop_bar_ref)
    def draw_angle_arm(self):
        p0x = np.mean(self.rotor_shuttle.get_bounding_box()[:,0])
        p0y = self.rotor_shuttle.get_bounding_box()[1,1]
        p0 = np.array((p0x,p0y))
        a_arm = self.lib.new_cell('Arm_' + str(id(self)))
        arm_angle = np.radians(self.arm_angle)
        if arm_angle<np.pi/2:
            arm_angle = np.pi-arm_angle
            flip = True
        else:
            flip = False
        hor_width = self.arm_width/np.sin(arm_angle)
        endpt = self.arm_length * np.array([np.cos(arm_angle), np.sin(arm_angle)])
        delt = np.array((hor_width/2, 0))
        pts = [delt,
        endpt+delt,
        endpt-delt,
        -delt]
        arm = gdspy.Polygon(pts, layer = self.layer, datatype=self.datatype)
        # print(pts[1])
        a_arm.add(arm)
        pawl = Pawl('Pawl' + str(id(self)), self.lib, self.n_teeth, self.tooth_width, self.tooth_separation, self.pawl_width, self.pawl_length, layer = self.layer, datatype=self.datatype )
        a_arm.add(gdspy.CellReference(pawl, pts[1]+ np.array([- pawl.pawl_length,0])))
        if flip:
            flipped = y_reflection(a_arm, self.lib)
            tempcell = self.lib.new_cell(a_arm.name + 'yryr')
            tempcell.add(flipped)
            self.rotor.add(gdspy.CellReference(tempcell, p0))
        else:
            self.rotor.add(gdspy.CellReference(a_arm,p0))

    def displace_rotor(self, amount, output_datatype=None):
        rotor = self.rotor.get_polygons(by_spec =True)
        layerobjs = rotor[(self.layer, self.datatype)]
        not_layerobjs = rotor[(self.not_layer, self.datatype)]
        if output_datatype is None:
            output_datatype= self.datatype + 1
        output = gdspy.boolean(layerobjs, not_layerobjs, 'not', layer = self.layer, datatype = output_datatype)
        output2 = gdspy.copy(output, 0, amount)
        new_cell = self.lib.new_cell('D_'+str(amount)+'_' + self.rotor.name)
        new_cell.add(output2)
        self.add(gdspy.CellReference(new_cell))
    def get_contact_point(self):
        return get_contact_point(self)
    def get_movable_area(self)-> dict:
        """returns areas of movable rotor and etch holes"""
        flexures = [flex for flex in self.rotor.references if isinstance(flex.ref_cell, SingleFlexure)]
        flexure_area = 0
        for flex in flexures:
            flexure_area += flex.area()
        areas = self.rotor.area(by_spec= True)
        areas[(self.layer, self.datatype)] -= flexure_area
        return areas


    def metalize(self, offset = 6, soi_layer = SOI, not_soi_layer = SOI_HOLE, metal_layer = METAL, top_metal_layer =TOP_METAL, datatype =0):
        metalized = compute_anchors(self, offset, layer = soi_layer, not_layer = not_soi_layer, re_layer= soi_layer + 2,  datatype= datatype, output_layer=metal_layer, output_datatype= datatype, join = 'miter')
        if top_metal_layer is not None:
            top_metal = gdspy.boolean(metalized, None, 'or', layer = top_metal_layer, datatype= datatype)
            if top_metal is not None:
                self.add(top_metal) 
def get_contact_point(rotor):
    def pawl_pt_contact(cell):
        if isinstance(cell, Pawl):
            return [cell.contact_point]
        else:
            return []
    return get_cell_pts(pawl_pt_contact, rotor)

class GCApar(GCA):
    def parallel_number(self, number, one_arm = True):
        """This method is used to set how many gca arrays for one pawl"""
        if number %2 ==0:
            print("This class is currently only setup for odd number of arrays")
        self.number_of_arrays = number 
        self.one_arm = one_arm 
        if number> 1:
            self.bar_length = self.bar_length + 1.5*self.anchor_width + 2* self.space
    def draw(self):
        """Supercedes draw method for GCA"""
        self.draw_rotor_shuttle()
        self.draw_back_stop()
        self.draw_flexures()
        self.modify_flexures()
        self.draw_capacitor_fingers(extra_space=self.gap_stop[0] + self.space)
        self.draw_stator_anchors()
        
        self.no_arm_rotor = self.rotor.copy(self.rotor.name+'noarm', deep_copy = True)
        self.lib.add(self.no_arm_rotor)
        self.draw_gap_stop()
        self.draw_angle_arm()
        self.add(gdspy.CellReference(self.rotor))
        if self.one_arm:
            rotor = self.no_arm_rotor
        else:
            rotor = self.rotor
        stator = self.stator
        self.add(gdspy.CellReference(self.stator))
        pitch = max(self.spring_length + 2*self.anchor_width + 2* self.space + 0.5*self.back_stop_anchor + 0.5*self.bar_width, 
                    np.linalg.norm(self.stator.get_bounding_box()[:,0],1)-self.anchor_width) 
        self.interarray_pitch = pitch
        back_stop = self.back_stop
        for i in range((self.number_of_arrays-1)//2):
            print(rotor)
            self.add(gdspy.CellReference(rotor, (pitch*(i+1), 0)))
            self.add(gdspy.CellReference(rotor, (-pitch*(i+1), 0)))
            self.add(gdspy.CellReference(stator, (pitch*(i+1), 0)))
            self.add(gdspy.CellReference(stator, (-pitch*(i+1), 0)))
            self.add(gdspy.copy(back_stop,-pitch*(i+1), 0))
            self.add(gdspy.copy(back_stop, pitch*(i+1), 0))
        self.connect_stators()
    def modify_flexures(self):
        anchor_cell = self.flexure_ref[0].ref_cell
        anchor_cell_or = anchor_cell.copy(anchor_cell.name + 'mod', deep_copy=True)
        anchor_cell.anchor.translate(0,-self.anchor_width/2 + self.spring_width*2)
        self.flexure_ref[1].ref_cell = anchor_cell_or
        # self.flexure_ref[0].ref_cell = anchor_cell_or
    def connect_stators(self):
        """connects stators electrically """
        ## connect adjacent stators 
        pt1 = self.stator.get_bounding_box()[0,:] + np.array((self.anchor_width,0))
        pt2 = self.stator.get_bounding_box()[1,:] - np.array((self.anchor_width,0))
        left_base = gdspy.Rectangle(pt1, pt2 - np.array((self.interarray_pitch,0)), layer = self.layer, datatype=self.datatype)
        right_base = gdspy.Rectangle(pt1 + np.array((self.interarray_pitch,0)), pt2, self.layer, self.datatype)
        for i in range((self.number_of_arrays-1)//2):
            left = gdspy.copy(left_base, -i*self.interarray_pitch, 0)
            right = gdspy.copy(right_base, i*self.interarray_pitch, 0)
            self.add((left,right))
        ## add voltage rail
        bounding_box = self.get_bounding_box()
        trace_width = self.anchor_width - self.space
        rail = gdspy.Rectangle(
            bounding_box[0,:] - np.array([0, trace_width + self.space]),
            (bounding_box[1,0] + self.anchor_width, bounding_box[0,1] -self.space),
            layer = self.layer, datatype= self.datatype
            )
        self.add(rail)
        y_rail = bounding_box[0,1] - trace_width - self.space 
        trace_width = self.anchor_width-2*self.space
        pt0 = self.stator.get_bounding_box()[0,:]+ np.array((self.anchor_width,0))*0.5
        pt1 = pt0 - np.array([0, trace_width])
        pt2 = np.array((0 - self.back_stop_anchor/2 - self.space - trace_width/2, pt1[1]))
        pt3 = np.array((pt2[0], y_rail))
        trace = gdspy.FlexPath([pt0, pt1, pt2, pt3], trace_width, layer = self.layer, datatype = self.datatype)
        trace_cell = self.lib.new_cell('StatorTraces'+self.name)
        trace_cell.add(trace)
        refs = []
        for i in range(-self.number_of_arrays//2+1, self.number_of_arrays//2+2):
            ref = gdspy.CellReference(trace_cell, (i*self.interarray_pitch, 0))
            refs.append(ref)
        self.add(refs)
        



            


class RotMotor(gdspy.Cell):
    def __init__(self, name: str or None, lib: gdspy.GdsLibrary, gca: GCA, rot_shuttle: RotShuttle, num_act=4):
        if name is None:
            self.name = 'FullMotor_' + str(id(self))
        else: 
            self.name = name
        super().__init__(self.name)
        self.lib = lib
        lib.add(self)  
        self.gca = gca
        # self.gca.draw()
        self.num_act = num_act
        self.rot_shuttle = rot_shuttle
        self.add(gdspy.CellReference(self.rot_shuttle))
        self.origin = self.rot_shuttle.origin
    def draw_gcas(self):
        pawl_pos_radius = self.rot_shuttle.R_out + self.rot_shuttle.tooth_width/2 + self.rot_shuttle.tooth_extension  + self.gca.min_feature/2
        angular_separation = 2*np.pi/self.num_act
        self.gca_refs = []
        for index in range(self.num_act):
            angle = -np.pi/2 + angular_separation*index
            pawl_pos = self.origin + pawl_pos_radius* np.array([np.cos(angle), np.sin(angle)])
            ref = gdspy.CellReference(self.gca,(0,0), rotation = np.rad2deg(angle+np.pi/2))
            ref.translate(pawl_pos[0]- get_contact_point(ref)[0][0], pawl_pos[1]-get_contact_point(ref)[0][1])
            self.gca_refs.append(ref)
            self.add(ref)
    def add_lid(self, gap, seal_width, bond_layers = (TOP_METAL, METAL), datatype=0):
        hydro.add_seal_ring(self, gap = gap, seal_width= seal_width, bond_layers = bond_layers, datatype = datatype)


# def add_seal_ring(cell: gdspy.Cell, gap = 10, seal_width = 100, opening  = 200,  bond_layers = (TOP_METAL, METAL), datatype =0): 
#         """Add seal ring around device on bond_layers, with opening at the bottom"""
#         bb = cell.get_bounding_box()
#         inner = gdspy.Rectangle(bb[:,0], bb[:,1], layer = bond_layers[0], datatype= datatype)
#         inner = gdspy.offset(inner, gap, layer = bond_layers[0], datatype= datatype)
#         outer = gdspy.offset(inner, seal_width, layer = bond_layers[0], datatype = datatype)
#         ring = gdspy.boolean(outer, inner, 'not', layer = bond_layers[0], datatype = datatype)
#         x_pos = outer.get_bounding_box()[0,0] + (outer.get_bounding_box()[1,0]-outer.get_bounding_box()[0,0] - opening)/2
#         y_pos = outer.get_bounding_box()[0,1]
#         not_ring = gdspy.Rectangle((x_pos, y_pos), (x_pos + opening,y_pos + seal_width))
#         ring = gdspy.boolean(ring, not_ring, 'not', layer=bond_layers[0], datatype = datatype)
#         ring2 = gdspy.boolean(ring, None, 'or', layer = bond_layers[1], datatype= datatype)
#         cell.add((ring, ring2))
#         return ring, ring2




 
def naive_etch_holes(polygon:gdspy.PolygonSet, hole_size, spacing, not_layer, datatype =0):
    bb = polygon.get_bounding_box()
    pitch = hole_size+spacing
    hole = gdspy.Rectangle((-hole_size/2, hole_size/2), (hole_size/2, -hole_size/2))
    xdim = bb[1,0] - bb[0,0]
    ydim = bb[0,1] - bb[1,1]
    x_holes = int(np.floor((xdim - spacing)/pitch))
    y_holes = int(np.floor((ydim - spacing)/pitch))
    center = np.mean(bb, axis =0)
    x = np.arange(bb[0,0] , bb[1,0] -spacing, pitch)
    y = np.arange(bb[0,1], bb[1,1] - spacing, pitch)
    x_coors, y_coors = np.meshgrid(x,y)
    # x_cent = (np.amax(x_coors) + np.amin(x_coors))/2
    # y_cent = (np.amax(y_coors) + np.amin(y_coors))/2
    # cent = np.array([x_cent,y_cent])
    # offset = np.mean(bb, axis =0) - cent
    # x_coors += offset[0]
    # y_coors += offset[1]
    x_coors = x_coors.flatten()
    y_coors = y_coors.flatten()
    holes = []
    for xx,yy in zip(x_coors, y_coors):
        p1 = np.array((xx,yy))-np.array((hole_size, hole_size))/2
        p2 = p1 + np.array((hole_size, hole_size))
        rect = gdspy.Rectangle(p1,p2, not_layer, datatype)
        holes.append(rect)
        shrunk = gdspy.offset(polygon, -MIN_FEATURE, layer = not_layer, datatype=datatype, join_first=True) ##offsets shape to check if etchholes within minimum feature of edge
    # holes = gdspy.boolean(shrunk, holes, 'and', layer = not_layer, datatype = datatype)
    filtered_holes = []
    # filtered_holes = holes
    for i, hole in enumerate(holes):
        if gdspy.inside(hole.polygons, shrunk, 'all')[0]:
            filtered_holes.append(hole)
        # else: ## this adds a potentially smaller hole
        #     hole = gdspy.boolean(hole, shrunk, 'and', layer=not_layer, datatype= datatype)
        #     if smaller_allowed:
        #         if hole is not None:
        #             s_hole = gdspy.offset(hole, -min_feature/2)
        #             hole.fillet(min_feature/2, points_per_2pi = 4)
        #             # hole = gdspy.offset(hole, min_feature/2, layer = not_layer, datatype = datatype)
        #             # hole = gdspy.boolean(hole, shrunk, 'and', layer=not_layer, datatype= datatype)
        #             if s_hole is None: #not counted if hole smaller than min feature size
        #                 hole = None
        #         if hole is not None:
        #             filtered_holes.append(hole)
    return filtered_holes        
def expand_etch_holes_smart(polygon, holes, layer, not_layer,undercut,hole_size, lib: gdspy.GdsLibrary=gdspy.current_library,datatype = 0, count =0):
    """ Checks if released, enlarges holes in turs (y first, then x), until it is fully released"""
    test_cell = lib.new_cell('Release_test_cell' + str(id(polygon)) +'_'+ str(count))
    test_cell.add(polygon)
    test_cell.add(holes)
    anchors = compute_anchors(test_cell, undercut=undercut, layer = layer, not_layer=not_layer, datatype=datatype, output_datatype=datatype+100)
    if anchors is None:
        lib.remove(test_cell)
        print(f"Structure is released after {count} iterations ")
        return holes
    try:
        iter(anchors)
    except:
        anchors = [anchors]
    for anchor in anchors:
        ## find all the etcholes within anchor
        anchor_off = gdspy.offset(anchor, undercut + hole_size/2, join = 'round', layer = 100,datatype=100)
        test_cell.add(anchor_off)
        for i,hole in enumerate(holes):
            if isinstance(hole, gdspy.CellReference):
                corners = hole.get_polygons()[0]
                polygonsets = hole.get_polygonsets()[0]
            if isinstance(hole, gdspy.PolygonSet):
                corners = hole.polygons[0]
                polygonsets = hole
            if gdspy.inside([corners], anchor_off, 'any')[0]: #if the hole is closed to unreleased anchor, make larger
                #find min norm
                diffs = [] 
                for j in range(corners.shape[0]): ## this code block finds the closest corner
                    diff = np.concatenate(anchor.polygons) - corners[j,:]
                    argin = np.argmin(np.linalg.norm(diff, axis =1))
                    this_diffin = diff[argin,:]
                    diffs.append(this_diffin)
                diffs = np.array(diffs)
                arg = np.argmin(np.linalg.norm(diffs, axis =1))
                this_diff = diffs[arg, :]  #this is the distance to the closest anchor
                
                dir = np.argmax(np.abs(this_diff))
                old_hole = polygonsets
                sign = np.sign(this_diff[dir])
                growth = abs(this_diff[dir]) - (undercut)/2 + 0.1 #takes a step towards the anchor
                if dir == 0 and (count%2 ==1) : #x growth, this fucntion does y first, then x. This avoids weak areas and overcorrection
                    new_hole1 = gdspy.copy(old_hole)
                    new_hole2 = gdspy.copy(old_hole, sign* growth, 0)
                    new_hole = gdspy.boolean(new_hole1, new_hole2, 'or', layer = not_layer, datatype=datatype)
                elif dir ==1 and (count%2 ==0): # y growth
                    new_hole1 = gdspy.copy(old_hole)
                    new_hole2 = gdspy.copy(old_hole,0, sign* growth)
                    new_hole = gdspy.boolean(new_hole1, new_hole2, 'or', layer = not_layer, datatype=datatype)
                else:
                    break
                #this part just makes sure we don't get to close to the edges. DRC should still catch errors
                reduced = gdspy.offset(polygon, -undercut/2, join ='round', join_first=True, precision = 1)
                new_hole= gdspy.boolean(new_hole, reduced, 'and', layer = not_layer, datatype=datatype)
                holes[i] = new_hole      
                # new_hole = shifted_hole
    lib.remove(test_cell)
    return expand_etch_holes_smart(polygon, holes, layer, not_layer, undercut, hole_size, lib, datatype, count = count +1 )


def add_etch_holes(polygon, hole_size, undercut, not_layer,lib: gdspy.GdsLibrary=gdspy.current_library,datatype = 0, shape = 'square', min_feature = 2, num_of_points = None, geometry = 'manhattan', R_in = 0, fracture = False, smaller_allowed = True, center = None, skip_filter = False):
    """ 
    General function to add etch holes to an area
    Parameters
    ----------
    polygon: polygon to add etch holes to
    hole_size : etch hole diameter or square size
    undercut: maximum distance between etch hole edges
    not_layer : layer for etch holes
    datatype : 0 by default
    shape: ('square', 'round') etch hole shape 
    geometry: ('manhattan', 'hex', 'rings')
    num_of_points : number of points if shape is circle
    smaller_allowed: weather smaller holes are permited (larger than mean feature)
    center: provided for circular cases where we want to revolve around a given center
    smart: allows for larger holes by checking if it releases and enlarging
    """
        
    if not isinstance( polygon, gdspy.polygon.PolygonSet):
        try:
            iterator = iter(polygon)
        except TypeError:
            return None
        output = []
        for this_poly in polygon:
            holes = add_etch_holes(this_poly, hole_size, undercut, not_layer,lib, datatype = datatype, shape = shape, min_feature = min_feature, geometry=geometry, fracture=fracture, smaller_allowed=smaller_allowed, center = None, num_of_points=num_of_points)
            if holes is not None:
                output.extend(holes)
        return output     
    if fracture: 
        pols = polygon.fracture(max_points = 5)
        results = []
        for pol in pols.polygons:
            pol = gdspy.Polygon(pol)
            center = np.mean(pol.get_bounding_box(), axis =0)
            polu = gdspy.copy(pol, 0, undercut)
            poluu = gdspy.copy(pol, 0, 2*undercut)
            polb = gdspy.copy(pol, 0, -undercut)
            polr = gdspy.copy(pol, undercut, 0)
            polrr = gdspy.copy(pol, 2*undercut, 0)

            poll = gdspy.copy(pol, -undercut, 0)
            # if geometry != 'naive':
            pol = gdspy.boolean((pol, polu,poluu, polb, polr, polrr, poll), pols, 'and')
            results += add_etch_holes(pol, hole_size, undercut, not_layer, lib, datatype, shape, min_feature, num_of_points, geometry, R_in, fracture =False, smaller_allowed=smaller_allowed, center=center, num_of_points=num_of_points)
            # results.append(pol)
        return results   
    if not_layer is None:
        not_layer = polygon.layers[0] + 1
    bb = polygon.get_bounding_box()
    if shape == 'square':
        diagonal = undercut + hole_size * np.sqrt(2)
    if shape == 'circle':
        diagonal = undercut + hole_size
    if geometry == 'naive':
        manhattan = np.array([undercut+hole_size, undercut + hole_size])
        y = np.arange(bb[0,1], bb[1,1]-undercut, manhattan[1])
        x = np.arange(bb[0,0], bb[1,0]-undercut, manhattan[0])
        # print(x,y)
        x_coors,y_coors = np.meshgrid(x,y)
    if geometry == 'manhattan':
        manhattan = diagonal / np.sqrt(2) ##distance for manhattan etch holes
        dims = bb[1]-bb[0] -1* manhattan
        dims = np.maximum(dims, np.array([0,0]))#solves case were only 1 row or column is needed
        num_holes = np.maximum(np.floor(dims/manhattan), np.array([1,1]))
        manhattan = dims/num_holes  #might be smaller to ensure enough holes
        if num_holes[0]<= 1 or num_holes[1]<=1:
            manhattan = undercut + hole_size
            dims = bb[1]-bb[0] -2* manhattan+hole_size
            dims = np.maximum(dims, np.array([0,0]))#solves case were only 1 row or column is needed
            num_holes = np.maximum(np.floor(dims/manhattan), np.array([1,1]))
            manhattan = dims/num_holes 
        if num_holes[0]>1:
            x = np.arange(bb[0,0], bb[1,0]-manhattan[0], manhattan[0])
        else:
            x = np.array(np.mean(bb[:,0]))
        if num_holes[1]>1:  
            y = np.arange(bb[0,1], bb[1,1]-manhattan[1], manhattan[1])
        else:
            y = np.array(np.mean(bb[:,1]))

        x_coors,y_coors = np.meshgrid(x,y)
    if geometry == 'hex':
        ##based on https://laurentperrinet.github.io/sciblog/posts/2020-04-16-creating-an-hexagonal-grid.html
        # print('diagonal ', diagonal)
        if shape =='square':
            diagonal = undercut + hole_size/np.sqrt(2)*0.8 ###somewhat hueristic, works
        ratio = np.sqrt(3)/2
        manhattan = np.array([diagonal*ratio, diagonal])/(ratio)
        x = np.arange(bb[0,0]+min_feature, bb[1,0]-min_feature, manhattan[0])
        y = np.arange(bb[0,1]+min_feature, bb[1,1]-min_feature, manhattan[1])
        # num_x = len(x)
        # num_y = len(y)
        # x = np.linspace(bb[0,0], bb[1,0], num_x)
        # y = np.linspace(bb[0,1], bb[1,1], num_y)
        # manhattan[0] = x[1]-x[0]
        # manhattan[1] = y[1]-y[0]
        #center of x_coors

        x_coors,y_coors = np.meshgrid(x,y)
        y_coors[:,::2] -= manhattan[1]/2 
        y_coors[:,1::2] += manhattan[1]/4*0
   
    if geometry == 'rings':
        x_coors = []
        y_coors = [] 
        dims = bb[1]-bb[0]
        center = np.mean(bb, axis =0)
        radius = np.max(dims)/2
        if R_in ==0:
            num_rings = np.ceil(radius/(diagonal))
            rad_step = radius/num_rings
            for ring_ind in range(int(num_rings)):
                if ring_ind == 0:
                    x_coors.append(center[0])
                    y_coors.append(center[1])
                num = 6*ring_ind
                rad = rad_step*ring_ind
                for tan_ind in range(num):
                    angle = tan_ind* np.pi*2/num
                    x_coors.append(center[0]+ rad*np.cos(angle))
                    y_coors.append(center[1] + rad*np.sin(angle))
        else:
            inner_ring_ind = int(np.floor(R_in/diagonal))
            num_rings = np.ceil((radius-R_in)/(diagonal))
            rad_step = (radius-R_in)/num_rings
            for ind in range(int(num_rings)):
                ring_ind = inner_ring_ind + ind
                # print(ring_ind)
                if ring_ind == 0:
                    x_coors.append(center[0])
                    y_coors.append(center[1])
                num = 6*ring_ind
                rad = rad_step*ind + R_in
                for tan_ind in range(num):
                    angle = tan_ind* np.pi*2/num
                    x_coors.append(center[0]+ rad*np.cos(angle))
                    y_coors.append(center[1] + rad*np.sin(angle))



        # x_coors = x_coors + hole_size/2
        # y_coors = y_coors + hole_size
    ##center with respect to bb
    x_cent = (np.amax(x_coors) + np.amin(x_coors))/2
    y_cent = (np.amax(y_coors) + np.amin(y_coors))/2
    cent = np.array([x_cent,y_cent])
    if center is None:
        center =  np.mean(bb, axis =0)
    offset = center - cent
    x_coors += offset[0]
    y_coors += offset[1]
    x_coors = x_coors.flatten()
    y_coors = y_coors.flatten()
    holes = []
    if skip_filter:
        ##currently useless piece of code, attempt at reducing comp time
        if shape == 'square':
            p1 = np.array((0,0))-np.array((hole_size, hole_size))/2
            p2 = p1 + np.array((hole_size, hole_size))
            hole = gdspy.Rectangle(p1,p2, not_layer, datatype)
        if shape == 'circle':
            p1 = np.array((0,0))
            if num_of_points is None:
                num_of_points = 10
            hole = gdspy.Round(p1, hole_size/2,layer = not_layer, datatype=datatype, number_of_points= num_of_points)
        hole_cell = lib.new_cell('holes'+ str(id(polygon)))
        hole_cell.add(hole)
        holes.append(hole_cell)
        for xx,yy in zip(x_coors, y_coors):
            hole_ref = gdspy.CellReference(hole_cell, (xx,yy))
            holes.append(hole_ref)
        return holes  
    if shape == 'square':
        p1 = -np.array((hole_size, hole_size))/2
        p2 = -p1
        hole = gdspy.Rectangle(p1,p2, not_layer, datatype)
    if shape == 'circle':
        if num_of_points is None:
            num_of_points = 10
        hole = gdspy.Round((0,0), hole_size/2, layer = not_layer, datatype = datatype, number_of_points=num_of_points)
    hole_cell = lib.new_cell('holes'+ str(id(polygon)))
    hole_cell.add(hole)
    for xx,yy in zip(x_coors, y_coors):
        hole_ref = gdspy.CellReference(hole_cell, (xx,yy))
        holes.append(hole_ref)
        # if shape == 'square':
        #     p1 = np.array((xx,yy))-np.array((hole_size, hole_size))/2
        #     p2 = p1 + np.array((hole_size, hole_size))
        #     rect = gdspy.Rectangle(p1,p2, not_layer, datatype)
        #     # holes.append(rect)
        # if shape == 'circle':
        #         p1 = np.array((xx,yy))
        #         if num_of_points is None:
        #             num_of_points = 10
        #         circ = gdspy.Round(p1, hole_size/2,layer = not_layer, datatype=datatype, number_of_points= num_of_points)
        #         holes.append(circ)
    shrunk = gdspy.offset(polygon, -min_feature, layer = not_layer, datatype=datatype, join_first=True) ##offsets shape to check if etchholes within minimum feature of edge
    shrunk2 = gdspy.offset(polygon, -hole_size, layer = not_layer, datatype=datatype, join_first=True)
    # holes = gdspy.boolean(shrunk, holes, 'and', layer = not_layer, datatype = datatype)

    filtered_holes = []
    # filtered_holes = holes
    for i, hole_ref in enumerate(holes):
        if gdspy.inside([hole_ref.origin], shrunk2, 'all')[0]:
            filtered_holes.append(hole_ref)
        else:
            # print(hole_ref.origin)
            hole = hole_ref.get_polygonsets()[0]

            if gdspy.inside(hole.polygons, shrunk, 'all')[0]:
                filtered_holes.append(hole_ref)
            else: ## this adds a potentially smaller hole
                hole = gdspy.boolean(hole, shrunk, 'and', layer=not_layer, datatype= datatype)
                if smaller_allowed:
                    if hole is not None:
                        s_hole = gdspy.offset(hole, -min_feature/2)
                        hole.fillet(min_feature/2, points_per_2pi = 4)
                        # hole = gdspy.offset(hole, min_feature/2, layer = not_layer, datatype = datatype)
                        # hole = gdspy.boolean(hole, shrunk, 'and', layer=not_layer, datatype= datatype)
                        if s_hole is None: #not counted if hole smaller than min feature size
                            hole = None
                    if hole is not None:
                        filtered_holes.append(hole)
    return filtered_holes

def compute_anchors(cell: gdspy.Cell, undercut, layer=0, not_layer=None, re_layer=None, datatype = 0,  output_layer = None, output_datatype =None, join = 'round', do_math= True):
    """computes anchors in cell with specified undercut
    Parameters
    ----------
    cell: desired cell to do this in
    undercut: distance of etch
    layer, not_layer, re_layer: layer numbers for boolean operations
    datatype: datatype of input layers
    output_layer, output_datatype: for anchor structure, if none provided layer set the same and datype is 1 more  """
    polsets = cell.get_polygonsets()
    if not_layer is None:
        not_layer = layer +1 
    if re_layer is None:
        re_layer = layer +2 

    if output_layer is None:
        output_layer = layer
        if output_datatype is None:
            output_datatype = datatype + 1
    layers = {'layer': layer, 'not_layer': not_layer, 're_layer':re_layer}
    pols = {'layer': [], 'not_layer': [], 're_layer': []}
    for pol in polsets:
        for key in layers:
            if layers[key] in pol.layers:
                pols[key].append(pol)
    # pols['layer'] = gdspy.offset(pols['layer'] , 0.2, layer = layers['layer'], datatype=datatype, max_points=20000)
    # pols['layer'] = gdspy.offset(pols['layer'], -undercut, max_points=200000, join_first=True, join='round')
    # pols['not_layer'] = gdspy.offset(pols['not_layer'],undercut, max_points=200000, join = 'miter', tolerance = 2)
    # pols['re_layer'] = gdspy.offset(pols['re_layer'], -undercut, max_points=200000)
    if do_math:
        anchor = gdspy.boolean(pols['layer'], pols['not_layer'], 'not', max_points=2000, layer = output_layer, datatype=output_datatype)
    else:
        anchor = pols['layer']
    anchor = gdspy.offset(anchor, -undercut, join=join, join_first = True, max_points = 2000, layer = output_layer, datatype=output_datatype)
    if (anchor is not None) and do_math:
        anchor = gdspy.boolean(anchor, pols['re_layer'], 'or', layer = output_layer, datatype=output_datatype, max_points=2000)
    # anchor = gdspy.offset(anchor, -undercut, max_points=20000)
    if anchor is not None:
        cell.add(anchor)
    else:
        print(cell.name, ' is not anchored')
    return anchor


class SealedMotor(RotMotor):
    def add_dummy_silicon(self,space = 10, not_dummy = NOT_DUMMY, output_datatype = 0):
        """ adds a not dummy edge around motor componets for insulation"""
        polygons = compute_anchors(self, -space, layer= SOI, output_layer = not_dummy, output_datatype=output_datatype, join = 'miter', do_math = False)
        self.space = space
        self.not_dummy = polygons 
        self.add(polygons)
    def flagellar_seal(self, overhang = 20, metal_gap = 7, metal_width = 90, top_metal = TOP_METAL, bottom_metal = METAL, top_via = TOP_VIA, datatype = 0, hydrophobic_fingers = False, num_points = None, num_circ_points = None):
        """ adds a flagellar seal around the flagella"""
        if num_points is not None:
            via_circle = gdspy.Round(self.origin, self.rot_shuttle.R_out - overhang, layer = top_via, datatype= datatype, number_of_points= num_points)
        else:
            via_circle = gdspy.Round(self.origin, self.rot_shuttle.R_out - overhang, layer = top_via, datatype= datatype)
        self.via_circle = via_circle
        if hydrophobic_fingers:
            fingers, temp = hydro.add_fingers_to_shape(via_circle, self.lib, length=5, width =3 , spacing =3, layer = top_via)
            self.add(fingers)
        self.add(via_circle)
        invalid_area = gdspy.offset(self.not_dummy, metal_gap, join_first=True)
        if num_circ_points is None:
            invalid_area = gdspy.boolean(invalid_area, gdspy.Round(self.origin, self.rot_shuttle.R_out + metal_gap + self.space + 2), 'or')
        else:
            invalid_area = gdspy.boolean(invalid_area, gdspy.Round(self.origin, self.rot_shuttle.R_out + metal_gap + self.space + 3.5, number_of_points= num_circ_points), 'or')
        if num_circ_points is None:
            metal_circle = gdspy.Round(self.origin, self.rot_shuttle.R_out + 10 + metal_width, layer = top_metal, datatype = datatype)
        else:
            metal_circle = gdspy.Round(self.origin, self.rot_shuttle.R_out + 10 + metal_width, layer = top_metal, datatype = datatype, number_of_points=num_circ_points)
        traces = gdspy.boolean(metal_circle, invalid_area, 'not', layer = top_metal, datatype = 0)
        traces2 = gdspy.boolean(traces, None, 'or', layer= bottom_metal, datatype =0)
        self.traces = [traces,traces2]
        self.add(self.traces)
    
    def connect_gca_sides(self, width = 100, layer = SOI, datatype = 0):
        gca = self.gca
        left_stator = gca.stator_anchors[1].get_bounding_box()
        right_stator = gca.stator_anchors[0].get_bounding_box()
        gca_bb = gca.get_bounding_box()
        pt1 = np.array([np.mean(left_stator, axis =0)[0], left_stator[0,1]])
        if abs(left_stator[0,0] -left_stator[1,0])< width:
            pt1[0] = left_stator[1,0] - width/2
        pt2 = np.array([pt1[0], gca_bb[0,1] - width/2 - self.gca.space])
        pt3 = np.array([gca_bb[1,0] + width/2 + gca.space, pt2[1]])
        pt4y = gca.flexure_ref[0].get_bounding_box()[1,1] + gca.space + width/2 
        pt4 = np.array([pt3[0], pt4y])
        pt5 = np.array([right_stator[1,0],pt4[1]])
        interconnect = gdspy.FlexPath((pt1, pt2, pt3,pt4, pt5), width, layer = layer, datatype = datatype)
        gca.add((interconnect.to_polygonset(), interconnect))
    def connect_opposite_pairs(self, width = 200, soi_layer = SOI, metal_layer = METAL, top_metal_layer =TOP_METAL, datatype =0, extra_space= 0, metal_gap = 5):
        """connects the opposite pairs of the GCA"""
        bb = self.get_bounding_box()
        def stator_anchors_disc(cell, side = 'right'):
            if side == 'right':
                i = 1
            elif side == 'left':
                i = 0
            if isinstance(cell, GCA):
                return cell.stator_anchors[i].get_bounding_box()
            else:
                return []
                
        left_anchors = np.array(get_cell_pts(lambda cell: stator_anchors_disc(cell, side = 'left'), self)).reshape((4,2,2))
        right_anchors = np.array(get_cell_pts(lambda cell: stator_anchors_disc(cell, 'right'), self)).reshape((4,2,2))
        gca_bbs = [gca.get_bounding_box() for gca in self.gca_refs]
        gca_origins = np.array([ref.origin for ref in self.gca_refs])
        top_ind = gca_origins[:,1].argmax()
        bottom_ind = gca_origins[:,1].argmin()
        left_ind = gca_origins[:,0].argmin()
        right_ind = gca_origins[:,0].argmax()
        # print(gca_origins)
        # print(left_anchors)
        #top and bottom, through right side
        top_right_bb = right_anchors[top_ind]
        ycor =  gca_bbs[right_ind][:,1].max() + self.gca.space+ width/2 + extra_space
        ycor = max(ycor, top_right_bb[:,1].min() + width/2)
        pt0 = np.array([top_right_bb[:,0].max() - metal_gap*3,ycor])
        pt1 = np.array([bb[:,0].max() + width/2 + self.gca.space + extra_space, pt0[1]])
        bottom_right_bb = left_anchors[bottom_ind]
        ptend = np.array([bottom_right_bb[:,0].max()-metal_gap*3, bottom_right_bb[:,1].mean()])
        pt2 = np.array([pt1[0], ptend[1]])
        interconnect = gdspy.FlexPath((pt0, pt1, pt2, ptend), width)
        # self.add(interconnect.to_polygonset())
        del pt0,pt1,pt2
        # connect top and bottom GCAS
        right_bottom_bb = right_anchors[right_ind]
        left_bottom_bb = left_anchors[left_ind]
        xcor = gca_bbs[bottom_ind][:,0].max()+ width/2 + self.gca.space + extra_space
        xcor = max(xcor, right_bottom_bb[:,0].min() + width/2)
        pt0 = np.array([xcor, right_bottom_bb[:,1].min() + metal_gap*3])
        pt1 = np.array([pt0[0], gca_bbs[bottom_ind][:,1].min()-self.gca.space - extra_space -width/2])
        pt2 = np.array([left_bottom_bb[:,0].mean(),pt1[1]])
        pt3 = np.array([pt2[0], left_bottom_bb[:,1].min() +  metal_gap*3])
        interconnect2 = gdspy.FlexPath((pt0, pt1, pt2, pt3), width)
        make_necking_bridge(self, interconnect, interconnect2, neck_width = 100, soi_layer=soi_layer, metal_layer=metal_layer, top_metal_layer=top_metal_layer, datatype=datatype, metal_gap=metal_gap, gap = self.gca.space)
        self.interconnect_A = interconnect2
        self.interconnect_B = interconnect
    def metalize_GCAs(self, offset = 6, soi_layer = SOI, not_soi_layer = SOI_HOLE, metal_layer = METAL, top_metal_layer =TOP_METAL, datatype =0):
        """metalizes cell by offseting from soi_layer """
        gca = self.gca
        gca.metalize(offset=offset, soi_layer=soi_layer, metal_layer=metal_layer, top_metal_layer=top_metal_layer, datatype=datatype)
        # metalized = compute_anchors(gca, offset, layer = soi_layer, not_layer = not_soi_layer, re_layer= soi_layer + 2,  datatype= datatype, output_layer=metal_layer, output_datatype= datatype, join = 'miter')
        # top_metal = gdspy.boolean(metalized, None, 'or', layer = top_metal_layer, datatype= datatype)
        # if top_metal is not None:
        #     gca.add(top_metal)
    def connect_grounds(self, width =3,layer = SOI, datatype = 0):
        """Connects grounds with serpentine springs"""
        gca_origins = np.array([ref.origin for ref in self.gca_refs])
        def shuttle_connecting_pts(cell):
            if isinstance(cell, SingleFlexure):
                return [np.array([-self.gca.bar_width,0])]
            else:
                return []
        def anchor_top_line(cell, left = True):

            if isinstance(cell, SingleFlexure):
                if left:
                    return [cell.get_bounding_box()[1,:] + np.array([-cell.anchor_width, 0])]
                else:
                    return [cell.get_bounding_box()[1,:]]
            else:
                    return []
        top_ind = gca_origins[:,1].argmax()
        bottom_ind = gca_origins[:,1].argmin()
        left_ind = gca_origins[:,0].argmin()
        right_ind = gca_origins[:,0].argmax()
        shuttle_connection_pts = [get_cell_pts(shuttle_connecting_pts, ref)[1] for ref in self.gca_refs]
        anchor_pt1 = [get_cell_pts(lambda cell: anchor_top_line (cell, left = True), ref)[1] for ref in self.gca_refs]
        anchor_pt2 = [get_cell_pts(lambda cell: anchor_top_line (cell, left = False), ref)[1] for ref in self.gca_refs]
        order = [right_ind, bottom_ind, left_ind, top_ind]
        for i in range(len(order)):
            this_ind = order[i%len(order)]
            next_ind = order[(i+1)%len(order)]
            pt1 = shuttle_connection_pts[this_ind]
            pt2_1 = anchor_pt1[next_ind]
            pt2_2 = anchor_pt2[next_ind]
            vec2 = pt2_2-pt2_1
            vec2_n = vec2 / np.linalg.norm(vec2)
            perp2 = np.array([[0,-1], [1,0]]) @vec2_n
            vecf = (pt2_1 - pt1)@perp2
            pt2 = pt1 + vecf *perp2
            spring = SerpentineSpring(None, self.lib, pt2, pt1, width = width, end_length= self.gca.anchor_width + 2 * self.gca.space + 10 , layer = self.gca.layer, datatype = self.gca.datatype)
            spring.add_not_dummy()
            self.add(gdspy.CellReference(spring))
            
    def traces_to_pads(self, trace_width = 300, metal_gap =5, soi_layer = SOI, top_metal = TOP_METAL, bottom_metal = METAL, via_layer = TOP_VIA, not_dummy_layer = NOT_DUMMY, space = 10, datatype =0, extra_length = 0):
        """ Produces 3 terminals for all pads"""
        gca_origins = np.array([ref.origin for ref in self.gca_refs])
        top_ind = gca_origins[:,1].argmax()
        #ground
        #connect SOI to ground plain
        def anchor_top_line(cell, left = True):

            if isinstance(cell, SingleFlexure):
                if left:
                    return [cell.get_bounding_box()[1,:] + np.array([-cell.anchor_width, 0])]
                else:
                    return [cell.get_bounding_box()[1,:]]
            else:
                    return []
        left_pt = get_cell_pts(lambda cell: anchor_top_line(cell, left =True), self.gca)[1]
        rect = gdspy.Rectangle(left_pt, left_pt+  np.array([self.gca.anchor_width + 20, -self.gca.anchor_width -20]), layer = soi_layer,datatype= datatype)
        # rect2 = gdspy.offset(rect, -metal_gap, layer = bottom_metal,datatype=datatype)
        # self.gca.add((rect, rect2))
        self.gca.add(rect)
        rect = gdspy.Rectangle(left_pt + np.array([-self.gca.anchor_width/3, self.gca.anchor_width/3]) , left_pt+np.array([0, -self.gca.anchor_width/3]), layer = soi_layer,datatype= datatype)
        self.gca.add(rect)


        left_pt_top_right= get_cell_pts(lambda cell: anchor_top_line(cell, left =True), self.gca_refs[top_ind])[1]
        metal_rect = gdspy.Rectangle(left_pt_top_right, left_pt_top_right+  np.array([-trace_width, trace_width]), layer = soi_layer ,datatype= datatype)
        metal_rect = gdspy.offset(metal_rect, -metal_gap, layer = bottom_metal, datatype=0)
        top_metal_rect = gdspy.boolean(metal_rect, None,'or', layer = top_metal,datatype=datatype)
        self.add((metal_rect, top_metal_rect))

        ### compute centers for 3 pads
        bb = self.get_bounding_box()
        width = bb[1,0] - bb[0,0]
        bottom_center = bb[0,:] + np.array([width/2,0])
        pad = ContactPad(None, self.lib, length = trace_width, width = trace_width, soi_layer = soi_layer, bottom_metal= bottom_metal, via_layer=via_layer, not_dummy_layer= not_dummy_layer, not_dummy_gap=space, datatype=0, metal_gap=metal_gap)

        ptgrnd = bottom_center + np.array([-1.5*trace_width, - trace_width - extra_length])
        pt_A =  bottom_center + np.array([0, - trace_width- extra_length])
        pt_B = bottom_center + np.array([1.5*trace_width, - trace_width- extra_length])
        # self.add(gdspy.CellReference(pad,ptgrnd))
        self.add(gdspy.CellReference(pad,pt_A))
        self.add(gdspy.CellReference(pad, pt_B))
        
        ground_pad = ContactPad(None, self.lib, length = trace_width, width = trace_width, soi_layer = soi_layer, bottom_metal= bottom_metal, via_layer=via_layer, not_dummy_layer= not_dummy_layer, not_dummy_gap=-space, datatype=0, metal_gap=metal_gap)
        self.add(gdspy.CellReference(ground_pad, ptgrnd))
        ## connect A to trace
        #find bottom pts of interconnect
        pts = self.interconnect_A.points
        bottom_pt = pts[pts[:,1].argmin(),:]
        vert = np.array([0,1])
        pt0 = pt_A + ((bottom_pt - pt_A)@vert) * vert
        connection = gdspy.FlexPath((pt0, pt_A), width = trace_width, layer = soi_layer, datatype = datatype)
        self.add(connection)
        self.add(gdspy.offset(connection, space, layer = not_dummy_layer, datatype =datatype))

        #connect B to 
        pt_B0 = pt_B
        max_A = pts[pts[:,0].argmax(),0]
        # print(max_A)
        pt_B = np.array([max_A + 1.5*trace_width, pt_B[1]])
        pts = self.interconnect_B.points
        bottom_pt = pts[pts[:,1].argmin(),:]
        pt0 = pt_B + ((bottom_pt - pt_B)@vert) * vert
        connection_B = gdspy.FlexPath((pt0, pt_B, pt_B0), width = trace_width, layer = soi_layer, datatype = datatype)
        self.add(connection_B)
        
        self.add(gdspy.offset(connection_B, space, layer = not_dummy_layer, datatype =datatype))

        
        ##connect ground trace to plane

        
class ContactPad(gdspy.Cell):
    def __init__(self, name, lib: gdspy.GdsLibrary, length = 300, width = 300, soi_layer = SOI, bottom_metal = METAL, metal_gap =5,  via_layer = TOP_VIA, not_dummy_layer = NOT_DUMMY, not_dummy_gap = 10, datatype =0, exclude_from_current=False):
        if name is None:
            self.name = 'Pad_' + str(id(self))
        else: 
            self.name = name

        gdspy.Cell.__init__(self, self.name, exclude_from_current=exclude_from_current)
        self.lib = lib 
        self.lib.add(self)
        self.length = length
        self.width = width
        self.metal_gap = metal_gap
        length = length + 2*metal_gap
        width = width + 2*metal_gap
        soi_rect = gdspy.Rectangle([-length/2, -width/2], [length/2, width/2], layer = soi_layer, datatype=datatype)
        not_dummy_rect = gdspy.offset(soi_rect, not_dummy_gap, layer = not_dummy_layer, datatype= datatype)

        metal_rect = gdspy.offset(soi_rect, -metal_gap, layer = bottom_metal, datatype= datatype)
        via_rect = gdspy.offset(metal_rect, -metal_gap, layer = via_layer, datatype=datatype)
        self.add((soi_rect, metal_rect, via_rect))
        if not_dummy_rect is not None:
            self.add(not_dummy_rect)


        
class SerpentineSpring(gdspy.Cell):
    def __init__(self, name, lib: gdspy.GdsLibrary,start_pt, end_pt, width =3, period = 18, amplitude = 40, start_length = 15, end_length = 25,  n_turns = None, exclude_from_current=False, layer = SOI, datatype = 0 ):
        if name is None:
            self.name = 'Serpentine_' + str(id(self))
        else: 
            self.name = name
        gdspy.Cell.__init__(self, self.name, exclude_from_current=exclude_from_current)
        self.lib = lib 
        self.lib.add(self)
        self.start_pt = np.array(start_pt)
        self.end_pt = np.array(end_pt)
        direction = self.end_pt - self.start_pt
        distance = np.linalg.norm(direction)
        self.distance = distance
        direction = direction / distance
        if n_turns is None:
            self.n_turns = int(np.floor((distance - start_length-end_length - width)/(period)))
        else:
            self.n_turns = n_turns
        self.period = period
        tails_error = distance - self.n_turns *period - width - start_length - end_length
        self.start_length = start_length + tails_error/2
        self.end_length = end_length + tails_error/2

        self.amplitude = amplitude
        self.width = width
        self.layer = layer
        self.datatype = datatype
        self.draw_spring()
        self.add(gdspy.CellReference(self.subcell, start_pt, np.rad2deg(np.arctan2(direction[1], direction[0])) ))
    def draw_spring(self):
        self.subcell = self.lib.new_cell('SC' + self.name)
        pts = []
        pts.append(np.array([0,0]))
        pts.append(pts[0] + np.array([self.start_length + self.width/2, 0]))
        y_diff = np.array([0, self.amplitude])
        x_diff = np.array([self.period/2, 0])

        # 1st turn
        pts.append(pts[-1] - y_diff/2)
        pts.append(pts[-1] + x_diff)

        for i in range(self.n_turns-1):
            pts.append(pts[-1] + y_diff)
            pts.append(pts[-1] + x_diff)
            pts.append(pts[-1] - y_diff)
            pts.append(pts[-1] + x_diff)
        
        #last 1/2 turn 
        pts.append(pts[-1]+ y_diff)
        pts.append(pts[-1] + x_diff)
        pts.append(pts[-1] - y_diff/2)
        pts.append(pts[0] + np.array([self.distance, 0]))
        self.pts = pts
        trace = gdspy.FlexPath(pts, self.width, layer = self.layer, datatype=self.datatype)
        self.subcell.add(trace)
    def add_not_dummy(self, gap = 5,  layer = NOT_DUMMY, datatype = 0):
        """adds a shell for not dummy around it"""
        #split into sections
        #start edge
        pts = self.pts 
        start = gdspy.FlexPath([pts[0], pts[1]], self.width + 2* gap, layer = layer, datatype = datatype)
        self.subcell.add(start)
        end = gdspy.FlexPath([pts[-1],pts[-2]], self.width + 2*gap, layer = layer, datatype = datatype)
        self.subcell.add(end)

        # rest
        path = gdspy.FlexPath(pts[2:-2], self.width).to_polygonset()
        middle_section = gdspy.Rectangle(path.get_bounding_box()[0,:], path.get_bounding_box()[1,:])
        middle_section = gdspy.offset(middle_section, gap, layer = layer, datatype = datatype)
        self.subcell.add(middle_section)

#####################################################################################################################
########TEST STRUCTURES###########################
class Vernier(gdspy.Cell):
    def __init__(self, name, lib: gdspy.GdsLibrary, finger_width=2,finger_length =15,
    N_left=5, N_right=6, pitch_left = 10, pitch_right =8, gap = 4,   bar_thickness =5,
    layer = SOI, not_layer = SOI_HOLE, datatype=0, exclude_from_current = False):
        if name is None:
            self.name = 'Vernier_' + str(id(self))
        else: 
            self.name = name
        gdspy.Cell.__init__(self, self.name, exclude_from_current=exclude_from_current)
        self.lib = lib 
        self.lib.add(self)
        self.layer = layer
        self.datatype = datatype
        self.N_left = N_left
        self.N_right = N_right
        self.pitch_left = pitch_left
        self.pitch_right = pitch_right
        self.finger_width = finger_width
        self.finger_length= finger_length
        self.bar_thickness = bar_thickness
        self.gap = gap
        self.not_layer = not_layer
        self.combs = []
        self.right_comb = self.draw_comb(self.pitch_right, self.N_right)
        self.add(gdspy.CellReference(self.right_comb))
        self.left_comb = self.draw_comb(self.pitch_left, self.N_left)
        self.add(y_reflection(self.left_comb, lib))
    def draw_comb(self, pitch, N):
        comb_cell = self.lib.new_cell(self.name + str(len(self.combs)))
        ll = np.array([self.gap/2, 0])
        width = self.bar_thickness + self.finger_length
        height = (N-1)*pitch + self.finger_width
        ur = ll + np.array([width, height])
        # print(ll, ur)
        rect = gdspy.Rectangle(ll, ur, layer = self.layer, datatype = self.datatype)
        comb_cell.add(rect)
        
        not_rect = gdspy.Rectangle(ll + np.array([0, self.finger_width]), ll + np.array([self.finger_length, pitch]), layer = self.not_layer, datatype=self.datatype)

        for i in range(N-1):
            comb_cell.add(not_rect)
            not_rect = gdspy.copy(not_rect, 0, pitch)

        self.combs.append(comb_cell)

        return comb_cell

# class RotSpringTest(gdspy.Cell):
#     """This class has test structures to test for rotational spring spring constants"""
#     def __init__(self, name, lib: gdspy.GdsLibrary, shuttle: RotShuttle,
#                  layer = SOI, not_layer = SOI_HOLE, datatype=0, exclude_from_current = False):
#         if name is None:
#             self.name = 'RotSpringTest_' + str(id(self))
#         else: 
#             self.name = name
#         gdspy.Cell.__init__(self, self.name, exclude_from_current=exclude_from_current)
#         self.lib = lib
#         self.lib.add(self)
#         self.layer = layer
#         self.datatype = datatype 
#         self.shuttle = shuttle
#         self.add(gdspy.CellReference(self.shuttle))
#     def pulling_struct(self, spring_length =25, spring_width = 3, vernier =None):
#         self.pull = self.lib.new_cell(self.name + 'pull')
#         if vernier is None:
#             vernier = Vernier(None, self.lib, )



        

        
        
class SpringGauge(gdspy.Cell):
    def __init__(self, name, lib: gdspy.GdsLibrary, shuttle_length = 100, shuttle_width =40,  etch_size = 8, undercut = UNDERCUT,
    layer = SOI, not_layer = SOI_HOLE, datatype=0, exclude_from_current = False):
        if name is None:
            self.name = 'SpringGauge_' + str(id(self))
        else: 
            self.name = name
        gdspy.Cell.__init__(self, self.name, exclude_from_current=exclude_from_current)
        self.lib = lib 
        self.lib.add(self)
        self.layer = layer
        self.datatype = datatype
        self.shuttle_width = shuttle_width
        self.shuttle_length = shuttle_length
        self.not_layer = SOI_HOLE
        self.etch_hole_size = etch_size
        self.undercut = undercut
        self.space = 10
    def draw_shuttle(self):
        """Draw main shuttle with connection at 0,0"""
        self.shuttle = gdspy.Rectangle((-self.shuttle_width/2, 0), (self.shuttle_width/2, -self.shuttle_length), layer = self.layer, datatype = self.datatype)
        etch_holes = add_etch_holes(self.shuttle, self.etch_hole_size, self.undercut, self.not_layer, self.lib,  geometry = 'manhattan', shape= 'square')
        self.add(self.shuttle)
        self.add(etch_holes)
    def draw_parallel_springs(self, width = 2, length=150):
        """Draw springs from shuttle"""
        self.right = self.lib.new_cell(self.name + 'right')
        space = self.space
        side_shuttle_length =100
        bb = self.shuttle.get_bounding_box()
        pt0 = np.array([np.max(bb[:,0]), np.min(bb[:,1] + space)])
        pt1 = np.array([length, 0]) + pt0
        spring = gdspy.FlexPath((pt0, pt1), width, layer = self.layer, datatype= self.datatype)
        # left = gdspy.copy(right)
        # left = left.rotate(np.pi, (0,pt0[1]))
        # self.add(right)
        self.right.add(spring)
        ul = pt1 + np.array([0, space])
        lr = ul + np.array([self.shuttle_width, -side_shuttle_length])
        right_shuttle = gdspy.Rectangle(ul, lr, layer = self.layer, datatype=self.datatype)
        etch_holes = add_etch_holes(right_shuttle, self.etch_hole_size, self.undercut, self.not_layer,lib = self.lib,  geometry = 'manhattan', shape = 'square')
        self.right.add(right_shuttle)
        self.right.add(etch_holes)
        bottom = gdspy.copy(spring, 0, -side_shuttle_length + 2* space)
        self.bottom_right_spring = bottom
        self.right.add(bottom)
        self.add(gdspy.CellReference(self.right))
        self.add(y_reflection(self.right, self.lib))
    def draw_bottom_shuttle(self, hole_size =100):
        lwr = self.bottom_right_spring.points[0,1]
        self.bottom = self.lib.new_cell(self.name + 'bottom')
        bottom_handle  = gdspy.Rectangle((-self.shuttle_width/2, 0), (self.shuttle_width/2,-self.shuttle_width), layer = self.layer, datatype= self.datatype)
        bottom_hole = gdspy.Rectangle((-hole_size/2, -2*self.shuttle_width), (hole_size/2, - hole_size - 2 * self.shuttle_width))
        bottom_outer = gdspy.offset(bottom_hole, self.shuttle_width, layer = self.layer, datatype= self.datatype)
        probe_tip_hole = gdspy.boolean(bottom_outer, bottom_hole, 'not', layer = self.layer, datatype= self.datatype)
        # probe_tip  = gdspy.boolean(probe_tip_hole, bottom_handle, 'or', layer = self.layer, datatype=self.datatype)

        self.bottom.add(probe_tip_hole)
        self.bottom.add(bottom_handle)
        etch = add_etch_holes(
            self.bottom.get_polygonsets(),
            self.etch_hole_size, self.undercut, self.not_layer, self.lib, 
            fracture=False, smaller_allowed=False, geometry= 'manhattan')
        #check anchored
        etch = expand_etch_holes_smart(self.bottom.get_polygonsets(),
                                       etch, self.layer, self.not_layer, self.undercut, self.etch_hole_size,
                                       self.lib, self.datatype)
        self.bottom.add(etch)
        self.bottom_ref = gdspy.CellReference(self.bottom, (0, lwr + self.space))
        self.add(self.bottom_ref)

    def draw_mid_vernier(self):
        vernier = Vernier(self.name + 'vernier', self.lib, layer = self.layer,not_layer=self.not_layer, datatype=self.datatype,
                           pitch_left=4, pitch_right=5, N_left=8, N_right=7, finger_length=10)
        # pos = (0, self.bottom_ref.get_bounding_box()[1,1]+ self.space)
        y_pos = np.mean([self.bottom_ref.get_bounding_box()[1,1], self.shuttle.get_bounding_box()[0,1]]) - np.linalg.norm(vernier.get_bounding_box()[:,1])/2
        pos = (0, y_pos)
        self.vernier_ref = gdspy.CellReference(vernier, pos)
        self.add(self.vernier_ref)
        ## add vernier connections
        #bottom left
        vernier_bb = self.vernier_ref.get_bounding_box()
        bottom_con = gdspy.Rectangle(
            (vernier_bb[0,0], vernier_bb[0,1]),
            ( vernier_bb[0,0] + vernier.bar_thickness, self.bottom_ref.get_bounding_box()[1,1]), layer = self.layer, datatype = self.datatype)
        self.add(bottom_con)

        # top connection 
        top_con = gdspy.Rectangle(
            (vernier_bb[1,0], vernier_bb[1,1]),
            (vernier_bb[1,0]- vernier.bar_thickness, self.shuttle.get_bounding_box()[0,1]),
            layer = self.layer,
            datatype = self.datatype
        )
        self.add(top_con)

    def draw_top_vernier(self, anchor_width =70):
        top_vernier = Vernier(self.name + 'Tvernier', self.lib, layer = self.layer, datatype=self.datatype,
                               pitch_left=4, pitch_right=5, N_left=8, N_right=7, finger_length=10)
        pos = (self.shuttle.get_bounding_box()[1,0] + top_vernier.finger_length + top_vernier.gap/2, 
               self.shuttle.get_bounding_box()[:,1].mean()- np.linalg.norm(top_vernier.get_bounding_box()[:,1])/2 )
        self.top_vernier_ref = gdspy.CellReference(top_vernier, pos)
        self.add(self.top_vernier_ref)
        ## add anchor
        pos = (self.top_vernier_ref.get_bounding_box()[1,0], self.top_vernier_ref.get_bounding_box()[0,1].mean())
        pos = np.array(pos)
        anchor = gdspy.Rectangle(pos, pos + np.array([anchor_width, anchor_width]), layer = self.layer, datatype=self.datatype)
        self.add(anchor)
    def draw(self):
        """This method comines all the common drawing technics for a spring gauge"""
        self.draw_shuttle()
        self.draw_parallel_springs()
        self.draw_bottom_shuttle()
        self.draw_mid_vernier()
        self.draw_top_vernier()
class BondingPillar(gdspy.Cell):
    """Defines a cell with rectangular metal pad, inset top metal pad, and offset SOI and not dummy"""
    def __init__(self, name, lib: gdspy.GdsLibrary, width, height, metals_gap = 50, soi_metal_gap = 10, soi_dummy_gap = 10, soi_layer = SOI, metal_layer = METAL, top_metal_layer = TOP_METAL, not_dummy_layer = NOT_DUMMY, datatype = 0, exclude_from_current=False):
        if name is None:
            self.name = 'BondingPillar_' + str(id(self))
        else: 
            self.name = name
        gdspy.Cell.__init__(self, self.name, exclude_from_current=exclude_from_current)
        lib.add(self)
        metal = gdspy.Rectangle((-width/2, -height/2), (width/2, height/2), layer = metal_layer, datatype=datatype)
        top_metal = gdspy.offset(metal, -metals_gap,layer = top_metal_layer, datatype=datatype)
        soi = gdspy.offset(metal, soi_metal_gap, layer = soi_layer, datatype=datatype)
        not_dummy = gdspy.offset(soi, soi_dummy_gap, layer = not_dummy_layer, datatype=datatype)
        self.add((metal, top_metal,soi, not_dummy))    


def make_necking_bridge(cell : gdspy.Cell, top_trace : gdspy.FlexPath, bottom_trace: gdspy.FlexPath,alignment_tol = 100, neck_width = 20, soi_layer = SOI, metal_layer = METAL, top_metal_layer =TOP_METAL, not_dummy_layer = NOT_DUMMY, datatype =0, metal_gap =5, gap =5, soi_gap =10, top_metal_gap =20):
    """adds traces in appropriate layers with inset metal layer by metal_gap to cell"""
    ## find intersection
    top_pts = top_trace.points
    bottom_pts = bottom_trace.points
    top_line = shapely.geometry.LineString(top_pts)
    bottom_line = shapely.geometry.LineString(bottom_pts)
    intersection = top_line.intersection(bottom_line)
    rad = max(alignment_tol, top_trace.widths[0]/3, bottom_trace.widths[0]/3)
    # rad = alignment_tol
    circle = intersection.buffer(rad)
    circle2 = intersection.buffer(2*rad)
    top_split= shapely.ops.split(top_line, circle).geoms
    top_split = [shapely.ops.split(line, circle2).geoms for line in top_split]
    top_split = [item for sublist in top_split for item in sublist]
    
    bottom_split = shapely.ops.split(bottom_line, circle).geoms
    bottom_split =[shapely.ops.split(line, circle2).geoms for line in bottom_split]
    bottom_split = [item for sublist in bottom_split for item in sublist]

    def construct_path(segments: list, widths: list):
        number_of_segments = len(segments)
        pts = []
        width_arr = []
        for i in range(number_of_segments):
            these_pts = segments[i].coords
            if i != 0:
                these_pts = these_pts[1:]
            pts+= these_pts
            # print(pts)
            if i%2 ==0:
                width_arr+= [widths[i//2] for pt in these_pts]
            else:
                width_arr.append(widths[i//2 + 1])
        pts = np.array(pts)
        width_arr = np.array(width_arr)
        path = gdspy.PolyPath(pts, width_arr)
        return path
    top_path_neck = construct_path(top_split, [top_trace.widths[0,0], neck_width,top_trace.widths[0,0]])
    # cell.add(top_path)
    bottom_path_neck = construct_path(bottom_split, [top_trace.widths[0,0], neck_width,top_trace.widths[0,0]])
    bridge = gdspy.boolean(top_path_neck, bottom_path_neck, 'and')
    bridge_large = gdspy.offset(bridge, soi_gap)
    bridge_large_metal = gdspy.offset(bridge, alignment_tol)
    #bottom trace
    bottom_trace_soi = gdspy.boolean(bottom_path_neck, None, 'or', layer = soi_layer, datatype=datatype)
    bottom_trace_metal = gdspy.boolean(bottom_path_neck, bridge_large_metal, 'not')
    bottom_trace_metal = gdspy.offset(bottom_trace_metal, -metal_gap, layer = metal_layer, datatype=datatype)
    bottom_trace_top_metal = gdspy.offset(bottom_trace_metal, -top_metal_gap, join_first=True, layer = top_metal_layer, datatype = datatype)
    # bottom_trace_top_metal = gdspy.boolean(bottom_trace_metal, None, 'or', layer = top_metal_layer, datatype=datatype)
    cell.add((bottom_trace_soi, bottom_trace_metal, bottom_trace_top_metal))
    #top trace
    bridge = gdspy.offset(bridge, gap)
    top_trace_soi = gdspy.boolean(top_path_neck, bridge_large, 'not', layer =soi_layer, datatype=0)
    top_trace_top_metal = gdspy.offset(top_path_neck, -top_metal_gap, layer = top_metal_layer, datatype=0)
    top_trace_metal = gdspy.offset(top_trace_soi, -metal_gap, layer= metal_layer, datatype=datatype)
    cell.add((top_trace_soi, top_trace_metal, top_trace_top_metal))

    ## add not dummy
    # not_dumm = gdspy.boolean(top_trace, bottom_trace, 'and')
    # not_dumm = gdspy.offset(not_dumm, alignment_tol, layer = not_dummy_layer, datatype=0)
    # cell.add(not_dumm)





    # cell.add(bottom_path)

    # make_bridge(cell, top_path, bottom_path, soi_layer = soi_layer, metal_layer = metal_layer, top_metal_layer=top_metal_layer,datatype = datatype, gap = alignment_tol, metal_gap = metal_gap)
    # bottom_split = shapely.ops.split(bottom_line, intersection).geoms
    # print(len(top_split))
    # lengths = []
    # points = []
    # for seg in top_split:
    #     pts = np.array(seg.coords)
    #     print(pts)
    #     lengths.append(pts.shape[0])
    #     points.append(pts)
    # print(np.concatenate(points), lengths)
    # lengths = np.array(lengths)
    # lengths = np.cumsum(lengths)
    # pts = np.concatenate((points[0][:-1], points[1], points[2][1:]))
    # widths = np.ones((pts.shape[0]))* top_trace.widths[0,0]
    # widths[lengths[0]-1: lengths[0]+1] = np.ones((2))*neck_width
    # # print( lengths[1]-lengths[0]+1, widths)
    # # top_1 = gdspy.PolyPath()
    # top_trace_new = gdspy.PolyPath(pts, widths)
    # print(top_trace_new)
    # print(widths)
    # cell.add(top_trace_new)
    # print(top_trace_new)

def make_bridge(cell: gdspy.Cell, top_trace: gdspy.FlexPath, bottom_trace:gdspy.FlexPath,soi_layer = SOI, metal_layer = METAL, top_metal_layer =TOP_METAL, datatype =0,  gap = 5, metal_gap =5):
    """adds traces in appropriate layers with inset metal layer by metal_gap to cell"""
    bridge = gdspy.boolean(top_trace, bottom_trace, 'and')
    
    #bottom trace
    bottom_trace_soi = gdspy.boolean(bottom_trace, None, 'or', layer = soi_layer, datatype=datatype)
    bottom_trace_metal = gdspy.boolean(bottom_trace, bridge, 'not')
    bottom_trace_metal = gdspy.offset(bottom_trace_metal, -metal_gap, layer = metal_layer, datatype=datatype)
    bottom_trace_top_metal = gdspy.boolean(bottom_trace_metal, None, 'or', layer = top_metal_layer, datatype=datatype)
    cell.add((bottom_trace_soi, bottom_trace_metal, bottom_trace_top_metal))
    #top trace
    bridge = gdspy.offset(bridge, gap)
    top_trace_soi = gdspy.boolean(top_trace, bridge, 'not', layer =soi_layer, datatype=0)
    top_trace_top_metal = gdspy.offset(top_trace, -metal_gap, layer = top_metal_layer, datatype=0)
    top_trace_metal = gdspy.offset(top_trace_soi, -metal_gap, layer= metal_layer, datatype=datatype)
    cell.add((top_trace_soi, top_trace_metal, top_trace_top_metal))
        
def add_trench(cell:gdspy.Cell, trench_size = 40, clearance =100, tether_width = (100,180), dummy_gap = 30,  layer = TRENCH, datatype = 0):
    """adds a trench""" 
    bb = cell.get_bounding_box()
    coordinates = np.array([bb[0,:] - clearance, bb[1,:] + clearance])
    inner = gdspy.Rectangle(coordinates[0,:], coordinates[1,:], layer = layer, datatype= datatype)
    outer = gdspy.offset(inner, trench_size, layer = layer, datatype = datatype)
    pts = [(0,0), (0,tether_width[0]), (trench_size, 0.5*(tether_width[0]+ tether_width[1])), (trench_size, -0.5*(tether_width[1]-tether_width[0]))]
    tether = gdspy.Polygon(pts, layer = layer, datatype= datatype)
    tether = tether.translate(bb[1,0]+clearance, (bb[1,1] + bb[0,1])/2)
    frame = gdspy.boolean(outer, inner, 'not', layer = layer, datatype = datatype)
    frame = gdspy.boolean(frame, tether, 'not', layer = layer, datatype = datatype)
    
    cell.add(frame)
    dummy = gdspy.offset(inner,-dummy_gap, layer = DUMMY, datatype =0)
    # outer_dummy = gdspy.offset(outer, 0, layer = DUMMY, datatype =0)
    inner_dummy = gdspy.offset(inner, 10, layer = DUMMY, datatype= 0)
    dummy_ring = gdspy.boolean(inner_dummy, dummy, 'not', layer = NOT_DUMMY, datatype = 0)
    cell.add(dummy)
    cell.add(dummy_ring)
    return frame, dummy

def add_metal(shape: gdspy.PolygonSet, gap = 10, layer = METAL, datatype = 0):
    """adds metal pad to shape"""
    pad = gdspy.offset(shape, -gap, join_first=True, layer = layer, datatype= datatype)
    return pad 
